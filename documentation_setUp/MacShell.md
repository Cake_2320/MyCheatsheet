Savoir dans quel shell on est:

````shell
echo $SHELL
echo $0
````

```
>> is append to
~ is file located in current user's home directory
.zshrc is file called .zshrc
```

`~/.zshrc` will get executed when you log in (assuming you are using ZSH as your shell)

## Gestion des paths

````shell
echo 'export PATH="/usr/local/opt/openssl@1.1/bin:$PATH"' >> ~/.zshrc
echo 'export DYLD_LIBRARY_PATH=/usr/local/opt/openssl/lib:$DYLD_LIBRARY_PATH' >> ~/.zshrc
````


Manually run the .profile file (eliminate logoff & logon at this time):
```
source ~/.profile
```

ouvre le fichier avec l'editeur nano
```
nano .profile
```



### LN
```
ln -s /usr/local/opt/openssl@1.1 /usr/local/opt/openssl
ln -shf /usr/local/Cellar/openssl@1.1/1.1.1l_1 /usr/local/opt/openssl
```


From `man ln`:

     ln [-Ffhinsv] source_file [link_name]

     -h    If the link_name or link_dirname is a symbolic link, do not follow it.  This is most useful with
           the -f option, to replace a symlink which may point to a directory.
     -s    Create a symbolic link.

#### exemple
On my M1 iMac running Monterey, I had the following situation in /opt/homebrew/opt:

    openssl@1.1 -> ../Cellar/openssl@1.1/1.1.1l_1
    openssl@3 -> ../Cellar/openssl@3/3.0.0_1
    openssl -> ../Cellar/openssl@3/3.0.0_1

So the plain "openssl" link was directing to version 3. After seeing the answer of @PILLOWPET, I did the following:

    mv openssl opensslbak
    ln -s ../Cellar/openssl@1.1/1.1.1l_1 openssl

So now the situation is this:

    openssl -> ../Cellar/openssl@1.1/1.1.1l_1
    openssl@1.1 -> ../Cellar/openssl@1.1/1.1.1l_1
    openssl@3 -> ../Cellar/openssl@3/3.0.0_1
    opensslbak -> ../Cellar/openssl@3/3.0.0_1

#### remove les links
```
rm -f /usr/local/opt/openssl
```
     -f          Attempt to remove the files without prompting for confirmation, regardless of the file's permissions.  If the file does not
                 exist, do not display a diagnostic message or modify the exit status to reflect an error.  The -f option overrides any
                 previous -i options.


Instead of linking to the specific version (e.g. 1.1.1.m), you could link to the 1.1 link which links to the latest version.
```
rm -f /usr/local/opt/openssl && ln -s /usr/local/opt/openssl@1.1 /usr/local/opt/openssl
```

### LS
lister les raccourcis fait avec `ln`
```
ls -ld /usr/local/opt/openssl*
```
     -l      (The lowercase letter “ell”.) List files in the long format, as described in the The Long Format subsection below.
     -d      Directories are listed as plain files (not searched recursively).

Donne les liens entre les 'fonctions' appelé et leur chemin en dur sur le disque
```shell
ls -la /usr/local/opt
```
     -a      Include directory entries whose names begin with a dot (‘.’).


### Profile file

A profile file is a start-up file of an UNIX user, like the autoexec.bat file of DOS. When a UNIX user tries to login to
his account, the operating system executes a lot of system files to set up the user account before returning the prompt
to the user.

In addition to the system settings, the user might wish to have some specific settings for his own account. To achieve
this in UNIX, at the end of the login process, the operating system executes a file at the user level, if present. This
file is called profile file.

The name of the profile file varies depending on the default shell of the user. The profile file, if present, should
always be in the home directory of the user. The following are the profile files of the commonly used shells:

| Shell  | Profile File     |
|--------|------------------|
| Ksh    | .profile         |
| Bourne | .profile         |
| Bash   | .bash_profile    |
| Tcsh   | .login           |
| Csh    | .login           |



