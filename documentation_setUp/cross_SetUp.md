# CICD

## Github

````yml
- bash: |
        python -m venv worker_venv
        source worker_venv/bin/activate
        pip install --target="$(workingDirectory)/.python_packages/lib/site-packages" -r requirements.txt
  workingDirectory: $(workingDirectory)
  displayName: 'Install application dependencies'
````

### Azure Function with GitHub
https://github.com/Azure/actions-workflow-samples/blob/master/FunctionApp/linux-python-functionapp-on-azure.yml
https://docs.microsoft.com/fr-fr/azure/azure-functions/functions-how-to-github-actions?tabs=dotnet
https://docs.microsoft.com/en-us/azure/azure-functions/functions-how-to-github-actions?tabs=dotnet

OR

https://azure.github.io/AppService/2020/12/11/cicd-for-python-apps.html
