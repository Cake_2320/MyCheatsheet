[how to use markdown, sheet sheets](https://www.markdownguide.org/basic-syntax/)

[Markdown in pycharm](https://www.jetbrains.com/help/pycharm/markdown.html)

[website to convert markdown into HTML](https://dillinger.io/)

[link for MD synthax](https://docs.framasoft.org/fr/grav/markdown.html)

# All

<u>Texte Souligné</u>

# Tables

https://www.tablesgenerator.com/markdown_tables#

## MarkDown Anchor
[Link to issue](https://youtrack.jetbrains.com/issue/IDEA-238096)

[link to markdown.md with anchor](./markdown.md#Advices)
[link to markdown.md with anchor](./markdown.md#Advices)

[link to markdown.md with anchor](../python/external_interaction/win32.md#win32com-with-multithreading)

[link to SSH.md with anchorMacOS](./SSH.md#MacOS)  
[link to basic_manipulation.md with anchor](../python/standard_use/basic_manipulation.md#Math)
```md
[click on this link](#my-multi-word-header)
### My Multi Word Header
```

```md
[link to markdown.md with anchor](./README.md#annexe---test-anchor)
## Annexe - test: Anchor
```

# Advices
## Pycharm use:
Use the Structure tool window `⌘7`

File Structure popup `⌘F12` to view and jump to the relevant headings.

# Examples
autolink : <https://www.jetbrains.com/help/pycharm/markdown.html>
https://www.jetbrains.com/help/pycharm/markdown.html

[comment]: <> (Your comment text)


# Diagrams
## Pycharm SetUp
Enable diagram support

1. In the Settings/Preferences dialog `⌘,`, select `Languages & Frameworks` | `Markdown.

2. Enable either `Mermaid` or `PlantUML` under Markdown Extensions.

### Examples
<https://plantuml.com/fr/sequence-diagram>

```puml
A -> B
B -> A
A -> A
B -> C
C -> A
```
```puml
Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response
Alice -> Bob: Another authentication Request
Alice <-- Bob: another authentication Response
```

