# Table Creation

## Global

````SQL
create table reservoir_profile
(
    id INT IDENTITY(1,1) PRIMARY KEY,
    file_name VARCHAR(255),
    import_date  DATETIME,
    status VARCHAR(10),
    hyp_comment VARCHAR(500),
    hyp_file_flag BIT,
    start_date DATE,
)
````

## Foreign Key

https://www.w3schools.com/sql/sql_foreignkey.asp

<u>SQL Server / Oracle / MS Access:</u>

````sql
    PersonID int FOREIGN KEY REFERENCES Persons(PersonID)
````

## Insert

https://sql.sh/cours/insert-into

````SQL
INSERT INTO table (nom_colonne_1, nom_colonne_2, ...
 VALUES ('valeur 1', 'valeur 2', ...)
 
INSERT INTO client (prenom, nom, ville, age)
 VALUES
 ('Rébecca', 'Armand', 'Saint-Didier-des-Bois', 24),
 ('Aimée', 'Hebert', 'Marigny-le-Châtel', 36),
 ('Marielle', 'Ribeiro', 'Maillères', 27),
 ('Hilaire', 'Savary', 'Conie-Molitard', 58);
````

For boolean value set value to `0` or `1`

# Truncate or delete
[link](https://learnsql.com/blog/difference-between-truncate-delete-and-drop-table-in-sql/#:~:text=To%20remove%20specific%20rows%2C%20use,and%20data%2C%20use%20DROP%20TABLE%20.)

# Select calcul or Group

## Max

````sql
SELECT MAX(items) as max_items
FROM product;
````
