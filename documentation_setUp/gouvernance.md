
```
└── Forecast
    └── APPS
        └── WOLF
            └── private
                └── EXCHANGE-SERVER
                    └── UNPLANNED_MAINTENANCE_MAIL
                        └── DBCargo
```

```
├── app_name
    │
    ├── app_name
    │   ├── __init__.py
    │   ├── folder_name
    │   └── etc...
    ├── tests
    │   ├── unit
    │   └── integration
    ├── README.md
    ├── setup.py
    └── requirements.txt
```

```
project.scratch/
    project/
        scratch/
            __init__.py
            dostuff.py
            tests/
                unit/
                    test_dostuff.py
                integration/
                    test_integration.py
```