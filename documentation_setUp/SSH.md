# MacOS

## To generate SSH keys in macOS:

1. Enter the following command in the Terminal window to starts the key generation process.

```shell
ssh-keygen -t rsa
```

2. Press the ENTER key to accept the default location. The ssh-keygen utility prompts you for a passphrase.
3. Type in a passphrase. You can also hit the ENTER key to accept the default (no passphrase). However, this is not
   recommended.

Your private key is saved to the id_rsa file in the .ssh directory and is used to verify the public key you use belongs
to the same Triton Compute Service account.

>  Never share your private key with anyone!
 
## Get your SSH key

```shell
pbcopy < ~/.ssh/id_rsa.pub
```
Choose to Import Public Key and paste your SSH key into the Public Key field.
You can keep everythings that you copy.


# Deal with 2 SSH

## To generate and use a second SSH

[link stackoverflow](https://stackoverflow.com/questions/23537881/fingerprint-has-already-been-taken-gitlab)

navigate to .ssh folder in your profile (even works on windows) and run command
```shell
ssh-keygen -t rsa
```

when asked for file name give another filename id_rsa_2 (or any other). enter for no passphrase (or otherwise). You will end up making id_rsa_2 and id_rsa_2.pub

use the command
```shell
cat id_rsa_2.pub
```

## clone git repo with second SSH

https://xiaolishen.medium.com/use-multiple-ssh-keys-for-different-github-accounts-on-the-same-computer-7d7103ca8693

create a file with no extension in .ssh folder named 'config'

put this block of configuration in your config file
```
Host           gitlab.com
HostName       gitlab.com
IdentityFile   C:\Users\<user name>\.ssh\id_rsa
User           <user name>


Host           gitlab_2
HostName       gitlab.com
IdentityFile   C:\Users\<user name>\.ssh\id_rsa_2
User           <user name>
```

for example instead of using
```shell
git clone git@gitlab.com:..............
```

simply use
```shell
git clone git@gitlab_2:...............
```

```shell
git clone git@github.com-work:[my work GitHub group]/[my project].git
```

## change the remote of a Git project

1. cd into project
2. do command `git remote -v` in terminal
3. change it
   - for HTTP : `git remote set-url origin https://github.com/Career-Karma-Tutorials/git-submodule`
   - for SSH URL : `git remote set-url origin git@github.com:Career-Karma-Tutorials/git-submodule.git`
4. check with `git remote -v`
