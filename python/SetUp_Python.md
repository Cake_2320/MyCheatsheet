# Python

````shell
python --version
````

# Virtual Environnement

[Link](https://realpython.com/effective-python-environment/#python-version-management)

[How to setup the Python and Spark environment for development, with good software engineering practices](https://towardsdatascience.com/how-to-setup-the-pyspark-environment-for-development-with-good-software-engineering-practices-5fb457433a86)

## venv : Virtualenv

Dans le folder faire :

````shell
python3 -m venv ./.venv
python3 -m venv <myenvname>
````

To choose the python version:

````shell
virtualenv --python=/usr/bin/python2.6 <path/to/new/virtualenv/>
````

## Pipenv

[SetUp PipEnv](https://realpython.com/effective-python-environment/#python-version-management)

```shell
cd /Users/dhillard/myproj

// Create virtual environment
$ pipenv install

// Activate virtual environment (uses a subshell)
$ pipenv shell
```

## Poetry
````shell
poetry env use /full/path/to/python
````

# Install Package

## PIP

`pip list` will show you all the packages that are installed for the virtualenv

to create the requirements.txt file from a CLI, so you can run this command: `pip freeze > requirements.txt`

````python
pip3 install -r requirements.txt --target=".python_packages/lib/site-packages"
````

# IDE

## Pycharm

Please go to `File`|`Settings`|`Tools`|`Python` Integrated Tools and change the default test runner to py.test. Then
you'll get the py.test option to create tests instead of the unit`test one.

# Test - Unittest

## unittest

````shell
python -m unittest test_module1 test_module2
python -m unittest test_module.TestClass
python -m unittest test_module.TestClass.test_method
python -m unittest tests/test_something.py
````

You can run tests with more detail (higher verbosity) by passing in the -v flag:

```
python -m unittest -v test_module
```

When executed without arguments Test Discovery is started:

```
python -m unittest
```

````shell
coverage html
coverage report -m

coverage run -m pytest test
python3 -m pytest test
python3 -m pytest --cov=. test/
python3 -m pytest --cov=Shared/ test/Shared
````

coverage run --source=<package> --module pytest --verbose <test-files-dirs> && coverage report --show-missing

----

You can invoke testing through the Python interpreter from the command line:

```shell
python -m pytest [...]
```

This is almost equivalent to invoking the command line script `pytest [...]` directly, except that calling via python
will also add the current directory to sys.path.
----
https://stackoverflow.com/questions/36456920/is-there-a-way-to-specify-which-pytest-tests-to-run-from-a-file

https://stackoverflow.com/questions/62221654/how-to-get-coverage-reporting-when-testing-a-pytest-plugin

````shell
coverage run --source=<package> --module pytest --verbose <test-files-dirs> && coverage report --show-missing
coverage run --source=<package> -m pytest -v <test-files-dirs> && coverage report -m


coverage run --source=Shared -m pytest -v test/Shared && coverage report -m
````

https://coverage.readthedocs.io/en/v4.5.x/cmd.html

````shell

````

https://stackoverflow.com/questions/62221654/how-to-get-coverage-reporting-when-testing-a-pytest-plugin
https://coverage.readthedocs.io/en/v4.5.x/cmd.html
https://stackoverflow.com/questions/36456920/is-there-a-way-to-specify-which-pytest-tests-to-run-from-a-file
https://docs.pytest.org/en/latest/how-to/usage.html
https://coverage.readthedocs.io/en/coverage-5.5/
https://realpython.com/pytest-python-testing/#pytest-cov
https://blog.j-labs.pl/2019/02/Pytest-why-its-more-popular-than-unittest
https://www.linuxjournal.com/content/python-testing-pytest-fixtures-and-coverage

## fixture and test

https://www.linuxjournal.com/content/python-testing-pytest-fixtures-and-coverage

# Dependencies

## Install
https://realpython.com/python-wheels/
https://python-poetry.org/


## Azure Functions

### Mac

https://docs.microsoft.com/en-us/azure/azure-functions/create-first-function-vs-code-python

[doc](https://docs.microsoft.com/fr-fr/azure/azure-functions/functions-run-local?tabs=v4%2Cmacos%2Cpython%2Cportal%2Cbash%2Ckeda#v2)

````shell
brew tap azure/functions
brew install azure-functions-core-tools@4
# if upgrading on a machine that has 2.x or 3.x installed:
brew link --overwrite azure-functions-core-tools@4
````

````shell
cd YourRepositoryDirectory
func init LocalFunctionProj --python
````

[doc2](https://docs.microsoft.com/fr-fr/azure/azure-functions/functions-create-function-linux-custom-image?tabs=in-process%2Cpowershell%2Cazure-cli&pivots=programming-language-python)

Dans command panel > Debug: Start debugging
