import pandas as pd
import datetime as dt

DATE_FORMAT = "%Y-%m-%d"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

dt.datetime.strptime("2022-01-31 09:00:00", DATETIME_FORMAT)
dt.datetime.strptime("2022-01-31 09:00:00", DATETIME_FORMAT).date()
pd.to_datetime("2022-01-31 09:00:00", format=DATETIME_FORMAT)

pd.DataFrame([
    {'start_datetime': "2022-01-31 09:00:00", 'end_datetime': "2022-02-01 16:00:00",
     'rate': 0.10, 'cause': '', 'well': 'Well_01'},
])

# Dataframe

pd.DataFrame().to_json(orient='records')

# empty with columns
pd.DataFrame(columns=["comment", "id"])
pd.DataFrame(columns=['date', 'value'])
pd.DataFrame(columns=['value'], index=pd.DatetimeIndex([], name=None, freq=None))
pd.DataFrame(index=pd.DatetimeIndex([], name=None, freq=None)) # freq :
#[link to markdown.md with anchor](./markdown.md#Advices)
#python/standard_use/advance.md:93

map_wagon_number_x_pool = pd.DataFrame([
    {'wagon_number': 100000000001, 'business_unit': 'TD', 'product_type': 'BMO'},
    {'wagon_number': 100000000002, 'business_unit': 'TD', 'product_type': 'BMO'},
]).convert_dtypes()

list_columns = ["wagon_number", 'nb_wagons_available', 'nb_wagons_in_maintenance']
df[list_columns] = df[list_columns].astype(int)
df = df.astype(int)

dates = pd.date_range('1/1/2000', periods=8)
df = pd.DataFrame(np.random.randn(8, 4),
                  index=dates, columns=['A', 'B', 'C', 'D'])

df = pd.DataFrame([
    {'wagon_number': 100000000001, 'business_unit': 'TD', 'product_type': 'BMO', 'Test': None},
    {'wagon_number': 100000000002, 'business_unit': None, 'product_type': 'BMO', 'Test': None},
    {'wagon_number': 100000000002, 'business_unit': 'TD', 'product_type': None, 'Test': None},
    {'wagon_number': 100000000002, 'business_unit': None, 'product_type': None, 'Test': None},
])

date_format = "%Y-%m-%d"

df = pd.DataFrame([
    {'historical_flag': 1, 'datetime': pd.to_datetime('2022-01-01', format=self.date_format),
     'baseline_gas_inject': '262'},
    {'historical_flag': 1, 'datetime': pd.to_datetime('2022-02-01', format=self.date_format),
     'baseline_gas_inject': '262.1'},
    {'historical_flag': 0, 'datetime': pd.to_datetime('2022-03-01', format=self.date_format),
     'baseline_gas_inject': '262.2'}
])

df_data = pd.DataFrame(
    [{'historical_flag': 1, 'datetime': '2022-01-01', var_05: '263', var_16: '264'},
     {'historical_flag': 1, 'datetime': '2022-02-01', var_05: None, var_16: '264.1'},
     {'historical_flag': 0, 'datetime': '2022-04-01', var_05: '263.2', var_16: None}])
df_data['datetime'] = pd.to_datetime(df_data['datetime'], format=self.date_format)

# time series

format = '%Y-%m-%d %H:%M:%S'
df['Datetime'] = pd.to_datetime(df['date'] + ' ' + df['time'], format=format)
df = df.set_index(pd.DatetimeIndex(df['Datetime']))
