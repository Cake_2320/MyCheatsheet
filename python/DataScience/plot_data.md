# ploty
[link](https://plotly.com/)

# Standard
## Pandas

[pandas doc plot](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.plot.html)

````python
df.plot()
df.plot.scatter(x='length',
                y='width',
                c='DarkBlue')
df.plot.scatter(x='length',
                y='width',
                c='species',
                colormap='viridis')

import matplotlib.pyplot as plt

x = [1,2,3,4]
y = [4,1,3,6]

plt.scatter(x, y, c='coral')
````

## Split View

````python
plt.figure(figsize=(15, 5))
for i, feature in enumerate(df.columns):
    plt.subplot(1, 4, i + 1)
    # then do the plot for each
    df[feature].plot(kind='hist', title=feature)
````

## Histogram

````python
df[feature].plot(kind='hist', title=feature)
plt.xlabel(feature)

size_largeur = 15
size_hauteur = 10
df.hist(figsize=(size_largeur, size_hauteur))
````

## Box Plot (boite a moustache)

````python
df.plot(kind='box',
        subplots=True, layout=(3, 3), sharex=False, sharey=False, figsize=(10, 8))
# subplots=True ; toutes les boites a moustache sont sur des graphiques différents
# sharex=False : toutes les largeurs ont leur propre echelle
# sharey=False : toutes les hauteurs ont leur propre echelle
# layout=(3,4) : 3 lignes, 4 colonnes
````