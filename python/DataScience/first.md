Stratify # param pour la repartition Stocker les données

shape pour sortir les infos

https://keras.io/
https://www.tensorflow.org/install/

# Data Exploration

# Basic Describe data

````python
df.info()
df.head()
df.describe()
df.column_name.value_counts()
X = df[['sqft', 'bdrms', 'age']].values
y = df['price'].values
len(X_train)
````

## Matrice de corrélation

Plot data

```python
import seaborn as sns

sns.pairplot(df, hue='Outcome');

sns.heatmap(df.corr(), annot=True)
# annot=True : met les valeurs de corrélation sur chaque case
```

# Preprocessing

Improve result

- normalize the input features with one of the rescaling techniques mentioned above
- use a different value for the learning rate of your model
- use a different optimizer

## Normalise Data

````python
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.utils import to_categorical

sc = StandardScaler()
X = sc.fit_transform(df.drop('Outcome', axis=1))
y = df['Outcome'].values
y_cat = to_categorical(y)
````

## Divide column to categorical (binary)

````python
from tensorflow.keras.utils import to_categorical

y = df['Outcome'].values
y_cat = to_categorical(y)
````

## Data Transform

````python
# convert the categorical features into binary dummy columns.
# You will then have to combine them with
# the numerical features using `pd.concat`.
df_dummies = pd.get_dummies(df[['sales', 'salary']])
# ca prend les combinaisons de toutes les valeurs des deux variables et
# ca les mets en colonnes, puis en dessous ca fait du binaire du coup (0 ou 1)
````

## Feature selection

[Documentation scikit-learn](https://scikit-learn.org/stable/modules/feature_selection.html)

````python

````

# Train and Apply Part

## split data test

````python
from sklearn.model_selection import train_test_split

# split the data into train and test with a 20% test size
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
X_train, X_test, y_train, y_test = train_test_split(X, y_cat, random_state=22, test_size=0.2)
# random_state:
# when random_state set to an integer, train_test_split will return same results for each execution.
# when random_state set to an None, train_test_split will return different results for each execution.
# test_size
````

## Cross Validation

````python
from sklearn.model_selection import KFold, cross_val_score


# check if you still get the same results if you use a 5-Fold cross validation on all the data

def build_logistic_regression_model():
    model = Sequential()
    model.add(Dense(1, input_dim=20, activation='sigmoid'))
    model.compile(Adam(learning_rate=0.5), 'binary_crossentropy', metrics=['accuracy'])
    return model


model = KerasClassifier(build_fn=build_logistic_regression_model,
                        epochs=10, verbose=0)

cv = KFold(5, shuffle=True)
scores = cross_val_score(model, X, y, cv=cv)

print("The cross validation accuracy is {:0.4f} ± {:0.4f}".format(scores.mean(), scores.std()))
````

## Train the model

Learning rate:

- si il est bas il met très longtemps à converger (reduire les erreurs et trouver la solution)
- si il est faut : sur apprentissage? on trouve pas vraiment la solution

````python
# train the model on the training set and check its accuracy on training and test set
# how's your model doing? Is the loss growing smaller?
model.fit(X_train, y_train, epochs=10)
model.fit(X_train, y_train, epochs=40, verbose=0)
model.fit(X_train, y_train, epochs=20, verbose=2, validation_split=0.1)
# epochs : nombre de fois que le reentrainement se lance
# validation_split=0.1 : take 10% of the train data and validate the loss and the accuracie over it
# it has nothing to do with the training set (train_y)
````

## Predict

````python
y_train_pred = model.predict(X_train)

y_test_pred = model.predict_classes(X_test)  # in case of binary ?
````

# Analyse result

## Predict True or False

## accuracy_score

````python
# Simplement le score (en proportion) de precision. Proche de 1 : predit mega bien
from sklearn.metrics import accuracy_score

accuracy_score(y_test_class, y_pred_class)
````

### Confusion Matrix

Matrice dans laquelle nous avons les faux positifs, faux negatifs, vrais positifs et vrai negatifs

````python
from sklearn.metrics import confusion_matrix


def pretty_confusion_matrix(y_true, y_pred, labels=["False", "True"]):
    cm = confusion_matrix(y_true, y_pred)
    pred_labels = ['Predicted ' + l for l in labels]
    df = pd.DataFrame(cm, index=labels, columns=pred_labels)
    return df
````

### Classification report

`````python
from sklearn.metrics import classification_report

print(classification_report(y_test, y_test_pred))
`````

### R2 Score

````python
from sklearn.metrics import r2_score

r2_score(y_train, y_train_pred)
print("The R2 score on the Train set is:\t{:0.3f}".format(r2_score(y_train, y_train_pred)))
````

# Models

## Models Parameters

### decription

````python
model.summary()
````

## Models Examples

## Linear Regression

````python
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.optimizers import Adam

number_of_output = 1
number_of_input = 3
# create a linear regression model in Keras
# with the appropriate number of inputs and output
model = Sequential()
model.add(Dense(number_of_output, input_shape=(number_of_input,)))
model.compile(Adam(learning_rate=0.8), 'mean_squared_error')

model = Sequential()
model.add(Dense(1, input_dim=20, activation='sigmoid'))
model.compile(Adam(learning_rate=0.5), 'binary_crossentropy', metrics=['accuracy'])

model.summary()
````

## Support Vector Machine (SVM)

````python
from sklearn.svm import SVC

for mod in [RandomForestClassifier(), SVC(), GaussianNB()]:
    mod.fit(X_train, y_train[:, 1])
````

## Gaussian

````python
from sklearn.naive_bayes import GaussianNB

for mod in [RandomForestClassifier(), SVC(), GaussianNB()]:
    mod.fit(X_train, y_train[:, 1])
````

## Random Forest

````python
from sklearn.ensemble import RandomForestClassifier

for mod in [RandomForestClassifier(), SVC(), GaussianNB()]:
    mod.fit(X_train, y_train[:, 1])
````