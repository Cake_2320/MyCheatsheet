# Logical Operator

```python
&  # et

0 or 1

0 and 1
```

----

| Symbole  | Nom                            |
|----------|--------------------------------|
| {}       | Agencement de dictionnaire     |
| ==       | egal de                        |
| !=       | Different de                   |
| {}       | Agencement de dictionnaire     |
| ()       | Agencement de n-uplet          |
| []       | Agencement de liste            |
| .        | Attribut                       |
| ()       | Argument de fonction           |
| []       | Partie (opérateur d'indiçage)  |
| await    | Attente de résultat            |
| **       | Puissance                      |
| ~        | inversion de bit               |
| +        | Positif                        |
| -        | Négatif                        |
| *        | Multiplier                     |
| @        | Multiplication de matrices     |
| /        | Diviser                        |
| //       | Résultat entier d'une division |
| %        | Modulo                         |
| +        | Addition                       |
| -        | Soustraction                   |
| <<       | Décalage à gauche              |
| > >      | Décalage à droite              |
| &        | et logique                     |
| ^        | ou exclusif                    |
| la barre | ou logique                     |
| in       | Test d'appartenance            |
| not in   | Test de non appartenance       |
| is       | Test d'égalité type            |
| is not   | Test de non égalité de type    |
| <        | inférieur                      |
| >        | supérieur                      |
| <=       | inférieur ou égal              |

| >= | supérieur ou égal | | == | est égal | | != | est différent | | not | non booléen | | and | et booléen | | or | ou
booléen | | if ... else ... | expression conditionelle | | lambda | expression lambda |

# Error Gestion

````python
raise ValueError("ValueError exception thrown")
````

# Threading

https://koor.fr/Python/CodeSamples/ThreadSample.wp

https://stackoverflow.com/questions/15085348/what-is-the-use-of-join-in-python-threading#:~:text=In%20python%203.,')%20time
.

Voir des exemples dans win32package.md
[ici](./external_interaction/win32.md#win32com-with-multithreading)

# Variable Pointer Comportment

## test

https://stackoverflow.com/questions/13530998/are-python-variables-pointers-or-else-what-are-they

We call them references. They work like this

```python
i = 5  # create int(5) instance, bind it to i
j = i  # bind j to the same int as i
j = 3  # create int(3) instance, bind it to j
print(i)  # i still bound to the int(5), j bound to the int(3)
```

Small ints are interned, but that isn't important to this explanation

````python
i = [1, 2, 3]  # create the list instance, and bind it to i
j = i  # bind j to the same list as i
i[0] = 5  # change the first item of i
print(j)  # j is still bound to the same list as i
````

Variables are not pointers. When you assign to a variable you are binding the name to an object. From that point onwards
you can refer to the object by using the name, until that name is rebound. In your first example the name i is bound to
the value 5. Binding different values to the name j does not have any effect on i, so when you later print the value of
i the value is still 5. In your second example you bind both i and j to the same list object. When you modify the
contents of the list, you can see the change regardless of which name you use to refer to the list. Note that it would
be incorrect if you said "both lists have changed". There is only one list but it has two names (i and j) that refer to
it.

## Example

````python
def aze(par):
    aaa = pd.DataFrame(par)
    aaa.loc[0, 'Name'] = 'KEV'
    return aaa


def aze(par):
    aaa = par.copy()
    aaa.loc[0, 'Name'] = 'KEV'
    return aaa


import pandas as pd

data = {'Name': ['Jai', 'Princi', 'Gaurav', 'Anuj'],
        'Age': [27, 24, 22, 32],
        'Address': ['Delhi', 'Kanpur', 'Allahabad', 'Kannauj'],
        'Qualification': ['Msc', 'MA', 'MCA', 'Phd']}
a = pd.DataFrame(data)

b = aze(a)
````

## Pandas

https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.copy.html

`````python
 DataFrame.copy(deep=True)
`````

Parameters deep: bool, default True Make a deep copy, including a copy of the data and the indices. With deep=False
neither the indices nor the data are copied.


## Class Method
[link](https://pynative.com/python-class-method/#:~:text=Delete%20Class%20Methods-,What%20is%20Class%20Method%20in%20Python,the%20object%20of%20the%20class.)

# Objects
## Dataclass
[link](https://www.invivoo.com/dataclasses-python/)
```python
@dataclasses.dataclass(frozen=True)
class UnitCalculDataclass:
    event_name: str
    event_type: str
    event_phase: str
```

## Inner Class
```python
from __future__ import annotations

class GetCalculClass(ABC):

    def __init__(self):
        self.result: Dict[GetCalculClass.DataEventPerimeter: pd.DataFrame] = {}

    @dataclasses.dataclass(frozen=True)
    class DataEventPerimeter:
        scenario_id: int  # event name send to the azure function
        asset: str
```

# Logging
## Theory
[doc python](https://docs.python.org/3/howto/logging.html)
[doc 1](https://realpython.com/python-logging/)
[doc python handler](https://docs.python.org/3/library/logging.handlers.html)

## Concrete Case 1 :use String IO

[example 1](https://stackoverflow.com/questions/31999627/storing-logger-messages-in-a-string)
```python
import io
import logging
from typing import List


def generate_handler(id_logger: str):
    log_stream = io.StringIO()
    log_handler = logging.StreamHandler(log_stream)
    log_handler.setFormatter(logging.Formatter('%(name)s - %(levelname)s - %(message)s'))
    functional_logger = logging.getLogger('functional_' + id_logger)
    functional_logger.addHandler(log_handler)
    return functional_logger, log_stream


def get_functional_logger(id_logger: str):
    return logging.getLogger('functional_' + id_logger)


def get_functional_logger_message_filtered(log_stream, keep_values: str = None) -> List[str]:
    if keep_values is not None:
        if keep_values.upper() not in ['WARNING', 'ERROR']:
            raise ValueError(f"the parameter 'keep_values' is '{keep_values}' and should be 'WARNING' or 'ERROR'")
        keep_values = keep_values.upper()

    final_list_message = []

    list_messages = log_stream.getvalue().split('\n')[:-1]
    for elem in list_messages:
        list_elem = elem.split(' - ')
        if len(list_elem) != 3:
            continue

        if keep_values is None:
            final_list_message.append(list_elem[2])
        else:
            if list_elem[1] == keep_values:
                final_list_message.append(list_elem[2])

    return final_list_message
```
test
```python
from ConfigFolder.log_functions import generate_handler, get_functional_logger, get_functional_logger_message_filtered


class TestLogFunctions:

    def test_info(self):
        # Given
        id_logger = 'foo'

        # When
        functional_logger_init, log_stream = generate_handler(id_logger=id_logger)
        functional_logger_get = get_functional_logger(id_logger=id_logger)
        functional_logger_get.info('message_1')

        # Expected
        message_expected = ""

        # Then
        assert log_stream.getvalue() == message_expected

    def test_warning(self):
        # Given
        id_logger = 'foo'

        # When
        functional_logger_init, log_stream = generate_handler(id_logger=id_logger)
        functional_logger_get = get_functional_logger(id_logger=id_logger)
        functional_logger_get.warning('message_1')

        # Expected
        message_expected = "functional_foo - WARNING - message_1\n"

        # Then
        assert log_stream.getvalue() == message_expected

    def test_error(self):
        # Given
        id_logger = 'foo'

        # When
        functional_logger_init, log_stream = generate_handler(id_logger=id_logger)
        functional_logger_get = get_functional_logger(id_logger=id_logger)
        functional_logger_get.error('message_1')

        # Expected
        message_expected = "functional_foo - ERROR - message_1\n"

        # Then
        assert log_stream.getvalue() == message_expected


class TestLogFunctionsFilter:
    def test_without_parameter(self):
        # Given
        id_logger = 'foo'
        functional_logger_init, log_stream = generate_handler(id_logger=id_logger)
        functional_logger_get = get_functional_logger(id_logger=id_logger)
        functional_logger_get.warning('message_1')
        functional_logger_get.error('message_2')
        functional_logger_get.warning('message_3')

        # When
        result = get_functional_logger_message_filtered(log_stream=log_stream)

        # Expected
        message_expected = ['message_1', 'message_2', 'message_3']

        # Then
        assert result == message_expected

    def test_with_parameter(self):
        # Given
        id_logger = 'foo'
        functional_logger_init, log_stream = generate_handler(id_logger=id_logger)
        functional_logger_get = get_functional_logger(id_logger=id_logger)
        functional_logger_get.warning('message_1')
        functional_logger_get.error('message_2')
        functional_logger_get.warning('message_3')

        # When
        result = get_functional_logger_message_filtered(log_stream=log_stream, keep_values='warning')

        # Expected
        message_expected = ['message_1', 'message_3']

        # Then
        assert result == message_expected

    def test_with_parameter_in_error(self):
        # Given
        id_logger = 'foo'
        functional_logger_init, log_stream = generate_handler(id_logger=id_logger)
        functional_logger_get = get_functional_logger(id_logger=id_logger)
        functional_logger_get.warning('message_1')
        functional_logger_get.error('message_2')
        functional_logger_get.warning('message_3')

        # When
        try:
            result = get_functional_logger_message_filtered(log_stream=log_stream, keep_values='warnings')
        except ValueError as e:
            result = e.args[0]

        # Expected
        message_expected = "the parameter 'keep_values' is 'warnings' and should be 'WARNING' or 'ERROR'"

        # Then
        assert result == message_expected

```
