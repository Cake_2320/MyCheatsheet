# Functions
````python
def change_name_file(filename: str, new_file_extension: str, filename_extension: str = ""):
    filename_list = filename.split(".")
    filename_list[-2] = filename_list[-2] + filename_extension
    filename_list[-1] = new_file_extension
    return ".".join(filename_list)
````

# Excel

````python
xls = pd.ExcelFile('path_to_file.xls')
df1 = pd.read_excel(xls, 'Sheet1')
df2 = pd.read_excel(xls, 'Sheet2')

xls = pd.ExcelFile('path_to_file.xls')
for sheet_name in xls.sheet_names:
    df_sheet=xls.parse(sheet_name, header=None, dtype=str)
    

list_sheets = [("NEW_CONTRACTS", pd.DataFrame(columns=ALL_COLUMNS_NEW_CONTRACT + ["comment"])),
               ("ACCIDENTAL_REPAIR", ar_parameters),
               ("REVISION", revision_parameters),
               ("SCRAPING", scraping_parameters)]

writer = pd.ExcelWriter(root_path, engine='xlsxwriter')
for name, group in list_sheets:
    group = group.fillna("")
    for column in group.columns:
        group[column] = group[column].astype(str)
    group.to_excel(writer, sheet_name=name, index=False)
    writer.save()

with open(filename, 'rb') as f:
    f.read()

# Return in HTTP
import mimetypes

with open(filename, 'rb') as f:
    mimetype = mimetypes.guess_type(filename)
    return func.HttpResponse(f.read(), mimetype=mimetype[0])
````

## Save with many sheets

````shell
pip3 install xlsxwriter
````

````python
list_sheets = [("NEW_CONTRACTS", pd.DataFrame(columns=ALL_COLUMNS_NEW_CONTRACT)),
               ("ACCIDENTAL_REPAIR", pd.DataFrame(columns=ALL_COLUMNS_NEW_CONTRACT))),
("REVISION", pd.DataFrame(columns=ALL_COLUMNS_NEW_CONTRACT))),
("SCRAPING", pd.DataFrame(columns=ALL_COLUMNS_NEW_CONTRACT)))]

writer = pd.ExcelWriter(root_path, engine='xlsxwriter')
for name, group in list_sheets:
    for
column in group.columns:
group[column] = group[column].astype(str)
group.to_excel(writer, sheet_name=name, index=False)
writer.save()
````




## Extract
````python
xl = pd.ExcelFile('foo.xls')
xl.sheet_names  # see all sheet names
xl.parse(sheet_name)  # read a specific sheet to DataFrame

pd.read_excel(self.file, sheet_name=self.sheet_name, skiprows=self.skip_rows, dtype=str)
pd.read_excel('path_to_file.xls', 'Sheet1', index_col=None, na_values=['NA'])
pd.read_excel('path_to_file.xls', header=None, # pour ne pas mettre la premiere ligne en colonne
              names=names = [20140109, 20140213, 20140313, 20140410, 20140508, 20140612, 20140714]) # pour mettre le nom des colonnes
````

# Json

## Functions
````python
import json


def get_json_as_dict(path_file: str) -> dict:
    with open(path_file) as json_file:
        return json.load(json_file)


def save_dict_as_json(dict_to_save_as_json: dict, path_file_json_to_save: str) -> None:
    with open(path_file_json_to_save, 'w') as outfile:
        json.dump(dict_to_save_as_json, outfile)

pandas.read_csv


df.to_csv(index=False)
````

## Json Schema

[Documentation](https://json-schema.org/draft/2019-09/json-schema-validation.html#rfc.section.6.1.3)

[Json to Json Schema Converter](https://www.liquid-technologies.com/online-json-to-schema-converter)

[how apply it in python code](https://pynative.com/python-json-validation/)


### enum
for "status" with value possible [10, 20, 23, 27]
```json
{
  "status": 10
}
```
do
````json
{
    "type" : "object",
    "required" : ["status"],
    "properties" : {
        "status" : {
            "type" : "number",
            "enum" : [10, 20, 23, 27]
        }
    }
}
````

[use ref](https://stackoverflow.com/questions/20154854/merge-two-json-schemas)
