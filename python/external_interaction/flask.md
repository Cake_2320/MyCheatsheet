https://stackoverflow.com/questions/27516480/encapsulate-flask-api-in-class-for-testing-purposes


You can replace the use of @app.route with add_url_rule.

To put it in an example:

```python
from flask import Flask

class MyFlaskApp:

    def __init__(self):
        self.app = Flask(__name__)
        self.app.add_url_rule('/', 'index', self.index)

    def index(self):
        pass
```


Which is similar to:

```python
from flask import Flask
app = Flask(__name__)

@app.route('/index')
def index():
    pass
```
