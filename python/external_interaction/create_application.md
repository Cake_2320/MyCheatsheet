# pyinstaller





command -w : no console
https://github.com/ciscomonkey/flask-pyinstaller


## Getting latest version
```shell
pip install https://github.com/pyinstaller/pyinstaller/archive/develop.zip
```


## Launch
http://www.pyinstaller.org/documentation.html

https://pyinstaller.readthedocs.io/en/stable/usage.html
````shell
cd ./MyDirectory
pyinstaller myscript.py

pyinstaller options… ~/myproject/source/myscript.py
````

Options :
https://pyinstaller.readthedocs.io/en/stable/usage.html

`-F`, `--onefile`  : Create a one-file bundled executable.

`-c`, `--console`, `--nowindowed` : Open a console window for standard i/o (default). On Windows this option has no effect if the first script is a ‘.pyw’ file. 

`-w`, `--windowed`, `--noconsole` : Windows and Mac OS X: do not provide a console window for standard i/o. On Mac OS this also triggers building a Mac OS .app bundle. On Windows this option is automatically set if the first script is a ‘.pyw’ file. This option is ignored on *NIX systems.

> mieux vaut ne pas avoir de console : `-w`


### writing difference with Mac/linux and Windows
Windows : `pyinstaller -w -F --add-data "templates;templates" --add-data "static;static" app.py`
Linux : `pyinstaller -w -F --add-data "templates:templates" --add-data "static:static" app.py`


## Launch pyinstaller from python code
https://pyinstaller.readthedocs.io/en/stable/usage.html#running-pyinstaller-from-python-code

If you want to run PyInstaller from Python code, you can use the run function defined in PyInstaller.__main__. For instance, the following code:
````python
import PyInstaller.__main__

PyInstaller.__main__.run([
    'my_script.py',
    '--onefile',
    '--windowed'
])
````

Is equivalent to:
````shell
pyinstaller my_script.py --onefile --windowed
````

also :
````python
PyInstaller.__main__.run([
    '--name=%s' % package_name,
    '--onefile',
    '--windowed',
    '--add-binary=%s' % os.path.join('resource', 'path', '*.png'),
    '--add-data=%s' % os.path.join('resource', 'path', '*.txt'),
    '--icon=%s' % os.path.join('resource', 'path', 'icon.ico'),
    os.path.join('my_package', '__main__.py'),
])
````

also:
````python
import subprocess

if create_exe == True:
   subprocess.call(r"python -m PyInstaller -F --name test --add-data 
   templates;templates --add-data static;static --icon=icon.ico path\to\test.py")
else:
   pass
````

## Using Spec files
https://pyinstaller.readthedocs.io/en/stable/spec-files.html

For many uses of PyInstaller you do not need to examine or modify the spec file. It is usually enough to give all the needed information (such as hidden imports) as options to the pyinstaller command and let it run.

There are four cases where it is useful to modify the spec file:
- When you want to bundle data files with the app.
- When you want to include run-time libraries (.dll or .so files) that PyInstaller does not know about from any other source.
- When you want to add Python run-time options to the executable.
- When you want to create a multiprogram bundle with merged common modules.
After you have created a spec file and modified it as necessary, you build the application by passing the spec file to the pyinstaller command:


1. You create a spec file using this command:
````shell
pyi-makespec options name.py [other scripts …]
````
The options are the same options documented above for the pyinstaller command. This command creates the name.spec file but does not go on to build the executable.

2. After you have created a spec file and modified it as necessary, you build the application by passing the spec file to the pyinstaller command
````shell
pyinstaller options name.spec
````

Only the following command-line options have an effect when building from a spec file:
```
--upx-dir
--distpath
--workpath
--noconfirm
--ascii
--clean
```

## Flask : exemple with Flask and pyinstaller
https://github.com/ciscomonkey/flask-pyinstaller


# py2app and py2wexe
