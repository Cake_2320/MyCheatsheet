import win32api
print(win32api.FormatMessage(-2147221008))

https://github.com/xlwings/xlwings/issues/1375
https://docs.xlwings.org/en/stable/threading_and_multiprocessing.html

## win32com

"If it's running in a thread, you need to call pythoncom.CoInitialize yourself."

````python
import win32com.client

speaker = win32com.client.Dispatch("SAPI.SpVoice")
speaker.Speak("It works. Hoorah!")
````

1. `win32com.client.Dispatch` ne marchait pas
2. `win32com.client.gencache.EnsureDispatch` marchait mais en changeant de machine le cache n'etait plus trouvé
3. l'option `clsctx=pythoncom.CLSCTX_LOCAL_SERVER` a tout débloqué
````python
def get_excel_instance():
    # create excel object
    pythoncom.CoInitialize()
    excel = win32com.client.Dispatch('Excel.Application', clsctx=pythoncom.CLSCTX_LOCAL_SERVER)
    # excel = win32com.client.gencache.EnsureDispatch('Excel.Application')
    # excel = win32com.client.Dispatch("Word.Application")
    return excel

excel = get_excel_instance()
excel.quit()
````

Other answer find: "OK in case anyone needs this I solved it via deleting the 'gens' folder from Python/Lib/site-packages/win32com/ and then prefacing the Dispatch call like so:"
```python
wi32com.client.gencache.is_readonly=False
win32com.client.gencache.GetGeneratePath()
pythoncom.CoInitialize()
xl = Dispatch('Excel.Application', clsctx = pythoncom.CLSCTX_LOCAL_SERVER)
```

#### run with flask
```python
def _run_flask(self, host, port, debug=False, using_win32=False):
    print host
    if using_win32:
        import pythoncom
        pythoncom.CoInitialize()
    self.flask_app.run(debug=debug, host=host, port=port, use_reloader=False)
```


#### win32com with multithreading
https://newbedev.com/using-win32com-with-multithreading
https://pretagteam.com/question/using-win32com-with-multithreading
https://stackoverflow.com/questions/26764978/using-win32com-with-multithreading/27966218#27966218


````python
import pythoncom

def onThreadStart(threadIndex):
  pythoncom.CoInitialize()

cherrypy.engine.subscribe('start_thread', onThreadStart)
````