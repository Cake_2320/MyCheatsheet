# HTTP

````python
url_logic_apps_07 = url_logic_apps_07_QA

dict = {
    self.DICT_PARAM_RUN_DATETIME: self.run_datetime,
    self.DICT_DATA_HISTORY_DETAILS: json_in_file[self.DICT_DATA_HISTORY_DETAILS]
}

requests.post(url_logic_apps_07, json=dict)
requests.post(url_logic_apps_07, data=dict)
````

## Other

test call of a http through terminal:

```shell
curl -X GET 'http://jsonplaceholder.typicode.com/todos'
```

return a json

```python
# Si vous devez écrire du code compatible avec Python2 et Python3, vous pouvez utiliser le programme d’importation suivant.

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse
```

`urllib a été scindé en `urllib.parse`, `urllib.request`, et `urllib.error` in Python 3.

# Azure Function

[Doc](https://docs.microsoft.com/en-us/python/api/azure-functions/azure.functions.httpresponse?view=azure-python)

````python
import azure.functions as func
import io
import pandas as pd


def main(req: func.HttpRequest) -> func.HttpResponse:
    req.get_body()
    req.get_json()
    name = req.params.get('name')

    a = req.form['asset']
    b = req.files["file"]
    for input_file in req.files.values():
        pass

    with io.BytesIO(json_input_file) as f:
        xl = pd.ExcelFile(f)
````

```python
url = 'https://<file_upload_url>'
fp = '/Users/jainik/Desktop/data.csv'

files = {'file': open(fp, 'rb')}
payload = {'file_id': '1234'}

response = requests.put(url, files=files, data=payload, verify=False)
```

````python
import requests

site = 'https://prnt.sc/upload.php'  # the site where you upload the file
filename = 'image.jpg'  # name example

up = {'image': (filename, open(filename, 'rb'), "multipart/form-data")}

data = {
    "Button": "Submit",
}

request = requests.post(site, files=up, data=data)
````
