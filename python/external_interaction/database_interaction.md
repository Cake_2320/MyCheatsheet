## Connexion

```python
import logging

import psycopg2
import sqlalchemy as db
from azure.identity import DefaultAzureCredential

from config.load_bdd_env_var import DATABASE, DB_USER_NAME, DB_PASSWORD, DB_HOST, DB_PORT, DB_DIALECT, DRIVER, ssl_args,

PASSWORD_OR_TOKEN, TOKEN_ADDRESS


class DB_connection:
    def __init__(self):
        logging.debug('init ForecastPipeline Class')

        if PASSWORD_OR_TOKEN == 'password':
            logging.info('connect to SQL DB by password')
            password = DB_PASSWORD
        elif PASSWORD_OR_TOKEN == 'token':
            logging.info('connect to SQL DB with token')
            password = DefaultAzureCredential().get_token(TOKEN_ADDRESS).token

        self.conn = psycopg2.connect(dbname=DATABASE, user=DB_USER_NAME, password=password, host=DB_HOST, port=DB_PORT,
                                     sslmode='require')
        self.engine = db.create_engine(DB_DIALECT + '+' + DRIVER + '://' + DB_USER_NAME + ':' + password + '@' +
                                       DB_HOST + ':' + DB_PORT + '/' + DATABASE,
                                       connect_args=ssl_args)
        self.connection = self.engine.connect()
        self.metadata = db.MetaData()
```

````python

from sqlalchemy.orm import sessionmaker
from sqlalchemy import Table

Session = sessionmaker(bind=engine)

session.query(SomeMappedClass)

Session.configure(bind=engine)  # once engine is available

session = Session()

address_table = Table('address', metadata, autoload=True)
# addresses = session.query(address_table).filter(address_table.c.retired == 1)
addresses = session.query(address_table)
addresses.delete(synchronize_session=False)

d = addresses_table.delete().where(addresses_table.c.retired == 1)
d.execute()

from sqlalchemy import delete

delete(user_table)

######
users = models.User.query.all()
models.db.session.delete(users)

models.User.query.delete()

####
Conn = dbengine.connect()
Conn.exectute("TRUNCATE TABLE tablename")
````

````python
database_connexion.connection.execute(f"TRUNCATE {table_name};")

database_connexion.connection.execute(f"DELETE FROM {table_name};")

data_to_save.to_sql(name=table_name, con=database_connexion.engine, if_exists="append", index=False)
````

## Delete Rows

PostGre Okay

````python
database_connexion = DB_connection()
database_connexion.connection.execute(f"TRUNCATE {table_name};")
database_connexion.connection.execute(f"DELETE FROM {table_name};")
````

Not working

````python
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Table

database_connexion = DB_connection()
Session = sessionmaker(bind=database_connexion.engine)
Session.configure(bind=database_connexion.engine)  # once engine is available
session = Session()
address_table = Table(table_name, database_connexion.metadata, autoload=True)
addresses = session.query(address_table)
addresses.delete(synchronize_session=False)
````

# pymssql

````python
conn = pymssql.connect(host="Dev02", database="DEVDb")
cur = conn.cursor()
query = """INSERT INTO dbo.SCORE_TABLE(index, column1, column2, ..., column20)
            VALUES (?, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 
            %s, %s, %s, %s, %s, %s)"""
cur.executemany(query, df_sql)
conn.commit()
cur.close()
conn.close()
````

# SQL Alchemy

[SQL Alchemy](https://realpython.com/python-sqlite-sqlalchemy)

## engine-connection-and-session-difference

[link](https://stackoverflow.com/questions/34322471/sqlalchemy-engine-connection-and-session-difference/34364247#34364247)

If you're using the ORM functionality, use session; if you're only doing straight SQL queries not bound to objects,
you're probably better off using connections directly.

## Connect

````python
import sqlalchemy
from sqlalchemy import create_engine, MetaData, inspect

# Connect
user = ""
password = ""
server = ""
port = ""
database = ""
sql_alchemy_url_connexion = f"mssql+pymssql://{user}:{password}@{server}:{port}/{database}?charset=utf8"
engine = create_engine(sql_alchemy_url_connexion, echo=False)
conn = self.engine.connect()

# Disconect
conn.invalidate()
engine.dispose()
````

## Delete

```python
from sqlalchemy import delete

stmt = (
    delete(user_table).
    where(user_table.c.id == 5)
)
```

## insert

```python
from sqlalchemy import delete

stmt = (
    insert(user_table).
    values(name='username', fullname='Full Username')
)
```

[pandas to_sql](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_sql.html)

## commit gestion

```python
from sqlalchemy.orm import sessionmaker

Session = sessionmaker(bind=engine)
session = Session()
session.execute('''TRUNCATE TABLE tempy''')
session.commit()
session.close()
```

```python
with engine.connect() as con:
    con.execution_options(autocommit=True).execute("TRUNCATE TABLE foo;")
```

[link](https://www.sqlshack.com/introduction-to-sqlalchemy-in-pandas-dataframe/)

````python
import pandas
import sqlalchemy

# Create the engine to connect to the PostgreSQL database
engine = sqlalchemy.create_engine('postgresql://postgres:test1234@localhost:5432/sql-shack-demo')

# Read data from CSV and load into a dataframe object
data = pandas.read_csv('C:/temp/pandas-db-sqlshack-demo/pandas-env/superstore.csv')

# Write data into the table in PostgreSQL database
data.to_sql('superstore', engine)
````

## run SQL command

````python
sql_engine.execute(sql_command)
````

### Get result

````python
engine = create_engine('postgresql://kyle.pekosh@localhost:5432/testload')
connection = engine.connect()
result = connection.execute(sql_command).all()
````

- `cr.dictfetchall()` will give you all the matching records in the form of ** list of dictionary** containing key,
  value.
    - Example : cr.dictfetchall() will give you [{'reg_no': 123},{'reg_no': 543},].
- `cr.dictfetchone()` works same way as cr.dictfetchall() except it returns only single record.
    - Example : cr.dictfetchone() will give you {'reg_no': 123}.
- `cr.fetchall()` will give you all the matching records in the form of list of tupple.
    - Example : cr.fetchall() will give you '[(123),(543)]'.
- `cr.fetchone()` works same way as cr.fetchall() except it returns only single record.
    - Example : cr.fetchone() will give you '(123)'.

## Scan Schema

````python
from sqlalchemy import MetaData, inspect


def scan_schema(sql_engine):
    """
    Scan SQL Database schema & table

    :param sql_engine: sql alchemy engine

    :return: dict of tables in metadata, list of table name
    """

    meta_data = MetaData(bind=sql_engine)

    return meta_data.tables, inspect(sql_engine).get_table_names()
````

## Describing Databases with MetaData

https://docs.sqlalchemy.org/en/14/core/metadata.html#module-sqlalchemy.schema

````python
from sqlalchemy import *

metadata = MetaData()

user = Table('user', metadata,
             Column('user_id', Integer, primary_key=True),
             Column('user_name', String(16), nullable=False),
             Column('email_address', String(60)),
             Column('nickname', String(50), nullable=False)
             )
````

````python
from sqlalchemy import *

metadata = MetaData()

user = Table('user', metadata,
             Column('user_id', Integer, primary_key=True),
             Column('user_name', String(16), nullable=False),
             Column('email_address', String(60)),
             Column('nickname', String(50), nullable=False)
             )
````

````python
class ReservoirProfilData(BaseDCM):
    """
    sql alchemy table object for video performance data
    """
    __tablename__ = "reservoir_profile_data"
    __table_args__ = (
        PrimaryKeyConstraint('reservoir_profile', 'date', 'well_name', 'historical_flag'),
    )

    reservoir_profile = Column(Integer, ForeignKey('reservoir_profile.id'))
    date = Column(Date)
    import_date = Column(DateTime(timezone=False))
    well_name = Column(String(255))
    historical_flag = Column(Boolean)
    baseline_oil_prod = Column(Float(53))

    reservoir_profils = relationship("ReservoirProfil")
````

# py functions for SQL

### sql_insert_statement_from_dataframe

````python
def sql_insert_statement_from_dataframe(df: pd.DataFrame, table_name: str, replace_str_empty_by_null: bool = True):
    sql_texts = f"INSERT INTO {table_name}({', '.join(df.columns)}) VALUES "

    df = df.fillna('NULL')
    if replace_str_empty_by_null:
        df = df.replace('', 'NULL')

    list_values = []
    for index, row in df.iterrows():
        values = str(tuple(row.values)).replace("'NULL'", 'NULL')  # probably better but issues with datetime
        # below we made loops and we add a la mano les guillements, et puis du coup meme les chiffres sont entre guillemet 
        values = "(" + ("'" + "', '".join([str(val) for val in row.values]) + "'").replace("'NULL'", 'NULL') + ")"

        list_values.append(values)

    return sql_texts + ", ".join(list_values) + ";"

````

tests

````python
import unittest

import numpy as np
import pandas as pd


class TestGlobalBddFunctions(unittest.TestCase):

    def test_sql_insert_statement_from_dataframe(self):
        # Given
        df = pd.DataFrame([
            {'facility': "Baltic", 'entity': "JV DCD"},
            {'facility': "Baltic", 'entity': ''},
            {'facility': "Baltic", 'entity': None},
            {'facility': "Baltic", 'entity': np.nan},
            {'facility': "Baltic", 'entity': pd.to_datetime('2022-02-17', format="%Y-%m-%d")},
        ])

        # When
        result = sql_insert_statement_from_dataframe(table_name='dbo.planning_event', df=df)

        # Expected
        result_expected = (
            "INSERT INTO dbo.planning_event(facility, entity) VALUES "
            "('Baltic', 'JV DCD'), "
            "('Baltic', NULL), "
            "('Baltic', NULL), "
            "('Baltic', NULL), "
            "('Baltic', '2022-02-17 00:00:00');"
        )

        # Then
        self.assertEqual(result_expected, result)

    def test_sql_insert_statement_from_dataframe_2(self):
        # Given
        df = pd.DataFrame([
            {'facility': "Baltic", 'entity': "JV DCD"},
            {'facility': "Baltic", 'entity': ''},
            {'facility': "Baltic", 'entity': None},
            {'facility': "Baltic", 'entity': np.nan},
        ])

        # When
        result = sql_insert_statement_from_dataframe(
            table_name='dbo.planning_event', df=df, replace_str_empty_by_null=False)

        # Expected
        result_expected = (
            "INSERT INTO dbo.planning_event(facility, entity) VALUES "
            "('Baltic', 'JV DCD'), "
            "('Baltic', ''), "
            "('Baltic', NULL), "
            "('Baltic', NULL);"
        )

        # Then
        self.assertEqual(result_expected, result)

    def test_sql_insert_statement_from_dataframe_3(self):
        # Given
        df = pd.DataFrame([
            {'facility': "Baltic", 'entity': "JV DCD", '1': None},
            {'facility': "Baltic", 'entity': '', '1': None},
            {'facility': "Baltic", 'entity': None, '1': None},
            {'facility': "Baltic", 'entity': np.nan, '1': None},
        ])

        # When
        result = sql_insert_statement_from_dataframe(
            table_name='dbo.planning_event', df=df, replace_str_empty_by_null=False)

        # Expected
        result_expected = (
            "INSERT INTO dbo.planning_event(facility, entity, 1) VALUES "
            "('Baltic', 'JV DCD', NULL), "
            "('Baltic', '', NULL), "
            "('Baltic', NULL, NULL), "
            "('Baltic', NULL, NULL);"
        )

        # Then
        self.assertEqual(result_expected, result)
````