# Index

````python
df.set_index(pd.DatetimeIndex(df['Datetime']))
# or
df = df.set_index('Datetime')
````

# Timeseries

## define timeserie

```python
pd.DataFrame(columns=['value', 'shortfall_type'],
             index=pd.DatetimeIndex([], name=None, freq=None))
```

create empty dataframe with frequence

```python
df = pd.DataFrame(columns=['a', 'b'], index=pd.to_datetime([]))
df = pd.DataFrame(columns=['a', 'b'], index=pd.DatetimeIndex([], name='startime', freq='d'))
df = pd.DataFrame(columns=['a', 'b'], index=pd.TimedeltaIndex([]))

dates = pd.date_range(min(actual_min_range, min_date), max(actual_max_range, max_date), freq='D')
df.reindex(dates)
```

## Freq gestion

```python
import pandas as pd

df.asfreq('d')
df.set_index('Date', inplace=True)
DATE_FORMAT = "%Y-%m-%d"
df = pd.DataFrame([
    {'date': "2022-04-01", 'value': 10},
])
pd.to_datetime(df['col'], infer_datetime_format=True)  # infer detect automatiquement le format
df_expected = df_expected
.set_index(pd.DatetimeIndex(pd.to_datetime(list(df_expected['date']), format=DATE_FORMAT)), freq='d')
.drop(columns=["date"])

df1 = df.astype({'date': 'datetime64[ns]',
                 'value': int})
.set_index(pd.DatetimeIndex('date', freq=None))
df2 = df

# Equivalent
pd.to_datetime(df2['date'], format="%Y-%m-%d")
df['Date'].astype('datetime64[ns]')

df = pd.read_csv('data.csv', parse_dates=['Date'], index_col=['Date'])

# parse_dates is supported by most of the read_ * methods:
```

### functions

```python
def set_datetime_index_from_column(df: pd.DataFrame, datetime_column_for_index,
                                   date_format: str = "%Y-%m-%d", freq=None) -> pd.DataFrame:
    if datetime_column_for_index not in df.columns:
        if df.empty:
            return df
        raise ValueError(f"column {datetime_column_for_index} is not in the dataframe, "
                         "so it can't be set as datetime index")
    return df.set_index(
        pd.DatetimeIndex(pd.to_datetime(df[datetime_column_for_index], format=date_format), freq=freq)
    ).drop(columns=[datetime_column_for_index]).rename_axis(None)


set_datetime_index_from_column(df=df, datetime_column_for_index="date", freq='d')
```

Tests

```python
class TestSetDatetimeIndexFromColumn:

    def test_with_empty_dataframe(self):
        # Given
        empty_dataframe = pd.DataFrame(columns=['date', 'value'])

        # When
        result = set_datetime_index_from_column(df=empty_dataframe, datetime_column_for_index='date')

        # Expected
        result_expected = pd.DataFrame(columns=['value'], index=pd.DatetimeIndex([], name=None, freq=None))

        # Then
        pd.testing.assert_frame_equal(result_expected, result)
```

## Resample : find missing elements into timeseries

[Pandas Documentation](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.resample.html)

```
Available options:

B       business day frequency
C       custom business day frequency (experimental)
D       calendar day frequency
W       weekly frequency
M       month end frequency
SM      semi-month end frequency (15th and end of month)
BM      business month end frequency
CBM     custom business month end frequency
MS      month start frequency
SMS     semi-month start frequency (1st and 15th)
BMS     business month start frequency
CBMS    custom business month start frequency
Q       quarter end frequency
BQ      business quarter endfrequency
QS      quarter start frequency
BQS     business quarter start frequency
A       year end frequency
BA      business year end frequency
AS      year start frequency
BAS     business year start frequency
BH      business hour frequency
H       hourly frequency
T       minutely frequency
S       secondly frequency
L       milliseonds
U       microseconds
N       nanoseconds
```

```python
index = pd.date_range('1/1/2000', periods=9, freq='T')
series = pd.Series(range(9), index=index)
series.resample('3T').sum()
```

to a smaller timestep (bigger timeseries)

````python
df = df.set_index(pd.DatetimeIndex(df['date']))
.drop(columns=["date"])
.resample('1D').mean().interpolate()
````

````python
df.set_index(pd.DatetimeIndex(df['date']))
.drop(columns=["date"])
.resample('1D').asfreq()
````

````python
# using the resample method
df.set_index(df.date, inplace=True)
df = df.resample('D').sum().fillna(0)

# fill values with that
idx = pd.period_range(min(df.date), max(df.date))
results.reindex(idx, fill_value=0)
````

[ways of fill empty data in timeseries](https://towardsdatascience.com/filling-gaps-in-time-series-data-2db7366f1965)
[Examples](https://machinelearningmastery.com/resample-interpolate-time-series-data-python/)
[Example filling gap](https://towardsdatascience.com/filling-gaps-in-time-series-data-2db7366f1965)
