# Hinting

````python
from typing import AnyStr, Callable
from numbers import Number


def my_function(name: AnyStr, func: Callable) -> None:


Callable[[ParamType1, ParamType2,.., ParamTypeN], ReturnType]
Callable[[int, int], int]
````

````python
typing.Dict[str, pd.DataFrame]  # typing.Dict[key_type, value_type]
````

````python
from typing import List, Callable, Union, IO
from pathlib import Path


def function(self):
    self.file: Union[str, Path, IO, None]  # Union[X, Y] means either X or Y
    after_prop: List[str]
    choices: dict
    apply_function: Callable[[tuple], bool]
````

## Optional

````python
Optional[str]
Union[str, None]
````

## List of hinting

https://mypy.readthedocs.io/en/stable/cheat_sheet_py3.html

global

````python
from typing import List, Set, Dict, Tuple, Optional, Mapping
from numbers import Number

# For simple built-in types, just use the name of the type
x: int = 1
x: float = 1.0
x: bool = True
x: str = "test"
x: bytes = b"test"

# For collections, the type of the collection item is in brackets
# (Python 3.9+)
x: list[int] = [1]
x: set[int] = {6, 7}

# In Python 3.8 and earlier, the name of the collection type is
# capitalized, and the type is imported from the 'typing' module
x: List[int] = [1]
x: Set[int] = {6, 7}

# Same as above, but with type comment syntax (Python 3.5 and earlier)
x = [1]  # type: List[int]

# For mappings, we need the types of both keys and values
x: dict[str, float] = {"field": 2.0}  # Python 3.9+
x: Dict[str, float] = {"field": 2.0}

# For tuples of fixed size, we specify the types of all the elements
x: tuple[int, str, float] = (3, "yes", 7.5)  # Python 3.9+
x: Tuple[int, str, float] = (3, "yes", 7.5)

# For tuples of variable size, we use one type and ellipsis
x: tuple[int, ...] = (1, 2, 3)  # Python 3.9+
x: Tuple[int, ...] = (1, 2, 3)

# Use Optional[] for values that could be None
x: Optional[str] = some_function()
# Mypy understands a value can't be None in an if-statement
if x is not None:
    print(x.upper())
# If a value can never be None due to some invariants, use an assert
assert x is not None
print(x.upper())

Mapping[str, str]
````

````python
import typing

typing.AnyStr
typing.Callable
typing.Callable[[ParamType1, ParamType2,.., ParamTypeN], ReturnType]
````

## hinting itself

[How do I type hint a method with the type of the enclosing class?](https://stackoverflow.com/questions/33533148/how-do-i-type-hint-a-method-with-the-type-of-the-enclosing-class)

```python
# Python 3.7+: from __future__ import annotations
from __future__ import annotations


class Position:
    def __add__(self, other: Position) -> Position:
        ...


-  # Python <3.7: use a string


class Position:
    ...

    def __add__(self, other: 'Position') -> 'Position':
        ...
```

## hinting abstract class for substract

````python
from typing import Type


def get_foo(foo_algorithm: Type[Algorithm]):  # with Algorithm the mother abstract class
    return foo_algoritm().foo
````

# String Manipulation

## Standard String manipulation

### Replace (find and replace)

````python
string.replace(old, new, count)
````

`old` – old substring you want to replace.
`new` – new substring which would replace the old substring.
`count` – the number of times you want to replace the old substring with the new substring. (Optional )

### Pandas dataframe

[doc](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.str.lower.html)

````python
dataframe['employee_id'].str.strip()  # strip = retire les espaces au debut et a la fin
dataframe['employee_id'].str.lower()
dataframe['employee_id'].str.upper()
dataframe['employee_id'].str.replace(" ", "")
````

### Split

```python
string.split(separator, maxsplit)

txt = "welcome to the jungle"
x = txt.split()
```

## Regular expression

### REGEX

https://www.programiz.com/python-programming/regex

````python
import re

pattern = '^a...s$'
test_string = 'abyss'
result = re.match(pattern, test_string)
if result:
    print("Search successful.")
else:
    print("Search unsuccessful.")

bool(result)
````

### valid REGEX with NA tolerance or not

````python
regex = '^[0-9]$'
nan_cond = ""
if nan_accept_condition == True:
    nan_cond = "|nan"
new_regex = regex + nan_cond
````

OR

```python
if null_tolerance:
    s_boolean_test = s_boolean_test | self.df[self.current_variable].isnull()
```

### find substring in string

````python
if substring in fillstring
    try:
        fullstring.find(substring)  # fail si pas dedans
if fullstring.find(substring) != -1  # si dedans renvoie l'index
    if re.search(substring, fullstring)

# for list
mylist = ["dog", "cat", "wildcat", "thundercat", "cow", "hooo"]
r = re.compile(".*cat")
newlist = list(filter(r.match, mylist))  # Read Note below
````

## String insertion

### security, Token and text ingestion

````python
'token {}'.format(myToken)
"Bearer {}".format(token)
````

https://docs.python.org/fr/3.5/library/string.html

````python
'--add-data=%s' % os.path.join('resource', 'path', '*.txt')

("""
   Base case oil rate: %s stb/d
""" % 3
````

Modifié dans la version 3.1: Les spécificateurs de position d’argument peuvent être omis. Donc '{} {}' est équivalent
à '{0} {1}'

````python
"Creating table %s" % table_name  # %s = string, %d = decimal
"Shepherd %s is %d years old." % (shepherd, age)
"First, thou shalt count to {0}"  # References first positional argument
"Bring me a {}"  # Implicitly references the first positional argument
"From {} to {}"  # Same as "From {0} to {1}"
f"My quest is {name}"  # References keyword argument 'name'
f"Shepherd {shepherd} is {age} years old."
f"My quest is {name}".format(name="albert")
"Number {:03d} is here.".format(11)  # Number 011 is here.
'A formatted number - {:.4f}'.format(.2)  # 'A formatted number - 0.2000'
"Weight in tons {0.weight}"  # 'weight' attribute of first positional arg
"Units destroyed: {players[0]}"  # First element of keyword argument 'players'.

"Harold's a clever {0!s}"  # Calls str() on the argument first
"Bring out the holy {name!r}"  # Calls repr() on the argument first
"More {!a}"  # Calls ascii() on the argument first
````

## File system and Path Gestion

Le python path indique à python quels dossiers il doit prendre en compte pour sa recherche de modules. Vous pouvez voir
cette liste de cette manière:

```python
>> > import sys
>> > sys.path
['/usr/lib/python2.7', '/usr/lib/python2.7/plat-x86_64-linux-gnu', '/usr/lib/python2.7/lib-tk',
 '/usr/lib/python2.7/lib-old', '/usr/lib/python2.7/lib-dynload', '/usr/local/lib/python2.7/dist-packages',
 '/usr/lib/python2.7/dist-packages', '/usr/lib/python2.7/dist-packages/PILcompat',
 '/usr/lib/python2.7/dist-packages/gtk-2.0', '/usr/lib/python2.7/dist-packages/ubuntu-sso-client']
```

Vous ne pouvez donc pas faire d'import de module si celui-ci ne se trouve pas dans ces dossiers.

https://docs.python.org/3/library/os.path.html

````python
import os
import sys

sys.path.append(os.path.dirname(__file__))

os.path.dirname(__file__)
os.path.dirname(__name__)

path = '/home/User/Documents'
os.path.dirname(path)  # '/home/User'
os.path.basename(path)  # 'Documents'
path = '/home/User/Documents/file.txt'
os.path.dirname(path)  # '/home/User/Documents'
os.path.basename(path)  # 'file.txt'
path = 'file.txt'
os.path.basename(path)  # 'file.txt'

os.path.join(PATH_DIR_LE_WDR_DATA, 'EUROPORTE')

os.path.commonprefix(['/usr/lib', '/usr/local/lib'])
# '/usr/l'
os.path.commonpath(['/usr/lib', '/usr/local/lib'])
# '/usr'


for file in os.listdir(dirpath):
    pass
````

# Datetime

## Links

- [String to datetime](https://stackabuse.com/converting-strings-to-datetime-in-python/)
- [Table Value convert with percent (ISO)](https://www.programiz.com/python-programming/datetime/strptime)
- [other table value](https://dataindependent.com/pandas/pandas-to-datetime-string-to-date-pd-to_datetime/)

## Global

````python
import datetime
import datetime as dt

NUMBER_OF_SECOND_IN_ONE_DAY = (24 * 60 * 60)
date_time_obj = datetime.datetime.now()
date_time_obj.date()
date_time_obj.time()
print('Date:', date_time_obj.date())
print('Time:', date_time_obj.time())
print('Date-time:', date_time_obj)
dt.date.today()

dt.date.toordinal()  # get in numeric
dt.timedelta.seconds / NUMBER_OF_SECOND_IN_ONE_DAY

dt.timedelta.seconds  # If seconds become larger than 86400 (24*60*60), they will overflow into one day
dt.timedelta.total_seconds()

dt.datetime.replace(hour=0, minute=0, second=0)
````

### Get special day

````python
import datetime as dt
import pandas as pd

last_day_of_prev_month = dt.date.today().replace(day=1) - dt.timedelta(days=1)
start_day_of_prev_month = dt.date.today().replace(day=1) - dt.timedelta(days=last_day_of_prev_month.day)
pd.DateOffset(days=1)

# For printing results
print("First day of prev month:", start_day_of_prev_month)
print("Last day of prev month:", last_day_of_prev_month)
````

## Conversion

### Format

````python
"Jun 28 2018 at 7:40AM" -> "%b %d %Y at %I:%M%p"
"September 18, 2017, 22:19:55" -> "%B %d, %Y, %H:%M:%S"
"Sun,05/12/99,12:30PM" -> "%a,%d/%m/%y,%I:%M%p"
"Mon, 21 March, 2015" -> "%a, %d %B, %Y"
"2018-03-12T10:12:45Z" -> "%Y-%m-%dT%H:%M:%SZ"

"12-03-2018 10:12:45" -> "%d-%m-%Y %H:%M:%S"
````

- `%Y`: Year (4 digits)
- `%y`: Year (2 digits)
- `%m`: Month
- `%d`: Day of month
- `%H`: Hour (24 hour)
- `%M`: Minutes
- `%S`: Seconds
- `%f`: Microseconds

### String to Datetime

When no custom formatting is given, the default string format is used, i.e. the format for "2018-06-29 08:15:27.243860"
is in **ISO 8601**

```python
import datetime

print('Current date/time: {}'.format(datetime.datetime.now()))

date_time_str = '2018-06-29 08:15:27.243860'
date_time_obj = datetime.datetime.strptime(date_time_str,
                                           '%Y-%m-%d %H:%M:%S.%f')

print('Date:', date_time_obj.date())
print('Time:', date_time_obj.time())
print('Date-time:', date_time_obj)
```

For more here is the
documentation : [strptime documentation](https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior)

```python
strftime(format)  # Convert object to a string according to a given format
strptime(date_string, format)  # Parse a string into a datetime object given a corresponding format
```

**Pandas**

````python
pd.to_datetime(df['Dates'], format='%Y%m%d')
df_monitoring['date'] = df_monitoring['date'].dt.strftime('%d/%m/%Y')
df_monitoring_stat['date'] = pd.to_datetime(df_monitoring_stat['date'], format='%Y-%m-%d').dt.strftime('%d/%m/%Y')
````

### Date format

**pandas**

````python
df['just_date'] = df['dates'].dt.date
````

The above returns a datetime.date dtype, if you want to have a datetime64 then you can just normalize the time component
to midnight so it sets all the values to 00:00:00:

````python
df['normalised_date'] = df['dates'].dt.normalize()
````

This keeps the dtype as datetime64, but the display shows just the date value.

    pandas: .dt accessor
    pandas.Series.dt

### Date to Datetime

````python
import datetime

import numpy as np

start_date = datetime.date.today()
start_datetime_1 = datetime.datetime.fromordinal(start_date.toordinal())
start_datetime_2 = np.datetime64(start_date)
````

### More Packages

```python
from dateutil.parser import parse

date_array = [
    '2018-06-29 08:15:27.243860',
    'Jun 28 2018 7:40AM',
    'Jun 28 2018 at 7:40AM',
    'September 18, 2017, 22:19:55',
    'Sun, 05/12/1999, 12:30PM',
    'Mon, 21 March, 2015',
    '2018-03-12T10:12:45Z',
    '2018-06-29 17:08:00.586525+00:00',
    '2018-06-29 17:08:00.586525+05:00',
    'Tuesday , 6th September, 2017 at 4:30pm'
]

for date in date_array:
    print('Parsing: ' + date)
    dt = parse(date)
    print(dt.date())
    print(dt.time())
    print(dt.tzinfo)
    print('\n')

import maya  # !!!! Met en UTC par default si ne trouve pas le timezone
```

## Timezone

[List of timezone](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)

### Set Timezone

```python
import datetime as dt

dtime = dt.datetime.now()
print(dtime)
print(dtime.tzinfo)
```

tzinfo is None since it is a naive datetime object.  
For timezone conversion, a library called pytz is available for Python.

```python
import datetime as dt
import pytz

pytz.utc()
pytz.timezone('America/New_York')
pytz.timezone('Europe/London')

timezone_nw = pytz.timezone('America/New_York')
nw_datetime_obj = dt.datetime.now(timezone_nw)

timezone_london = pytz.timezone('Europe/London')
london_datetime_obj = nw_datetime_obj.astimezone(timezone_london)
```

### Converting Timezone

```python
import datetime as dt
import pytz

timezone_nw = pytz.timezone('America/New_York')
nw_datetime_obj = dt.datetime.now(timezone_nw)

timezone_london = pytz.timezone('Europe/London')
london_datetime_obj = nw_datetime_obj.astimezone(timezone_london)

print('America/New_York:', nw_datetime_obj)
print('Europe/London:', london_datetime_obj)
```

**Pandas**

````python
df.Date.dt.tz_localize('UTC').dt.tz_convert('Asia/Kolkata')
````

## Timedelta

````python
pd.to_timedelta('1 days 06:05:01.00003')
pd.to_timedelta(np.arange(5), unit='s')
````

    unitstr, optional:

    Denotes the unit of the arg for numeric arg. Defaults to "ns".
    Possible values:
        ‘W’
        ‘D’ / ‘days’ / ‘day’
        ‘hours’ / ‘hour’ / ‘hr’ / ‘h’
        ‘m’ / ‘minute’ / ‘min’ / ‘minutes’ / ‘T’
        ‘S’ / ‘seconds’ / ‘sec’ / ‘second’
        ‘ms’ / ‘milliseconds’ / ‘millisecond’ / ‘milli’ / ‘millis’ / ‘L’
        ‘us’ / ‘microseconds’ / ‘microsecond’ / ‘micro’ / ‘micros’ / ‘U’
        ‘ns’ / ‘nanoseconds’ / ‘nano’ / ‘nanos’ / ‘nanosecond’ / ‘N’

### to integer for operations

````python
timedelta_series.dt.days
````

## Add

add month

````python
pd.DateOffset(months=i)
df.date + pd.DateOffset(months=plus_month_period)
df.date + pd.offsets.MonthOffset(plus_month_period)
````

## DatetimeIndex

### filtering

````python
df[(df.index >= start_datetime) &
   (df.index < realised_end_datetime)]
````