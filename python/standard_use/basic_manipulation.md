# to order

````python
DATE_FORMAT = "%Y-%m-%d"
pd.to_datetime('2022-01-01', format=DATE_FORMAT) + MonthEnd(0)  # get the end of the month
````

# Path

````python
import os
from pathlib import Path

root_dir = Path(__file__).parent.parent
````

# Environment variable

````python
import os

# Set environment variables
os.environ['API_USER'] = 'username'
os.environ['API_PASSWORD'] = 'secret'

# Get environment variables
USER = os.getenv('API_USER')
PASSWORD = os.environ.get('API_PASSWORD')

# Getting non-existent keys
FOO = os.getenv('FOO')  # None
BAR = os.environ.get('BAR')  # None
BAZ = os.environ['BAZ']  # KeyError: key does not exist.
````

# Python Rule

## Loop / iterate

````python
for number in range(10):
    if number == 5:
        break  # break here

    print('Number is ' + str(number))

print('Out of loop')

for number in range(10):
    if number == 5:
        continue  # continue here

for number in range(10):
    if number == 5:
        pass  # pass here
````

L'instruction pass qui se produit après l'instruction conditionnelle if indique au programme de continuer à exécuter la
boucle et d'ignorer le fait que le number de variables est évalué comme étant équivalent à 5 pendant l'une de ses
itérations.

````python
for elem in list_elem:
    if False:
        break  # stops the loop
        continue  # go to next elem of the loop
````

````python
for elem in list_elem:
    pass
````

###### Dataframe

````python
for index, row in df.iterrows():
    pass

for column in df:
    print(df[column])

for name, values in df.iteritems():
    print('{name}: {value}'.format(name=name, value=values[0]))

for (index, colname) in enumerate(df):
    print(index, df[colname].values)
````

###### on dictionaries

```python
for key in a_dict:
    print(key)

for item in a_dict.items():  # sort des tuples...
    print(item)

for key, value in a_dict.items():
    print(key, '->', value)
```

# Create

## Date & Datetime

````python
import datetime

datetime.datetime.strptime('24052010', "%d%m%Y").date()
datetime.date(2010, 5, 24)
````

## Set

frozenset can't be modified

````python
fs = frozenset((1, 2, 3))
````

set is like list but not organised/ordered and without double values

````python
a = {'a', 'b', 1}
a.add('c')
a.remove('a')
a.discard('a')  # same effect as remove ;
# The benefit of this approach (discard) over the remove method is if you try 
# to remove a value that is not part of the set, you will not get a KeyError
a.pop()  # remove and return an arbitrary value from a set
a..clear()  # remove all values
type(sorted(dataScientist))  # > list
dataScientist.union(dataEngineer)  # merge 2 set
dataScientist.intersection(dataEngineer)  # Equivalent Result for dataScientist & dataEngineer
dataScientist.isdisjoint(dataEngineer)  # These sets have elements in common so it would return False
dataScientist.difference(dataEngineer)  # Equivalent Result for dataScientist - dataEngineer
dataScientist.symmetric_difference(
    dataEngineer)  # contraire de 'union' ; Equivalent Result for dataScientist ^ dataEngineer

{skill for skill in ['GIT', 'PYTHON', 'SQL'] if skill not in {'GIT', 'PYTHON', 'JAVA'}}  # comprehensive set
mySkills.issubset(possibleSkills)  # return True or False

immutableSet = frozenset()  # Initialize a frozenset
nestedSets = set([frozenset()])  # You can make a nested

# initialize empty set
set()
# initialize empty dict
dict()
{}
````

## List

list of single item repeated N times

```python
[e] * n
```

### flatten list of list

````python
def flatten(l) -> list:
    return [item for sublist in l for item in sublist]


class TestFlatten:

    def test_with_empty_dataframe(self):
        # Given
        list_of_list = [['value_1', 'value_2'], ['value']]

        # When
        result = flatten(l=list_of_list)

        # Expected
        result_expected = ['value_1', 'value_2', 'value']

        # Then
        assert result_expected == result
````

## dict

### DefaultDict

[doc collection](https://docs.python.org/fr/3/library/collections.html)

if a key is called but doesn't exist, returned the default value

````python
from collections import defaultdict

somedict = {}
print(somedict[3])  # KeyError

someddict = defaultdict(int)
print(someddict[3])  # print int(), thus 0
````

````python
from collections import defaultdict

# default value Should be callable
defaultdict(int)
defaultdict(lambda: None)
defaultdict(lambda: np.nan)

a = defaultdict(dict)
a['aze'].update({})
````

## Dataframe

````python
# empty with columns
pd.DataFrame(columns=["comment", "id"])
dfObj = pd.DataFrame(columns=['User_ID', 'UserName', 'Action'])
pd.DataFrame(columns=['value', 'shortfall_type'],
             index=pd.DatetimeIndex([], name=None, freq=None))

map_wagon_number_x_pool = pd.DataFrame([
    {'wagon_number': 100000000001, 'business_unit': 'TD', 'product_type': 'BMO'},
    {'wagon_number': 100000000002, 'business_unit': 'TD', 'product_type': 'BMO'},
])

df = pd.DataFrame(list(zip(lst, lst2)),
                  columns=['Name', 'val'])
````

````python
import numpy as np

df = pd.DataFrame(np.arange(25).reshape(5, 5),
                  index=list('abcde'),
                  columns=['x', 'y', 'z', 8, 9])
````

With ramdom values

````python
dates = pd.date_range('1/1/2000', periods=8)
df = pd.DataFrame(np.random.randn(8, 4),
                  index=dates, columns=['A', 'B', 'C', 'D'])
````

````python
pd.DataFrame({'col1': [1, 2, 3, 4], 'col2': [9, 9, 0, 9]}, index=['A', 'B', 'A', 'A'])
````

## Timeseries

[doc pandas pd.ranges](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.date_range.html)

````python
pd.date_range(start='1/1/2018', end='1/08/2018')
pd.date_range(start='1/1/2018', periods=8)
pd.date_range(start='1/1/2018', periods=5, freq='3M')
pd.date_range(start='1/1/2018', periods=5, freq=pd.offsets.MonthEnd(3))
pd.date_range(start='1/1/2018', periods=5, tz='Asia/Tokyo')
pd.date_range(start='2017-01-01', end='2017-01-04',
              inclusive='left')  # inclusive{“both”, “neither”, “left”, “right”}, default “both”
````

[pandas doc DatetimeIndex](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DatetimeIndex.html)

# Type Gestion

## Pandas

> voir aussi deal_with_null.md

```python
s = pd.Series([129944444999999922.0, 1001.0, 1119999999912.0, None])
s.astype('str')
```

<https://pandas.pydata.org/docs/reference/api/pandas.to_numeric.html>

### Categorical data

[Anchor: Click Then go : Pandas > Categorical data](./basic_manipulation.md#Type Gestion)

[Pandas Documentation](https://pandas.pydata.org/pandas-docs/stable/user_guide/categorical.html)

### convert multiple columns

````python
list_columns = ["wagon_number", 'nb_wagons_available', 'nb_wagons_in_maintenance']
df[list_columns] = df[list_columns].astype(int)
df[["wagon_number", 'nb_wagons_available', 'nb_wagons_in_maintenance']]  # c'est bien une double list
# or simply :
df = df.astype(int)

# NOT WORKING:
pd.to_numeric(df[list_columns], downcast='float')
````

### numeric With null dealing

comprehensive list

```python
s1 = pd.Series(['' if pd.isnull(x) else int(x) for x in s], index=s.index)
```

[pandas doc to_numeric](https://pandas.pydata.org/docs/reference/api/pandas.to_numeric.html)

```python
import pandas as pd

df['column'] = pd.to_numeric(df['column'], downcast='float')
df['column'] = pd.to_numeric(df['column'], downcast='integer')
df['column'] = pd.to_numeric(df['column'], downcast='integer', errors='ignore')
```

**downcast{‘integer’, ‘signed’, ‘unsigned’, ‘float’}, default None**

If not None, and if the data has been successfully cast to a numerical dtype (or if the data was numeric to begin with),
downcast that resulting data to the smallest numerical dtype possible according to the following rules:

- `integer` or `signed`: smallest signed int dtype (min.: np.int8)
- `unsigned`: smallest unsigned int dtype (min.: np.uint8)
- `float`: smallest float dtype (min.: np.float32)

**errors{‘ignore’, ‘raise’, ‘coerce’}, default ‘raise’**

- If `raise`, then invalid parsing will raise an exception.
- If `coerce`, then invalid parsing will be set as NaN.
- If `ignore`, then invalid parsing will return the input.

## global

````python
s.convert_dtypes()  # convertis avec le meilleur type possible
````

# Null

ne pas tester as `is None` mais avec `pandas.isnull()` or a l'inverse `pandas.notnull()`

**dataframe**  
[doc pandas](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.isnull.html)

```python
# with comprehensive list
pd.Series(['' if pd.isnull(x) else int(x) for x in s], index=s.index)
```

```python
df1 = df.where(pd.notnull(df), None)  # remplace les NaN en None
```

## Test

```python
df.empty  # work even if there are column but no rows
rows_null = pd.isnull(df[columns_concerned]).all(axis='columns')
cols_null = pd.isnull(df[columns_concerned]).all()
```

## Drop NA

**dataframe**  
[doc pandas](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.dropna.html)

```python
df.dropna()  # drop Rows with any NA in it
df.dropna(axis='columns')  # drop Columns with any NA in it
df.dropna(thresh=2)  # Keep only the rows with at least 2 non-NA values.
df.dropna(how='all')  # Drop only if all elements are null
df.dropna(subset=['name', 'toy'], inplace=True)

self.df_data.dropna(axis='columns', how='all', inplace=True)  # drop_columns_all_null
self.df_data.dropna(how='all', subset=self.df_data.columns[2:], inplace=True)  # drop_rows_all_null
```

# Format, Schema and Manipulation

## Size and Len

````python
len(df)  # get le nombre de ligne
df.shape[0]
len(df.columns)  # get le nombre de colonnes
df.shape[1]
````

````python
df.shape  # nb col et rows
df.ndim  # nombre de dimension : 2 pour un tableau
df.size  # Nombre d'éléments dans le tableau (row x cols)
````

## Describe

````python
df.describe()
````

## Index

### set

````python
df.set_index(pd.Index([0, 2]))
# Set column as Index.
df = df.set_index('Courses')


````

### reset

````python
df.reset_index()
df.reset_index(drop=True)  # drop for not having the old index in a new column
````

[pandas reindex documentation](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.reindex.html)

### name

````python
# Get name of the index column of DataFrame.
index_name = df.index.name
# set Index Name
df.index.name = 'Index1'
# Rename Index.
df = df.rename_axis('Courses1')
# Using df.index.rename get pandas index title/name.
df.index.rename('Courses1', inplace=True)
# Get pandas index title/name by index and columns parameter.
df = df.rename_axis(index='Courses', columns="Courses1")
# Removing index and columns names to set it none.
df = df.rename_axis(index=None, columns=None)
````

### MultiIndex

#### set

````python
index_labels = ['r1', 'r2', 'r3', 'r4']
df = pd.DataFrame(technologies, index=index_labels)
````

#### name

````python
# Add Multilevel index using set_index() 
df2 = df.set_index(['Courses', 'Duration'], append=True)

# Rename Single index from multi Level
df2.index = df2.index.set_names('Courses_Duration', level=2)
# Rename All indexes
df2.index = df2.index.rename(['Index', 'Courses_Name', 'Courses_Duration'])
````

## Select

````python
truc_list[0:3]  # prend de 0 jusque 2 inclus (donc 3 valeurs), index 3 est exclu
df.columns[0:1]  # prend de 0 jusque 1 exclu (donc 1 valeurs)
````

###### dataframe

- [Doc](https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html)
- [Doc iloc](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.iloc.html)
- [Doc loc](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.loc.html)


- [pandas.DataFrame.query()](https://sparkbyexamples.com/pandas/pandas-dataframe-query-examples/) – To query rows based
  on column value (condition).
- [pandas.DataFrame.loc[]](https://sparkbyexamples.com/pandas/pandas-dataframe-query-examples/) – To select rows by
  indices label and column by name.
- [pandas.DataFrame.iloc[]](https://sparkbyexamples.com/pandas/pandas-iloc-usage-with-examples/) – To select rows by
  index and column by position.
- [pandas.DataFrame.apply()](https://sparkbyexamples.com/pandas/pandas-apply-function-usage-examples/) – To custom
  select using lambda function.
- `DataFrame.at[]` property is used to access a single cell by row and column label pair. Like loc[] this doesn’t
  support column by position. This performs better when you wanted to get a specific cell value from Pandas DataFrame as
  it uses both row and column labels. Note that at[] property doesn’t support negative index to refer rows or columns
  from last.
- `DataFrame.iat[]` is another property to select a specific cell value by row and column position. Using this you can
  refer to column only by position but not by a label. This also doesn’t support negative index or column position.

````python
import numpy as np
import pandas as pd

dates = pd.date_range('1/1/2000', periods=8)
df = pd.DataFrame(np.random.randn(8, 4),
                  index=dates, columns=['A', 'B', 'C', 'D'])

df[['A', 'B']]  # sub dataframe with these columns

s = df['A']
s[dates[5]]

# iloc : By index
df.iloc[]
# loc : by value
df.loc[]
df.loc[:, ['B', 'A']]

df = pd.DataFrame([[1, 2], [4, 5], [7, 8]],
                  index=['cobra', 'viper', 'sidewinder'],
                  columns=['max_speed', 'shield'])

df.loc['viper']
df.loc[['viper', 'sidewinder']]
df.loc['cobra', 'shield']
df.loc['cobra':'viper', 'max_speed']
df.loc[[False, False, True]]  # return row 3
df.loc[df['shield'] > 6]
df.loc[df['shield'] > 6, ['max_speed']]
df.loc[['viper', 'sidewinder'], ['shield']] = 50
df.loc[:, 'max_speed']
df.loc[7:9]  # get row de 7 a 9 exclu (2row)
````

The main distinction between the two methods is:

- `loc` gets rows (and/or columns) with particular labels.
- `iloc` gets rows (and/or columns) at integer locations.

````python
s = pd.Series(list("abcdef"), index=[49, 48, 47, 0, 1, 2])
s.loc[0]  # 'd' : value at index label 0
s.iloc[0]  # 'a': value at index location 0
s.loc[0:1]  # 'd', 'e': rows at index labels between 0 and 1 (inclusive)
s.iloc[0:1]  # 'a' : rows at index location between 0 and 1 (exclusive)
````

| object                 | 	description	                                                 | `s.loc[object]`                            | 	`s.iloc[object] `                         |
|------------------------|---------------------------------------------------------------|--------------------------------------------|--------------------------------------------|
| `0`                    | single item                                                   | 	Value at index label `0` (the string `d`) | Value at index location 0 (the string 'a') |
| `0:1`                  | slice                                                         | Two rows (labels `0` and `1`)              | One row (first row at location 0)          |
| `1:47`                 | slice with out-of-bounds end                                  | Zero rows (empty Series)                   | Five rows (location 1 onwards)             |
| `1:47:-1`              | slice with negative step                                      | three rows (labels `1` back to `47`)       | Zero rows (empty Series)                   |
| `[2, 0]`               | 	integer list                                                 | Two rows with given labels                 | Two rows with given locations              |
| `s > 'e'`              | Bool series (indicating which values have the property)       | One row (containing 'f')                   | `NotImplementedError`                      |
| `(s>'e').values`       | Bool array                                                    | One row (containing 'f')                   | Same as `loc`                              |
| `999`                  | int object not in index                                       | `KeyError`                                 | `IndexError` (out of bounds)               |
| `-1`                   | int object not in index                                       | `KeyError`                                 | Returns last value in `s`                  |
| `lambda x: x.index[3]` | callable applied to series (here returning 3rd item in index) | `s.loc[s.index[3]]`                        | `s.iloc[s.index[3]]`                       |

### Change value selected

```python
 df['Name'] = 'abc'
df.assign(Name='abc')
df.insert(0, 'Name', 'abc')
```

## filter and Keep

````python
X_train, y_train[:, 1]
````

````python
# filter/ keep the columns ['one', 'three']
df.filter(items=['one', 'three'])
df = df.filter(items=[index to keep], axis=0)  # How to Filter Based on Index

# select columns by regular expression
df.filter(regex='e$', axis=1)
# select rows containing 'bbi'
df.filter(like='bbi', axis=0)

# filter on row
df.loc[df['TeamID'] == 12]
df = df.loc[row_not_selected]

````

|     | revision_date  | business_unit | product_type |
|-----|----------------|:-------------:|-------------:|
| 0   | 2020 - 01 - 01 |      TD       |            1 |
| 1   | 2022 - 01 - 02 |               |            2 |
| 2   | 2022 - 01 - 03 |      TPF      |            3 |

````shell
>>> df_in.iloc[1, "product_type"]  # i comme index
2
>>> df_in.loc[1, 2]
2
````

loc / iloc

###### row dataframe

````python
boolean_series_of_rows = df.index.to_series().between('2018-01-01', '2018-01-10')
boolean_series_of_rows = df.index.isin(my_list)

s = df.loc[df['instrument_token'].eq(12295682), 'tradingsymbol']
# alternative
s = df.loc[df['instrument_token'] == 12295682, 'tradingsymbol']

# get sub dataframe
df = df[boolean_series_of_rows]

# filter by index
df.truncate(before=realised_date, after=None, axis='index')
````

[pandas between documentation](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.between.html)

## Rename

https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.rename.html?highlight=rename

````python
df.rename(columns={"A": "a", "B": "c"})  # rename "A" in "a"
df.rename(index={0: "x", 1: "y", 2: "z"})

df.rename(str.lower, axis='columns')
df.rename({1: 2, 2: 4}, axis='index')
````

```python
# for many columns
dict_for_renaming = {"old_column_name_1": "new_column_name_1",
                     "old_column_name_2": "new_column_name_2"}
df.columns = df.columns.to_series().map(dict_for_renaming)
# or
df.rename(columns=dict_for_renaming, inplace=True)

ALL_COLUMNS = list(DICT_TO_MAP_OUTPUT.keys())
```

## Drop

https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.drop.html

````python
df.drop(columns=['B', 'C'])
df.drop(['B', 'C'], axis=1)

df.drop(index='cow', columns='small')
````

## Replace / mapping value

### global variable

````python
value_from = "kevin"
dict_of_conversion = {
    "kevin": "GUERNIGOU",
    "nina": "KRAVITZ"
}
try:
    value_to = dict_of_conversion[value_from]
except KeyError:
    value_to = "default_value"
# of
if "kevin" in list(dict_of_conversion.keys()):
    value_to = dict_of_conversion[value_from]
````

### Dataframe values

[Doc](https://sparkbyexamples.com/pandas/pandas-remap-values-in-column-with-a-dictionary-dict/#:~:text=Using%20Pandas%20DataFrame.-,replace(),regular%20expressions%20for%20regex%20substitutions.)

````python
dict = {"Spark": 'S', "PySpark": 'P', "Hadoop": 'H', "Python": 'P', "Pandas": 'P'}
df2 = df.replace({"Courses": dict})
df.replace({"Courses": dict}, inplace=True)

# for all
df.replace(1, 5)
df.replace({0: 10, 1: 100})
````

````python
# dans la colonne 'a' transform les values 'Medium'... en 1, 2 ou 3
df.replace({'column_a': {'Medium': 2, 'Small': 1, 'High': 3}})
````

## Append, Concat

````python
yourdict['yourkey'] = 'yourvalue'
set_test = {'g', 'e', 'k'}
set_test.add('s')  # for a set ; genre de Inplace
list_test = ['g', 'e', 'k']
list_test.append('s')  # for a list ; genre de Inplace
````

[Documentation Pandas append](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.append.html)

[Documentation Pandas concat](https://pandas.pydata.org/docs/reference/api/pandas.concat.html)

````python
df.append(df2, ignore_index=True)  # deprecated
pd.concat([df, df2], ignore_index=True)

# pd.concat will serve the purpose of rbind in R. 
print(pd.concat([df1, df2]))
````

## Join

**set**

````python
set.union(other_set)
````

**Dict**

In Python 3.9.0 or greater (released 17 October 2020, PEP-584, discussed here):

```
z = x | y
```

In Python 3.5 or greater:

```
z = {**x, **y}
```

or

```
z.update(y)    # modifies z with keys and values of y
```

**List**

````python
list3 = list1 + list2
print(list3)

list1.extend(list2)
````

## Merge

**Dataframe**
[doc pandas merge](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.merge.html)
params:

- `how` : {‘left’, ‘right’, ‘outer’, ‘inner’, ‘cross’}, default ‘inner’

````python
df1.merge(df2, left_on='lkey', right_on='rkey', how='left')
df1.merge(df2, how='inner', on='a')
````

### create every possible combination

````python
# Assuming the 2 series are s and s1, use itertools.product() which gives a cartesian product of input iterables
import itertools

df = pd.DataFrame(list(itertools.product(s, s1)), columns=['letter', 'number'])
print(df)
````

### Dataframe

- [`DataFrame.join`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.join.html#pandas.DataFrame.join):
  Join DataFrames using indexes.
- [`DataFrame.merge`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.merge.html#pandas.DataFrame.merge):
  Merge DataFrames by indexes or columns.
- [`pandas.concat`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.concat.html):
  Concatène par le bas

[doc pandas merge](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.merge.html)

Often you may want to merge two pandas DataFrames by their indexes. There are three ways to do so in pandas:

1. Use join: By default, this performs a left join.
   `df1.join(df2)`

2. Use merge. By default, this performs an inner join.
   `pd.merge(df1, df2, left_index=True, right_index=True)`

3. Use concat. By default, this performs an outer join.
   `pd.concat([df1, df2], axis=1)`

## Duplicates

### ask any Duplicates

**List and set**

`if len(mylist) != len(myset)`  # if True then duplicates values

````python
dup = {x for x in mylist if mylist.count(x) > 1}  # get a set of duplicated values
````

**DataFrame**  
[Pandas Documentation](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.duplicated.html)

```python
df.duplicated()  # Renvoie une serie avec les lignes duplicated
df.duplicated(keep='last')
df.duplicated(keep=False)
df.duplicated(subset=['brand'])
```

### Drop Duplicates

````python
new_menu = ['Hawaiian', 'Margherita', 'Mushroom', 'Prosciutto', 'Meat Feast', 'Hawaiian', 'Bacon',
            'Black Olive Special', 'Sausage', 'Sausage']
final_new_menu = list(set(new_menu))  # simplest way
list(dict.fromkeys(logs_warning))  # keep the order
````

**DataFrame**  
[Pandas Documentation](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.drop_duplicates.html)

```python
df.drop_duplicates()
df.drop_duplicates(subset=['brand'], inplace=True)
df.drop_duplicates(subset=['brand', 'style'], keep='last')
```

## Range

https://www.w3schools.com/python/ref_func_range.asp

````python
range(start, stop, step)

range(10)
range(0, 10)  # 10 valeur commance a 0 finit a 9 inclus
````

## Sort

###### list

````python
import numpy

numpy.argsort(myList)
````

````python
cars = ['Ford', 'BMW', 'Volvo']

cars.sort(reverse=True)
````

**Dataframe**
https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.sort_values.html

````python
df.sort_values(by=['col1', 'col2'], ascending=False, na_position='first')
````

# Detection / Logical test

## Compare

###### dict

[link compare two dict](https://miguendes.me/the-best-way-to-compare-two-dictionaries-in-python)

## Logical Operation

```python
result = foo & bar
```

 ````python
ser1 = pd.Series([True, True, False, False])
ser2 = pd.Series([True, False, True, False])

print(ser1 & ser2)
````

#### revert boolean series

````python
~index_with_negative_value
````

### Any / All

[doc pandas any](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.any.html)

````python
df.any(axis='columns')  # donne une colonne (un bool pour chaque ligne)
df.any()  # serie: un bool pour chaque colonne
df.any(axis=None)  # all
````

## Existence check

### Column Existence (works for lists)

#### single value

This will work:

```python
bikes = ['trek', 'redline', 'giant']
'trek' in bikes

if value in list:
    pass

if value not in list:
    pass

if 'A' in df:
    pass
```

But for clarity, I'd probably write it as:

```python
if 'A' in df.columns:
```

#### multiple value

```python
if set(['A', 'C']).issubset(df.columns):
# OR
if {'A', 'C'}.issubset(df.columns):
# OR with comprehensive list
if all([item in df.columns for item in ['A', 'C']]):
```

### filter List with list boolean

````python
list(compress(list_contracts_selected, error_condition))
````

### invert boolean List

````python
[not elem for elem in boolean_list]
````

## Null Values

**Pandas and Series**
[is null pandas documentation](https://pandas.pydata.org/docs/reference/api/pandas.Series.isnull.html)

```python
Series.isnull()

>> > pd.isna('dog')
False
>> > pd.isna(pd.NA)
True
>> > pd.isna(np.nan)
True
>> > pd.isna(None)
True

df_pandas.isnull()
df_pandas.notnull()

# (1) Check for NaN under a single DataFrame column:
df['your column name'].isnull().values.any()
# (2) Count the NaN under a single DataFrame column:
df['your column name'].isnull().sum()
(3)
Check
for NaN under an entire DataFrame:
    df.isnull().values.any()
# (4) Count the NaN under an entire DataFrame:
df.isnull().sum().sum()
```

get rows with conditionnal with any if there is any by row or all if every one need to

```python
index_with_negative_value = (df_to_clean[columns_concerned] < 0).any(axis=1, skipna=True)
```

## replace NA values

```python
df['DataFrame Column'] = df['DataFrame Column'].fillna(0)  # For one column using pandas
df['DataFrame Column'] = df['DataFrame Column'].replace(np.nan, 0)  # For one column using numpy
df.fillna(0)  # For the whole DataFrame using pandas
df.replace(np.nan, 0)  # For the whole DataFrame using numpy
```

# Data Transformation / Manipulation

# Compare

## get difference

**List**: between two list

````python
import numpy as np

main_list = np.setdiff1d(list_2, list_1)
# yields the elements in `list_2` that are NOT in `list_1`
````

# Math

## Max / Min

````python
a = (1, 5, 3, 9)
x = max(a)

df['column1'].max()
df.max()  # Give the max of each columns
final_df["value"] = final_df[["shortfall", "f_pot"]].min(axis=1)
````

### Create Boolean Column which indicate if max (or get index)

````python
# Works for only one value
df = pd.DataFrame([
    {'wagon_number': 100000000001, 'business_unit': 'TD', 'product_type': 'BMO'},
    {'wagon_number': 100000000002, 'business_unit': 'TD', 'product_type': 'PROPYLENE'},
    {'wagon_number': 100000000003, 'business_unit': 'TD', 'product_type': 'BMO'},
    {'wagon_number': 100000000004, 'business_unit': 'TRF', 'product_type': 'BMO'},
    {'wagon_number': 100000000005, 'business_unit': 'TRF', 'product_type': 'BMO'},
    {'wagon_number': 100000000005, 'business_unit': 'TRF', 'product_type': 'PROPYLENE'},
])
df['index_temp'] = df.index
index_which_is_max = df.groupby("business_unit").max()['index_temp']
df['is_max_choose'] = df['index_temp'].isin(index_which_is_max)

# For alls values
# Solution 1: Merge
df_with_max = df.groupby("business_unit").max()
df = df.merge(df_with_max.rename(columns={"wagon_number": "wagon_number_max_sol1"})['wagon_number_max'],
              on='business_unit')
## Solution 2: Without Merge
# https://stackoverflow.com/questions/15705630/get-the-rows-which-have-the-max-value-in-groups-using-groupby
df['wagon_number_max_sol2'] = df.groupby(['business_unit'])['wagon_number'].transform(max)
## Final
df['is_max'] = df['wagon_number'] == df['wagon_number_max']
````

## Round

**DataFrame**  
[Pandas Documentation](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.round.html)

```python
df.round(1)
df.round({'dogs': 1, 'cats': 0})  # with 'dogs' and 'cats' some columns
# OR
decimals = pd.Series([0, 1], index=['cats', 'dogs'])
df.round(decimals)
```

## Count

````python
# group by and count
df.eventMessage.groupby(pd.to_datetime(df.timeStamp).dt.hour)).count()
````

````python
# Modifying a subset of rows in a pandas dataframe
df['A' == 0]['B'] = np.nan
````

## Aggregate Sum

````python
df.groupby(['A', 'B']).mean()

# group by Team, get mean, min, and max value of Age for each value of Team.
grouped_multiple = df.groupby(['Team', 'Pos']).agg({'Age': ['mean', 'min', 'max']})
# rename columns
grouped_multiple.columns = ['age_mean', 'age_min', 'age_max']
# reset index to get grouped columns back
grouped_multiple = grouped_multiple.reset_index()
print(grouped_multiple)
````

```python
# Using DataFrame.sum() to Sum of each row
df2 = df.sum(axis=1)

# Sum the rows of DataFrame
df['Sum'] = df.sum(axis=1)

# Using DataFrame.iloc[] to select 2 and 3 columns to sum
df['Sum'] = df.iloc[:, [2, 3]].sum(axis=1)
```

###### dataframe

[pandas GroupBy documentation](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.groupby.html)
[pandas agg documentation](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.agg.html)

````python
import numpy as np

# generate a column double index with first index column level 'date' and second 'min' and 'max'
df.agg({'sum_col': np.sum,
        'date': [np.min, np.max]})

# if aggregate from index then
result
.rename_axis(index='date')
.groupby('date').agg({'value': 'min'})
# or 
result.groupby(level=0).agg({'value': 'min'})
result = sales_data.groupby('salesman_id').agg({'purchase_amt': ['mean', 'min', 'max']})

df.groupby(level=0, axis=1).sum()
````

###### timeseries

````python
# This is because your groupby uses PeriodIndex, rather than datetime:
df.groupby(pd.PeriodIndex(data=df.date, freq='D'))
# You could have instead used a pd.Grouper:
df.groupby(pd.Grouper(key="date", freq='D'))
````

# Specific

## get execution time

````python
import time

start_time = time.time()
main()
print("--- %s seconds ---" % (time.time() - start_time))
````

## adding values or simple operation

adding 1 to every element of a list

````python
mylist = [1, 2, 3]
[x + 1 for x in mylist]
map(lambda x: x + 1, [1, 2, 3])
````

## Transform 2 columns into list of tuple

````python
df['new_col'] = list(zip(df.lat, df.long))
````

## If colnames with same name

````python
def generate_unique_colnames(df_columns: List[str]) -> List[str]:
    return [str(index_col) + "A5b7x4WOOt__" + df_columns[index_col] for index_col in range(0, len(df_columns))]


def reset_unique_colnames(df_columns: List[str], separator: str = "__") -> List[str]:
    return [column_name.split(separator, 1)[1] for column_name in df_columns]
````

unit Test

````python
import unittest


class TestFileValidation(unittest.TestCase):
    def test_generate_unique_colnames(self):
        # Given
        df_columns = ["wagon_number", "Test", "Test", "Test2", "Test"]

        # When
        result = generate_unique_colnames(df_columns=df_columns)

        # Expected
        result_expected = ["0A5b7x4WOOt__wagon_number", "1A5b7x4WOOt__Test", "2A5b7x4WOOt__Test", "3A5b7x4WOOt__Test2",
                           "4A5b7x4WOOt__Test"]

        # Then
        self.assertEqual(result_expected, result)

    def test_reset_unique_colnames(self):
        # Given
        df_columns = ["0__wagon__number", "1__Test", "2__Test", "3__Test2", "4__Test"]

        # When
        result = reset_unique_colnames(df_columns=df_columns)

        # Expected
        result_expected = ["wagon__number", "Test", "Test", "Test2", "Test"]

        # Then
        self.assertEqual(result_expected, result)

````

## Drop columns or row all defined value (null allowed)

[for null see this function](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.dropna.html)

````python
from typing import List

import pandas as pd


def drop_rows_all_null(df: pd.DataFrame, columns_concerned: List[str]) -> pd.DataFrame:
    rows_null = pd.isnull(df[columns_concerned]).all(axis='columns')
    return df.loc[~rows_null]


def drop_columns_all_null(df: pd.DataFrame, columns_concerned: List[str]) -> pd.DataFrame:
    cols_null = pd.isnull(df[columns_concerned]).all()

    if any(cols_null):
        for column in cols_null[cols_null].index:
            df.drop(columns=[column], inplace=True)

    return df
````

unit Test

````python
import unittest

import pandas as pd
from pandas.testing import assert_frame_equal


class TestFileValidation(unittest.TestCase):

    def test_drop_columns_all_null(self):
        # Given
        df = pd.DataFrame([
            {'wagon_number': 100000000001, 'business_unit': 'TD', 'Test': None, 'product_type': 'BMO', 'Test2': None},
            {'wagon_number': 100000000002, 'business_unit': None, 'Test': None, 'product_type': 'BMO', 'Test2': None},
            {'wagon_number': 100000000003, 'business_unit': 'TD', 'Test': None, 'product_type': None, 'Test2': None},
            {'wagon_number': 100000000004, 'business_unit': None, 'Test': None, 'product_type': None, 'Test2': None},
        ])

        # When
        result = drop_columns_all_null(df=df, columns_concerned=['business_unit', 'Test', 'product_type', 'Test2'])

        # Expected
        result_expected = pd.DataFrame([
            {'wagon_number': 100000000001, 'business_unit': 'TD', 'product_type': 'BMO'},
            {'wagon_number': 100000000002, 'business_unit': None, 'product_type': 'BMO'},
            {'wagon_number': 100000000003, 'business_unit': 'TD', 'product_type': None},
            {'wagon_number': 100000000004, 'business_unit': None, 'product_type': None},
        ])

        # Then
        assert_frame_equal(result_expected, result)

    def test_drop_rows_all_null(self):
        # Given
        df = pd.DataFrame([
            {'wagon_number': 100000000001, 'business_unit': 'TD', 'Test': None, 'product_type': 'BMO', 'Test2': None},
            {'wagon_number': 100000000002, 'business_unit': None, 'Test': None, 'product_type': None, 'Test2': None},
            {'wagon_number': 100000000003, 'business_unit': 'TD', 'Test': None, 'product_type': None, 'Test2': None},
            {'wagon_number': 100000000004, 'business_unit': None, 'Test': None, 'product_type': None, 'Test2': None},
        ])

        # When
        result = drop_rows_all_null(df=df, columns_concerned=['business_unit', 'Test', 'product_type', 'Test2'])

        # Expected
        result_expected = pd.DataFrame([
            {'wagon_number': 100000000001, 'business_unit': 'TD', 'Test': None, 'product_type': 'BMO', 'Test2': None},
            {'wagon_number': 100000000002, 'business_unit': 'TD', 'Test': None, 'product_type': None, 'Test2': None},
        ]).set_index(pd.Index([0, 2]))

        # Then
        assert_frame_equal(result_expected, result)
````

## variable declaration

````python
form = {}


def init_form(form_parameter: {}):
    global form
    form = form_parameter


def required(name: str):
    if name not in form:
        raise ParameterException(name)
    return form[name]


def optional(name: str, or_else: str = None):
    return form[name] if name in form else or_else
````
