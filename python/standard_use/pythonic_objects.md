## Property

- [link realpython](https://realpython.com/python-property/#:~:text=Python's%20property()%20is%20the,use%20it%20without%20importing%20anything.)
- [link realpython getter-setter](https://realpython.com/python-getter-setter/)

## Dataclass

- [doc python dataclass](https://docs.python.org/3/library/dataclasses.html)
- [article reconcilier property and dataclass](https://florimond.dev/en/posts/2018/10/reconciling-dataclasses-and-properties-in-python/)

````python
from dataclasses import dataclass


@dataclass
class Stock:
    symbol: str
    price: float = 0.
````

````python
from dataclasses import dataclass


@dataclass(frozen=True)
class Frozen:
    x: int
    y: int
````

#### validation

##### naive validation

````python

import dataclasses


@dataclasses.dataclass
class EventShortfall:
    scenario_id: int
    asset: str

    def dict(self):
        return dataclasses.asdict(self)

    def __post_init__(self):
        self.validate()

    def validate(self):
        self.validate_scenario_id()

    def validate_scenario_id(self):
        if self.scenario_id not in [1, 2, 3]:
            raise ValueError("not in perimeter")
````

##### validation

````python
from typing import Union, List
from dataclasses import dataclass


class Validations:
    def __post_init__(self):
        """Run validation methods if declared.
        The validation method can be a simple check
        that raises ValueError or a transformation to
        the field value.
        The validation is performed by calling a function named:
            `validate_<field_name>(self, value, field) -> field.type`
        """
        for name, field in self.__dataclass_fields__.items():
            if (method := getattr(self, f"validate_{name}", None)):
                setattr(self, name, method(getattr(self, name), field=field))


@dataclass
class Product(Validations):
    name: str
    tags: Union[str, List[str]]

    def validate_name(self, value, **_) -> str:
        if len(value) < 3 or len(value) > 20:
            raise ValueError("name must have between 3 and 20 chars.")
        return value

    def validate_tags(self, value, **_) -> List[str]:
        """Ensure tags are always List[str] even if "tag1,tag2" is passed"""
        if isinstance(value, str):
            value = [v.strip() for v in value.split(",")]
        return value


if __name__ == "__main__":
    product = Product(name="product", tags="tag1, tag2")
    assert product.tags == ["tag1", "tag2"]  # transformed to List[str]

    try:
        product = Product(name="pr", tags="tag1, tag2, tag3")
    except ValueError as e:
        assert str(e) == "name must have between 3 and 20 chars."
````

##### Validator

If the price is always derivable from the symbol, I would go with a @property. Keeps the code lean and it looks like an
attribute from the outside:

````python
def get_price(symbol):
    return 123


@dataclass
class Stock:
    symbol: str


@property
def price(self):
    return get_price(symbol)


stock = Stock("NVDA")
print(stock.price)  # 123
````

If you want to have a settable attribute that also has a default value that is derived from the other fields, you may
just want to implement your own `__init__` or `__post_init__ as suggested.

If this is something you encounter a lot and need more sophisticated ways to handle it, I would recommend looking into
pydantic and validators.

`````python
from typing import Optional

from pydantic import validator
from pydantic.dataclasses import dataclass


def get_price(symbol):
    return 123.0


@dataclass
class Stock:
    symbol: str
    price: Optional[float] = None

    @validator('price')
    def validate_price(cls, v, values, **kwargs):
        return v or get_price(values["symbol"])


print(Stock("NVDA"))
# > Stock(symbol='NVDA', price=123.0)
print(Stock("NVDA", 456))
# > Stock(symbol='NVDA', price=456.0)
`````

#### calcul a variable from another

You can use `__post_init__` here. Because it's going to be called after `__init__`, you have your attributes already
populated so do whatever you want to do there:

````python
from typing import Optional
from dataclasses import dataclass


def get_price(name):
    # logic to get price by looking at `name`.
    return 1000.0


@dataclass
class Stock:
    symbol: str
    price: Optional[float] = None

    def __post_init__(self):
        if self.price is None:
            self.price = get_price(self.symbol)


obj1 = Stock("boo", 2000.0)
obj2 = Stock("boo")
print(obj1.price)  # 2000.0
print(obj2.price)  # 1000.0
````

````python
@dataclass
class Stock:
    symbol: str
    price: float = get_price(symbol)
````

## set class property

````python

````