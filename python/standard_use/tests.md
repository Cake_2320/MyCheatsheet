# Tests Unitaires

## Launch

````shell
import unittest
from pandas.testing import assert_frame_equal

class TestPandasValidation(unittest.TestCase):

    def test_validate_column_type_string(self):
        # Given
        df_in = pd.DataFrame([
            {'revision_date': "2020-01-01", 'business_unit': 'TD', 'product_type': "1"},
            {'revision_date': None, 'business_unit': "", 'product_type': "47q89"},
            {'revision_date': "2022-01-03", 'business_unit': 'TPF', 'product_type': "3"},
        ]).convert_dtypes()

        # When
        column_name = "revision_date"
        pandas_validation = PandasColumnValidation(df=df_in,
                                                   column_name=column_name, column_type="string", allow_empty=True)
        result, result_errors = pandas_validation.validate_column()

        # Then
        self.assertEqual(True, result)
        self.assertEqual(0, len(result_errors))
        assert_frame_equal(dict_param_expected['AMKP1-05'], result['AMKP1-05'])
        
        class TestApplyEventProfil(unittest.TestCase):

    def setUp(self) -> None:
        self.date_format = "%Y-%m-%d"

    def test_1(self):
        # Given
        df = pd.DataFrame([
            {'date': "2022-01-01", 'baseline_value': 100., 'delta_value': 50.},
            {'date': "2022-02-01", 'baseline_value': 150., 'delta_value': 100.},
        ])
        df['date'] = pd.to_datetime(df['datetime'], format=self.date_format)

        # When
        df_result = deploy_date(df)

        # Expected
        df_expected = pd.DataFrame([
            {'date': "2022-01-01", 'baseline_value': 100., 'delta_value': 50.},
            {'date': "2022-02-01", 'baseline_value': 150., 'delta_value': 100.},
        ])
        df_expected['date'] = pd.to_datetime(df_expected['date'], format=self.date_format)

        # Then
        assert_frame_equal(df_expected, df_result)

        
````

### Coverage

[Doc](https://pypi.org/project/pytest-cov/)
[Doc coverage file config](https://pytest-cov.readthedocs.io/en/latest/config.html?highlight=exclude)

````shell
pip3 install pytest-cov
````

````shell
pytest --cov=folder_a --cov=folder_b --cov=folder_c --cov-report=html
````

```
proj_folder
   - > tests_functional
   - > tests_unit
```

```shell
pytest --cov=proj_folder proj_folder/tests_unit proj_folder/tests_functional
```

#### file .coveragerc

run `pytest --cov-config=.coveragerc --cov=./ tests/`

```
[run]
branch = True
include =
        ./*
omit =
        *tests*
        *__init__*
        */data*
        tests/*

[report]
exclude_lines =
        pragma: no cover
        if __name__ == .__main__.:
[html]
directory = coverage_html_report
```

```
# .coveragerc to control coverage.py
[run]
branch = True

source =
        my_folder/

[report]
# Regexes for lines to exclude from consideration
exclude_lines =
    # Have to re-enable the standard pragma
    pragma: no cover

    # Don't complain about missing debug-only code:
    def __repr__
    if self\.debug

    # Don't complain if tests don't hit defensive assertion code:
    raise AssertionError
    raise NotImplementedError

    # Don't complain if non-runnable code isn't run:
    if 0:
    if __name__ == .__main__.:

ignore_errors = True

[html]
directory = coverage_html_report
```

## Code

[Documentation Pandas assert_frame_equal](https://pandas.pydata.org/docs/reference/api/pandas.testing.assert_frame_equal.html)

```python
import pandas as pd
from pandas.testing import assert_frame_equal

pd.testing.assert_frame_equal
assert_frame_equal
```

````python
        # Given
df_mean_accidental_repair_by_pool = pd.DataFrame([
    {'wagon_number': 100000000001,
     "ticket_duration_in_period": dt.timedelta(days=100), "duration_wagon_in_fleet": dt.timedelta(days=200),
     "ar": 0.5},
    {'wagon_number': 100000000002,
     "ticket_duration_in_period": dt.timedelta(days=100), "duration_wagon_in_fleet": dt.timedelta(days=200),
     "ar": 1.0},
    {'wagon_number': 100000000003,
     "ticket_duration_in_period": dt.timedelta(days=100), "duration_wagon_in_fleet": dt.timedelta(days=200),
     "ar": 0.3},
    {'wagon_number': 100000000004,
     "ticket_duration_in_period": dt.timedelta(days=100), "duration_wagon_in_fleet": dt.timedelta(days=200),
     "ar": 0.2},
    {'wagon_number': 100000000005,
     "ticket_duration_in_period": dt.timedelta(days=100), "duration_wagon_in_fleet": dt.timedelta(days=200),
     "ar": 0.15},
    {'wagon_number': 100000000006,
     "ticket_duration_in_period": dt.timedelta(days=100), "duration_wagon_in_fleet": dt.timedelta(days=200),
     "ar": 0.0},
    {'wagon_number': 100000000007,
     "ticket_duration_in_period": dt.timedelta(days=100), "duration_wagon_in_fleet": dt.timedelta(days=200),
     "ar": 0.66666},
]).convert_dtypes()

# When
self.ar_calculation_repair.df_mean_accidental_repair_by_pool = df_mean_accidental_repair_by_pool
self.ar_calculation_repair.calculate_accidental_repair_by_bool(map_wagon_number_x_pool=map_wagon_number_x_pool)
df_result = self.ar_calculation_repair.df_mean_accidental_repair_by_pool

# Expected
df_result_expected = pd.DataFrame([
    {'business_unit': 'TD', 'product_type': 'BLACK', "ar": 0.3},
    {'business_unit': 'TD', 'product_type': 'BMO', "ar": 0.75},
    {'business_unit': 'TPF', 'product_type': 'BLACK', "ar": 0.15},
    {'business_unit': 'TPF', 'product_type': 'RAFFINATE 1', "ar": 0.0},
    {'business_unit': 'TPF', 'product_type': 'RAFFINATE 2', "ar": 0.67},
    {'business_unit': 'TRF', 'product_type': 'BLACK', "ar": 0.2},
]).convert_dtypes()
df_input['date_in'] = pd.to_timedelta(df_input['date_in'])
df_input['date_in'] = pd.to_datetime(df_input['date_in'], format='%Y-%m-%d')
dt.timedelta(days=int(req.params['range_of_days_to_calculate']))
df_input['ticket_duration'] = pd.to_timedelta(df_input).dt.days

# Then
assert_frame_equal(df_result.reset_index(drop=True), df_result_expected.reset_index(drop=True))
````

## parametrize

```python

class TestCalculateShortfallSimpleCase:

    @pytest.mark.parametrize(
        "test_function_name, df_shortfall_parameters, df_monthly_potential, column_name_value_baseline, df_expected",
        [
            (
                    "test_percent_with_percent_missing",
                    pd.DataFrame([
                        {'date': "2022-03-01", 'value': 10},
                        {'date': "2022-04-01", 'value': 0},
                    ]),
                    pd.DataFrame([
                        {'date': "2022-02-01", 'value': 1000},
                        {'date': "2022-03-01", 'value': 1000},
                        {'date': "2022-04-01", 'value': 1000},
                    ]),
                    "f_pot",
                    pd.DataFrame([
                        {'date': "2022-03-01", 'value_percent': 10, 'value_baseline': 1000, 'shortfall': 100},
                        {'date': "2022-04-01", 'value_percent': 0, 'value_baseline': 1000, 'shortfall': 0},
                    ]),
            ),
        ],
    )
    def test_function(self, test_function_name,
                      df_shortfall_parameters, df_monthly_potential, column_name_value_baseline, df_expected):
        # Given
        df_shortfall_parameters = df_shortfall_parameters
        .set_index(pd.DatetimeIndex(pd.to_datetime(list(df_shortfall_parameters['date']), format=DATE_FORMAT)))
        .drop(columns=["date"])

    df_monthly_potential = df_monthly_potential
    .set_index(pd.DatetimeIndex(pd.to_datetime(list(df_monthly_potential['date']), format=DATE_FORMAT)))
    .drop(columns=["date"])


# When
df_result = CalculateShortfallSimpleCase.calculate_shortfall_by_percent(
    df_baseline=df_monthly_potential,
    df_percent=df_shortfall_parameters)

# Expected
df_expected['shortfall'] = df_expected['shortfall'].astype(float)
df_expected = df_expected
.set_index(pd.DatetimeIndex(pd.to_datetime(list(df_expected['date']), format=DATE_FORMAT)))
.drop(columns=["date"])

# Then
pd.testing.assert_frame_equal(df_expected.convert_dtypes(), df_result.convert_dtypes())

```

## test raise Exception
````python
def test_raises():
    with pytest.raises(Exception) as exc_info:   
        raise Exception('some info')
    # these asserts are identical; you can use either one   
    assert exc_info.value.args[0] == 'some info'
    assert str(exc_info.value) == 'some info'
````
# Mock Data

### Gestion Paths

````python
with patch('BDDConnexion.BDD_request.create_engine') as mock_get:
````

dans le source du projet:

- un folder `FOLDER1`
- un sub folder `SUBFOLDER_1`
- un py file `script1.py`
- avec une class `class1` comportant une function `func1`

mocker la function `create_engine`
si import as `import sqlalchemy` then call `sqlalchemy.create_engine`:

````python
with patch('src.FOLDER1.SUBFOLDER_1.script1.class1.func1') as mock_get:
````

ce qui compte c'est le chemin de la fonction où elle est APPELE et non DEFINIT

si import as `from sqlalchemy import create_engine`:

````python
with patch('FOLDER1.SUBFOLDER_1.script1.sqlalchemy.create_engine') as mock_get:
````

### Mock a function/method return

https://stackoverflow.com/questions/60270639/python-mocking-sqlalchemy-connection

```python
import unittest
import db
from unittest.mock import patch
from unittest.mock import Mock


class Cursor:
    def __init__(self, vals):
        self.vals = vals

    def fetchall(self):
        return self.vals


class TestPosition(unittest.TestCase):

    @patch.object(db, '_create_engine')
    def test_get_all_pos(self, mock_sqlalchemy):
        to_test = [1, 2, 3]

        mock_cursor = Mock()
        cursor_attrs = {'fetchall.return_value': to_test}
        mock_cursor.configure_mock(**cursor_attrs)

        mock_execute = Mock()
        engine_attrs = {'execute.return_value': mock_cursor}
        mock_execute.configure_mock(**engine_attrs)

        mock_sqlalchemy.return_value = mock_execute

        args = {'DB': 'test'}
        rows = db.get_all_pos(args)

        mock_sqlalchemy.assert_called_once()
        mock_sqlalchemy.assert_called_with({'DB': 'test'})
        self.assertEqual(to_test, rows)
```

````python
with patch('BDDConnexion.BDD_request.create_engine') as mock_get:
    mock_cursor = Mock()
    cursor_attrs = {'all.return_value': [[elem] for elem in list_well_in_BDD],
                    "cursor.lastrowid": 12}
    mock_get.configure_mock(**cursor_attrs)
````

ici quand on appel l'attribut du résultat tel que : result.cursor.lastrowid on aura 12

Other Way :

````python
class Cursor:
    pass


with patch('BDDConnexion.BDD_request.create_engine') as mock_get:
    mock_cursor = Mock()
    cursor_return = Cursor()
    cursor_return.lastrowid = 12
    cursor_attrs = {'all.return_value': [[elem] for elem in list_well_in_BDD]}
    mock_get.configure_mock(cursor=cursor_return, **cursor_attrs)
````

# Pytest

## parametrize

````python
import pytest


def apply_square_calculation(value: int):
    return value * value


class TestS1pHandler:
    @pytest.mark.parametrize(
        "test_function_name, variable1, expected_result",
        [
            (
                    "test_with_3",
                    3,
                    9
            ),
            (
                    "test_with_4",
                    4,
                    16
            ),
        ]
    )
    def test_function(self, test_function_name, variable1, expected_result):
        # Given

        # When
        result = apply_square_calculation(value=variable1)

        # Then
        assert result == expected_result
````

# Business Driven Development

## Gerkins

### Selenium (front)

Extension chrome pour avoir facilement le nom des elements front `ChroPath`
[Chrome driver pour mac (pour selenium)](https://chromedriver.chromium.org/downloads)
[python package Keyboard Control Functions](https://pyautogui.readthedocs.io/en/latest/keyboard.html)

## Cucumber

# Json validate

[Example](https://pynative.com/python-json-validation/)

## Selenium Apply

### Select Element

[selenium.webdriver.common.by](https://www.selenium.dev/selenium/docs/api/py/webdriver/selenium.webdriver.common.by.html)

````python
class_icon_checked = "material-icons-outlined checked ng-star-inserted"
xpath_researched = '//*[@class="{}"]'.format(class_icon_checked)
xpath_researched = "//button[contains(text(),'Confirm')]"

## Way 1
namedInput = driver.find_element_by_name("A")  # first find your "A" named element
rowElement = namedInput.find_element_by_xpath(xpath_researched)

## Way 2
driver.find_element(By.XPATH, xpath_researched)
driver.find_element(By.XPATH, "//button[@class='btn btn-lg btn-primary']")
driver.find_element(By.CSS_SELECTOR, "#idSIButton9")

# Way 3
driver.findElement(By.xpath("//*[@id='ui_modal_1505889041536']//*[text()='ok']")

## filter on many elements
driver.findElement(By.xpath("//*[@id='ui_modal_1505889041536']//*[text()='ok']")

## one elem or many
elem = driver.find_element_by_xpath(xpath_researched)
elem_list = driver.find_elements_by_xpath(xpath_researched)

## Select Parent
# Way 1
input_el = driver.find_element_by_name('A')
td_p_input = input_el.find_element_by_xpath('..')  # Parent element of input_el

## Way 2
# first find your "A" named element
namedInput = driver.find_element_by_name("A")
# from there find all ancestors (parents, grandparents,...) that are a table row 'tr'
rowElement = namedInput.find_element_by_xpath(".//ancestor::tr")
# from there find the first "selected" tagged option
selectedOption = rowElement.find_element_by_xpath(".//option[@selected='selected']")
````

You were using XPath `//*[@class="home-team"]` but no matter what parent element you are using the // tells XPath to
search the whole document not just the children of the parent element. Using the XPath `.//*[@class="home-team"]` with
the period in front of the forward slashes (IE .//) it tells it to search under the current element only.

### Interaction

````python
elem.sendKeys("Your-Name")
elem.click()
````

#### wait until

````python
WebDriverWait(self.driver, 30).until(ec.visibility_of_element_located((By.CSS_SELECTOR, "#i0116")))
WebDriverWait(self.driver, 30).until(ec.visibility_of_element_located((By.CSS_SELECTOR, "#i0116"))).send_keys(
    USER_LOGIN)
````

# Pandas schema Validation

## Package pandas-schema

[Pypi](https://pypi.org/project/pandas-schema/)

[Package pandas_schema example](https://medium.com/@bogdan.cojocar/how-to-do-column-validation-with-pandas-bbeb38f88990)

[link 2](https://stackoverflow.com/questions/64130610/python-coulmns-validation-using-pandas-schema)

[link 3](https://stackoverflow.com/questions/66729027/data-validation-with-pandas-schema)

````python
from pandas_schema import Column, Schema
from pandas_schema.validation import LeadingWhitespaceValidation, TrailingWhitespaceValidation, CanConvertValidation,

MatchesPatternValidation, InRangeValidation, InListValidation
from pandas_schema.validation import (
    InListValidation
, IsDtypeValidation
, DateFormatValidation
, MatchesPatternValidation
)

schema.validate(df, columns=schema.get_column_names())

schema = Schema([
    Column('Aspiration'),  # text 
    Column('Make', [LeadingWhitespaceValidation(), TrailingWhitespaceValidation()]),  # text
    Column('Body Style', [InListValidation(['hardtop', 'wagon', 'sedan', 'hatchback', 'convertible'])]),
    # text: hardtop, wagon, sedan, hatchback, convertible 
    Column('Symboling', [InRangeValidation(-3, 3)]),  # integer from -3 to 3 
    Column('Wheel Base', [InRangeValidation([86.6, 120.9])]),  # decimal from 86.6 to 120.9 

    # Match a string of length between 1 and 5
    Column('CompanyID', [MatchesPatternValidation(r".{1,5}")]),
    # Match an IP address RegEx (https://www.oreilly.com/library/view/regular-expressions-cookbook/9780596802837/ch07s16.html)
    Column('ip', [MatchesPatternValidation(r"(?:[0-9]{1,3}\.){3}[0-9]{1,3}")]),

    # Match a date-like string of ISO 8601 format (https://www.iso.org/iso-8601-date-and-time-format.html)
    Column('initialdate', [DateFormatValidation("%Y-%m-%d %H:%M:%S")], allow_empty=True),

    # Match only strings in the following list
    Column('customertype', [InListValidation(["type1", "type2", "type3"])]),
    # Match only strings in the following list    
    Column('customersatisfied', [InListValidation(["yes", "no"])], allow_empty=True)
])

[MatchesPatternValidation(pattern=RE_INTEGER_WITHOUT_COMMA_TOLERANCE,
                          message='is not an integer')]
````

## Other package to explore

[pandera](https://pandera.readthedocs.io/en/latest/dataframe_schemas.html#ordered-columns)
