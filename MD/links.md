# Global development
[Organise Folders 1](https://wiki.c2.com/?UnitTestFolderStructure)
[Organise Folders 2](https://docs.pytest.org/en/reorganize-docs/new-docs/user/directory_structure.html)
[on stackoverflow](https://stackoverflow.com/questions/16331235/typical-folder-structure-for-a-python-project-with-unit-and-integration-tests)
[organise datascience project](https://www.kdnuggets.com/2018/07/cookiecutter-data-science-organize-data-project.html)
[organise datascience project 2](https://www.kdnuggets.com/2021/04/how-organize-your-data-science-project-2021.html)

# Python
#### SQL
[SQL Alchemy tutoriel](https://docs.sqlalchemy.org/en/14/tutorial/)
#### Schema
[Panderas : DataFrame Schemas](https://pandera.readthedocs.io/en/latest/dataframe_schemas.html)
[Json Schema : Schemas Validation](https://python-jsonschema.readthedocs.io/en/stable/validate/)
[Json Schema example](https://stackoverflow.com/questions/62841557/python-json-schema-validation-for-array-of-objects)
[Package Schema : moins abouti](https://pypi.org/project/schema/)