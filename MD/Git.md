- [Manipulation & Shortcuts](#manipulation--shortcuts)
    - [Oh My ZSH](#oh-my-zsh)
    - [VIM](#vim)
- [Shortcuts](#shortcuts)
    - [Get Help](#get-help)
- [Methodology & Rules](#methodology--rules)
    - [Rules for the commit naming](#rules-for-the-commit-naming)
        - [Methodology 1](#methodology-1)
- [Links](#links)
- [Initialisation](#initialisation)
    - [File du SSH](#file-du-ssh)
        - [Mise en place](#mise-en-place)
    - [configuration du GIT](#configuration-du-git)
    - [initialisation du Git](#initialisation-du-git)
    - [se connecte au serveur](#se-connecte-au-serveur)
- [Commandes](#commandes)
    - [infos](#infos)
        - [Log](#log)
    - [Branching](#branching)
    - [Commit: photographie de l’etat de votre projet (take a snapshot)](#commit-photographie-de-letat-de-votre-projet-take-a-snapshot)
    - [Push: pousse le code sur le serveur](#push-pousse-le-code-sur-le-serveur)
    - [Fetch](#fetch)
    - [Pull: tire (prend) le code du serveur](#pull-tire-prend-le-code-du-serveur)
    - [Rebase](#rebase)
    - [Merge](#merge)
    - [Stash: Uncommitted changes](#stash-uncommitted-changes)
    - [Squash: regroupe commits](#squash-regroupe-commits)
    - [Reset: reinitialise une branch](#reset-reinitialise-une-branch)
- [Actions, ensemble d'Actions](#actions-ensemble-dactions)
    - [Résolution de conflit de code](#r%C3%A9solution-de-conflit-de-code)
    - [Get Master](#get-master)
    - [Annuler un commit](#annuler-un-commit)
    - [move a commit on top](#move-a-commit-on-top)
    - [Delete all the deads Branch](#delete-all-the-deads-branch)
    - [Change the history of your commit](#change-the-history-of-your-commit)
        - [fixup and autosquash](#fixup-and-autosquash)
        - [Diviser un commit en plusieurs](#diviser-un-commit-en-plusieurs)
    - [Actions Diverses](#actions-diverses)
- [Actions plus complexes](#actions-plus-complexes)
    - [How do I force “git pull” to overwrite local files?](#how-do-i-force-git-pull-to-overwrite-local-files)
        - [For More](#for-more)
        - [Maintain current local commits](#maintain-current-local-commits)

# OhMyZsh
## Cheatsheets
https://github.com/ohmyzsh/ohmyzsh/wiki/Plugins

# Get Help

If you ever need help while using Git, there are three equivalent ways to get the comprehensive manual page (manpage) help for any of the Git commands:

```
git help <verb>
git <verb> --help
git <verb> -h
man git-<verb>
```

# Methodology & Rules

## Rules for the commit naming

### Methodology 1

[Methodology Commit](https://www.conventionalcommits.org/en/v1.0.0/#summary)

```
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

```
docs(changelog): update changelog to beta.5
fix(release): need to depend on latest rxjs and zone.js

The version in our package.json gets copied to the one we publish, and users need the latest of these.
```

<u>**Types:**</u>

- `build`: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
- `chore`: Updating no production task
- `ci`: Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs)
- `docs`: Documentation only changes
- `feat`: A new feature
- `fix`: A bug fix
- `perf`: A code change that improves performance
- `refactor`: A code change that neither fixes a bug nor adds a feature
- `style`: Changes that do not affect the meaning of the code (white-space, formatting missing semi-colons, etc)
- `test`: Adding missing tests or correcting existing tests
- 

# Links

[https://www.youtube.com/watch?v=V6Zo68uQPqE](https://www.youtube.com/watch?v=V6Zo68uQPqE)  
[https://www.youtube.com/watch?v=ExbfGzCcuL8x](https://www.youtube.com/watch?v=ExbfGzCcuL8)  
[https://opensource.com/article/19/5/python-3-default-mac](https://opensource.com/article/19/5/python-3-default-mac)

# Initialisation

## File du SSH

```
C:\Users\kguernigou\OneDrive - Greenflex\00. Administratif\Info Connexion\.ssh

Cd C:\Users\kguernigou\'OneDrive - Greenflex'\'05.Pycharm'

cd /Users/Work/Documents/'Greenflex GitLab Repository'/
cd C:\Users\kguernigou\'OneDrive - Greenflex'\'10.Repository'\
git clone git@gitlab.greenflex.com:data-analytics/pre-invoice.git
git@gitlab.greenflex.com:data-analytics/pre-invoice.git
```

### Mise en place

On bouge dans le dossier `cd /C/Users/kguernigou/'OneDrive - Greenflex'/'05.Pycharm'/'pre-invoice'`

1.  Récupération du statut  
    `git status`
2.  Evaluation du SSH  
    `eval "$(ssh-agent -s)"`
3.  Ajout du SSH
    - `ssh-add /c/Users/'kguernigou'/'OneDrive - Greenflex'/'00. Administratif'/'Info Connexion'/'.ssh'/id_rsa`
    - `C:\Users\kguernigou\'OneDrive - Greenflex'\'00. Administratif'\'Info Connexion'\'05.Pycharm'\'.ssh'\id_rsa`

## configuration du GIT

`git config —global user.name « Kevin Guernigou »`  
`git config —global user.email « kguernigou@greenflex.com »`  
`git config l`

## initialisation du Git

`mkdir PATH` Creer le dossier du projet  
`cd PATH` bouge dedans  
`git --version`  
`git init` Initialise le git

## se connecte au serveur

`git remote add origin URLSERVEUR`  
`git remote -v`

clone le projet chez moi (Actions Standard)  
`git clone ADRESSEDUGIT`

# Commandes

## infos

Donne le status avec les modifications non add  
\- `git status` demande le status

GIT STATUS  
untracked file : fichier pas encore traqué (nouveau)

### Log

pour voir les branchements ; donne les etats du git (les différentes versions) ; show commit history  
`git lg`  
OU  
`git log`  
`git log origin/[filename]` : avec un fetch avant on voit les commits du coup  
`git log -- [filename]` : Voir les modifications qui ont été apporté d'un fichier  
`git log --follow -p -- [filename]` : pour voir vraiment tous les changements dans le detail  
Cela montrera l’historique entier du fichier (y compris l’histoire au-delà des renommage et avec des différences pour chaque modification).  
En d'autres termes, si le fichier nommé `bar` a déjà été nommé `foo`, alors `git log -p bar` (sans l'option `--follow`) affichera uniquement l'historique du fichier jusqu'au point où il a été renommé - il ne montrera pas l'historique du fichier quand il s'appelait `foo`. L'utilisation de `git log --follow -p bar` affiche l'historique complet du fichier, y compris toute modification apportée au fichier alors qu'il s'appelait `foo`. L'option `-p` garantit que les différences sont incluses pour chaque modification.

`git log --decorate` : l'option `--decorate` montre l'état du dépôt distant  
`git log --oneline` : affichage simplifié

## Branching

**montre les branches**  
`git branch`  
`git branch -d [nom-de-ma-branche]`  
○ `-d` est l'abréviation de `--delete` qui indique de supprimer la branche  
`git branch -D [nom-de-ma-branche]`  
○ `-D` est l'abréviation de `--delete --force` qui permet la suppression peut importe si elle a été mergé ou pas

**Créer une nouvelle branche**  
`git checkout -b [name]`  
`git branch MYFEATURE`

**se met dans la branche MYFEATURE**

```
git checkout MYFEATURE
```

**fait la diff entre master et notre branche**

```
git diff master..MYFEATURE
```

**force checkout (throw away local changes)**  
When you run the following command, Git will ignore unmerged entries: (Basically, it can be used to throw away local changes).  
`git checkout -f BRANCH-NAME`  
`git checkout --force BRANCH-NAME` # Alternative  
You can use the git checkout command to undo changes you’ve made to a file in your working directory. This will revert the file back to the version in .  
`git checkout -- FILE-NAME`  
[à voir aussi](#how-do-i-force-git-pull-to-overwrite-local-files)

**supprime une branche**

```
git branch -d client
```

**voir les branch des repo**

```
git branch -r
```

faire un gitch fetch avant quand même

## Commit: photographie de l’etat de votre projet (take a snapshot)

Select which file to add to the commit  
`git add FILE_1`  
pour ajouter tous les fichiers que l'on veut pusher  
`git add .`

`git add -u`  
`-u` (ajoute juste les truc que j'ai modifié, et ca n'ajoute pas les fichiers créer)  
`-a` (ajoute tout)

take a snapshot what is in the staging area  
`git commit —message « le texte qui explique le changement »`  
Commit avec le commentaire suivant  
`git commit -m "ajout d'un systeme de logging"`

Pour rajouter les modifications dans le commit actuel  
`git commit --amend` to amend the current commit  
The `git commit --amend` command is a convenient way to modify the most recent commit. It lets you combine staged changes with the previous commit instead of creating an entirely new commit.  
<u>From Svet</u>: c'est quand tu veux garder le même commit avant tu push il faut mettre `git commit --amend --no-added`

donne les changements effectué dans le code  
`git diff git diff FILEtoCOMPARE`

## Push: pousse le code sur le serveur

On fait un Push  
`git push`  
On fait un Push, `--set-upstream` ca push pour la première fois sur cette branche  
`git push --set-upstream origin logging`

`git push --force`

## Fetch

[More info](https://www.atlassian.com/git/tutorials/syncing/git-fetch)  
La commande `git fetch` va récupérer toutes les données (de la branche courante) des commits qui n'existent pas encore dans votre version en local. Ces données seront stockées dans le répertoire de travail local mais ne seront pas fusionnées avec votre branche locale. Si vous souhaitez fusionner ces données pour que votre branche soit à jour, vous devez utiliser ensuite la commande `git merge`.  
La commande `git pull` est en fait la commande qui regroupe les commandes `git fetch` suivie de `git merge`.  
En fait ca rappatrie les métadata du serveur, il est préferable de faire un `fetch` avant un `pull`

- cela rappatrie les branch du remote que l'on ne voyais pas avant aussi et qui ont été pushé

`git fetch` : rappatrie le code distant sans faire les merges  
`git fetch <branch>` or `git fetch <remote>`  
`git fetch --all` : A power move which fetches all registered remotes and their branches. (Fetch all remotes. : FROM DOC)

`git fetch --prune` == `git fetch -p` : delete les branches qui n'y sont plus (du serveur les vire de notre origin)

## Pull: tire (prend) le code du serveur

`git pull origin master` prend le code du serveur (origin, par defaut), le met sur master  
`git pull --rebase` pull toutes les modification de la branche ou on est  
En fait pull ca ramène les modifications par rapport à ce que l'on a. Avec `rebase` ca remet tout dans le bon ordre et ca refait tout bien, en gros on fait toujours un `rebase` si on est d'avantage sûr du serveur que du local.  
`git pull rebase` : tu te mets sur le mets sur le meme

## Rebase

[Git Link](https://git-scm.com/book/fr/v2/Les-branches-avec-Git-Rebaser-Rebasing)  
[explanation Link](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase)  
`git pull --rebase` : equivaut à `git fetch` puis `git rebase team1/master`  
vous pouvez prendre toutes les modifications qui ont été validées sur une branche et les rejouer sur une autre.

```
git checkout experience
git rebase master
```

On bascule sur la branche expérience et nous y appliquons les modifications apportées a master depuis que `experience` est issue de `master`

Cette commande rejoue les modifications de serveur sur le sommet de la branche master:

```
git rebase master serveur
```

Exécuter `git rebase` avec le flag `-i` démarre une session de rebase interactif. Au lieu de déplacer aveuglément tous les commits vers la nouvelle base, le rebase interactif vous permet de modifier des commits un à un au cours du processus.

**Exemple (voir le gitlink)**  
La branche `serveur` est issue de `master`, puis `client` est issue de `serveur`.  
Ici nous voulons la branche `client` issue de `master` sans les modifications de `serveur`

```
git rebase --onto master serveur client
```
```
git rebase --interactive
```

## Merge

`git merge MYFEATURE` merge

Une fois la résolution de conflit faite : on. supprime la branche, nous n’en avons plus besoin : bonne pratique  
`git branch -d MYFEATURE`  
ne marche pas si la branch n’a pas été mergé (évité de perdre du travail)  
`git br -D` : `branch` peut etre abregé par `br` et `D` veut dire qu’on veut quand meme supprimer la branch

PUSH : pousse le code sur le serveur  
`git push origin master` : pousse mon code sur le serveur (origin, par defaut), ici on push master

```
git checkout master
git log origin/master
git merge origin/master
```

## Stash: Uncommitted changes

Uncommitted changes, however (even staged), will be lost. Make sure to stash and commit anything you need. For that you can run the following:  
`git stash`

And then to reapply these uncommitted changes:  
`git stash pop`

Pour obtenir votre plus récente réserve après avoir exécuté git stash, utilisez :  
`git stash apply`  
Pour voir une liste de vos caches, utilisez  
`git stash list`  
Choisissez un autre git stash à restaurer avec le numéro qui apparaît pour la réserve que vous voulez (la numero de stash est obtenu avec la commande list précedente)  
`git stash apply stash@{2}`

`git stash pop`  
ou  
`git stash pop stash@{2}`

Différence dans Git Stash s'applique et git stash pop ...  
Git Stash Pop : Les données de stash seront supprimées de la pile de la liste de stash.  
Ex:

- `git stash list`

## Squash: regroupe commits

[Link](https://www.ekino.com/articles/comment-squasher-efficacement-ses-commits-avec-git)

Tout d’abord, il faut récupérer le sha1 du commit précédant celui dans lequel on va squasher(du commit à la base de notre branche), puis on l’utilise pour lancer la commande :

```
git rebase -i fbde9fd9c14c9f449f9461b6d3c17c92923b97f0
```

Le premier commit correspond à celui de base, celui que l’on souhaite conserver ; on laisse donc l’instruction “pick” devant. On souhaite squasher tous les commits suivants, on met donc l’instruction “squash” devant. (vous pouvez aussi utiliser l’alias “s” à la place de “squash”).  
Concrètement, on dit à GIT de se baser sur le premier commit et on lui applique tous les suivants pour n’en faire qu’un seul.  
On peut ensuite revérifier toujours à l’aide de la commande tig que nos commits ont bien été squashés.  
puis push force : `git push origin master --force`π

> \[!NOTE\]  
> Pensez à bien utiliser l’option “–force” car le rebase doit écraser l’ancien historique des commits.  
> Attention, l’option —force ne doit être utilisée que sur votre propre fork !

**cas plus complexe**  
Vous remarquerez ici qu’on ne voit pas les commits du merge de master apparaître, grâce au sha1 que l’on a récupéré précédemment ; on travaille uniquement sur les commits de notre branche. (En cas de merge ou de rebase, ils n'aparaissent pas)

Il ne vous reste plus qu’à pusher vos modifications, en `--force`, comme vu précédemment.

```
git push origin master --force
```

> \[!TIP\]  
> Si vous souhaitez faire un rebase depuis le tout premier commit de votre projet, vous devez utiliser l’option `--root`

Le squash de commit rend vos PR beaucoup plus propres et plus lisibles pour les autres, il simplifie aussi le revert en cas de problème. N’hésitez pas à squasher vos PR autant que possible lorsque vous travaillez sur vos projets.

## Reset: reinitialise une branch

[lien git](https://git-scm.com/docs/git-reset)

`git reset --hard origin/master` : sur la branche actuelle tu force le reset à partir de `origin/master`, donc la branche actuelle sera écrasé et tout se quelle contient sera ce qui est sur `origin/master`  
il faut faire un fetch avant

> \[!NOTE\]  
> viter le git reset

## cherry-pick: Ramener un commit d'une branche dans une autre
se placer dans la branche où l'on souhaite ramener le commit
```
git cherry-pick super-long-hash-here-of-the-wished-commit
```

# Actions, ensemble d'Actions

## Résolution de conflit de code

Save tes modifs chez toi dans un truc a part  
`git stash`  
Revient/switch sur la branche master  
`git checkout master`  
Tu récupère tout ce qui est sur la branche master  
`git pull`  
Revient/switch sur la branche "brancheName"  
`git checkout brancheName`  
Toutes les modifications sur master tu les mets dans ta branche  
`git rebase master`  
Tu remets les modifications de cotées dans ta branche  
`git stash pop`  
-\> résolution conflit

## Get Master

On fait quoi la ?  
`ssh-add -l`  
Pour switcher sur la branche master :  
`git checkout master`  
Pour récupérer se qui est dans le repo distant pour le mettre en local.  
`git pull --rebase`

## Annuler un commit

**sale 1**

```
git reset --hard HEAD~1
```

Celle-ci est pertinente tant que les commits n'ont pas été poussés. Git vous retiendra au push d'ailleurs.  
En effet, à partir du moment où un commit existe sur le serveur, il est potentiellement utilisé par des collaborateurs (mergé, à la base d'une branche, etc.). On pourrait faire le sale et forcer le push.

**sale 2: use git reset**  
Si le commit est bon à mettre à la poubelle et que les modifications qu'il comporte sont totalement inutiles, il est possible d'utiliser la commande suivante :

```
git reset --hard HEAD^
```

Cette commande aura pour effet de remonter l'historique sur le commit parent de HEAD en mettant à jour le répertoire de travail et l'index (en d'autres termes, supprimer le dernier commit). Le commit ne sera réellement supprimé que lorsque le garbage collector de Git le supprimera.  
Il est aussi possible de supprimer le dernier commit, tout en conservant ses modifications dans le répertoire de travail, pour cela il faut utiliser la commande suivante :

```
git reset --mixed HEAD^
```

J'espère que cet article vous sera utile. Faites attention à ne pas commiter n'importe quoi !

**sale 3: supprimer un commit au milieu**

```
git rebase -i B
```

avec B le numero de commit avant celui que l'on target. Puis il suffit de supprimer la ligne du commit que l'on veut voir disparaitre

**a faire : creer un commit inverse, surtout si ca a ete pushé**  
Annuler un commit, c'est finalement appliquer l'inverse de son diff !

On peut rediriger le diff des commits à annuler vers la commande patch --reverse :)

```
git diff HEAD^ | patch --reverse
```

Pour faire plus simple, il y a git revert !

Par exemple pour annuler les trois derniers commits :

```
git revert HEAD~3..HEAD
git revert HEAD~3
git revert HEAD~n
git revert HEAD
```

Avec “n” comme étant le nombre de commits à inverser en plus du précédent.

Ou pour annuler un commit en particulier :

```
git revert 444b1cff
```

Il suffit alors de pousser proprement le commit obtenu sur le serveur. Les éventuels collaborateurs qui avaient basé leur travail sur les commits annulés devront gérer les conflits au moment venu...

## move a commit on top

```
feature: A--B--$--C--D
```

So let's say I want to do it, by moving the $ commit on top of feature:

```
new feature: A--B--C--D--$
```

do

```
git rebase -i B
```

avec B le numero de commit.  
And then move $ to the end of the list that shows up in your editor. It will start out as the first line in the file that opens. You could also just delete that line entirely, which will just drop that commit out of your branch's history.

## Delete all the deads Branch

```
git fetch -p && for branch in $(git branch -vv | grep ': gone]' | awk '{​​print $1}​​'); do git branch -D $branch; done
```

`git fetch --prune` == `git fetch -p` : delete les branches qui n'y sont plus (du serveur les vire de notre origin)

`git branch -v` :donne des details sur les branch  
`git branch -vv` : donne encore plus de details sur les branchs

## Change the history of your commit

[https://git-scm.com/book/fr/v2/Utilitaires-Git-Réécrire-l’historique](https://git-scm.com/book/fr/v2/Utilitaires-Git-R%C3%A9%C3%A9crire-l%E2%80%99historique)

- `pick` (abrégé `p`) : Conserver le commit, sans modification.
- `reword` (abrégé `r`) : Ouvre un éditeur de texte pour modifier le message de commit.
- `edit` (abrégé `e`) : Donne la main à l'utilisateur dans un shell pour effectuer des opérations manuelles.
- `squash` (abrégé `s`) : Fusionne le commit avec le précédent, en permettant de définir le message de commit final.
- `fixup` (abrégé `f`) : Fusionne le commit avec le précédent, en conservant le message de commit du précédent.
- `exec` (abrégé `x`) : Exécute une commande arbitraire.
- `drop` (abrégé `d`) : Supprime le commit. Il est aussi possible de simplement supprimer la ligne.

### fixup and autosquash

[https://fle.github.io/git-tip-keep-your-branch-clean-with-fixup-and-autosquash.html](https://fle.github.io/git-tip-keep-your-branch-clean-with-fixup-and-autosquash.html)

- `git commit --fixup <shaCommitOrigin>` automatically marks your commit as a fix of a previous commit
- `git rebase -i --autosquash <shaCommitOrigin-1>` automatically organize merging of these fixup commits and associated normal commits

define a fixup commit

```
$ (dev) git add featureA                # you've removed a pdb : shameful commit
$ (dev) git commit --fixup fb2f677
[dev c5069d5] fixup! Feature A is done
```

Whatch the log

```
$ (dev) git log --oneline
c5069d5 fixup! Feature A is done
733e2ff Feature B is done
fb2f677 Feature A is done
ac5db87 Previous commit
```

Then Autosquash

```
$ (dev) git rebase -i --autosquash ac5db87
pick fb2f677 Feature A is done
fixup c5069d5 fixup! Feature A is done
fixup c9e138f fixup! Feature A is done
pick 733e2ff Feature B is done
```

### Diviser un commit en plusieurs

`git log --oneline`: affichage simplifié

do a rebase interactive and edit the commit concerned

```
pick f7f3f6d changed my name a bit
edit 310154e updated README formatting and added blame
pick a5f4a0d added cat-file
```

Une fois dans l'edit nous effectuons :

```
$ git reset HEAD^
$ git add README
$ git commit -m 'updated README formatting'
$ git add lib/simplegit.rb
$ git commit -m 'added blame'
$ git rebase --continue
```
## Remove directory from remote repository after adding them to .gitignore

The rules in your .gitignore file only apply to untracked files. Since the files under that directory were already committed in your repository, you have to unstage them, create a commit, and push that to GitHub:
```
git rm -r --cached some-directory
git commit -m 'Remove the now ignored directory "some-directory"'
git push origin master
```


## Actions Diverses

# Actions plus complexes

## How do I force “git pull” to overwrite local files?

[Voir aussi : force checkout](#branching)

> \[!IMPORTANT\]  
> Essential information required for user successIf you have any local changes, they will be lost. With or without --hard option, any local commits that haven't been pushed will be lost.

1.  First, run a fetch to update all `origin/<branch>` refs to latest:  
    `git fetch --all`  
    git fetch downloads the latest from remote without trying to merge or rebase anything.
    
2.  Backup your current branch:  
    `git checkout -b backup-master`
    
3.  Then, you have two options :
    
    1.  `git reset --hard origin/master`
    2.  `git reset --hard origin/<branch_name>` If you are on some other branch  
        Then the git reset resets the master branch to what you just fetched. The --hard option changes all the files in your working tree to match the files in origin/master

### For More

Really the ideal way to do this is to not use pull at all, but instead fetch and reset:

```
git fetch origin master
git reset --hard FETCH_HEAD
git clean -df
```

(Altering `master` to whatever branch you want to be following.)

pull is designed around merging changes together in some way, whereas reset is designed around simply making your local copy match a specific commit.

You may want to consider slightly different options to clean depending on your system's needs.

### Maintain current local commits

It's worth noting that it is possible to maintain current local commits by creating a branch from `master` before resetting:

```
git checkout master
git branch new-branch-to-save-current-commits
git fetch --all
git reset --hard origin/master
```

After this, all of the old commits will be kept in `new-branch-to-save-current-commits`.