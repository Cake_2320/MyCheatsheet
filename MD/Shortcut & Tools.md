- [Techno](#techno)
- [Total Digital Factory](#total-digital-factory)
- [Shortcuts & Manip](#shortcuts--manip)
    - [Visual Studio Code](#visual-studio-code)
        - [Plugins](#plugins)
        - [Shortcuts](#shortcuts)
    - [Pycharm](#pycharm)
        - [Plugins](#plugins-1)
        - [Shortcuts](#shortcuts-1)
    - [Sublime Text](#sublime-text)
- [Autres](#autres)
    - [Command Shell](#command-shell)

# Manipulation & Shortcuts

## Oh My ZSH

`tig`: Truc pour la visualisation `git`

[OMZSH for Git](https://www.synbioz.com/blog/tech/astuces-zsh-plugins-git-oh-my-zsh)

[Cheatsheet](https://github.com/ohmyzsh/ohmyzsh/wiki/Cheatsheet)

- `gco`: Git Checkout
- `gco -` : tu retournes sur la branche sur laquelle tu etais
- `gp`: git push
- `gpf`: git push force
- `gpsup`: git push set upstream
- `gl`: git pull
- `ggu` ou `gl -r`: git pull -rebase
- `gc`: git commit
- `gca`: git commit all
- `gcan!`: git commit all -no-edit- amend
- `gcmsg`: git commit msg

- `glg`: git log
- `gst`: git status
- `gd` : git diff
- `ga` : git add
- `gsta` : équivalent à 'git stash'
- `gstp` : équivalent à 'git stash pop'

## VIM

`i` : insert  
`esc` : sortir  
puis  
`:wq` : saver  
`q!` : ne pas sauvegarder

Pour

- p = pick
- d = drop
- r = reword

# Shortcuts

Ctrl + K : commit  
Ctrl + Shift + K : Push

# Techno

**Git - How to close commit editor?**  
Save the file in the editor. If it's Emacs: `CTRL`+`X` `CTRL`+`S` to save then `CTRL`+`X` `CTRL`+`C` to quit or if it's
vi: `:wq`

in_name : foo.txt  
in_dir : ./  
in_path : ./abc.txt  
in_df : le dataframe

Python/Pyspark  
Lazy / Wide / Narrow  
Log Technique / Log Fonctionnel  
Test Unitaire / Test d'intégration  
TDD

`Datafactory` : Orchestration (Extract & Load + Scheduling)

`Class` : Data Access Object  
SP Access Service Provider pour DTLk  
Azure Functions : Petit Volume  
`AMLS` : Au milieu (Bien intégré DataBricks et MLFlow)  
`Databricks` : Gros Volume

Voir CoP Data > DataScience Best Practice

# Total Digital Factory

`Mentor` : Elhadi Boucharab  
`ADSL` : Azure Datalake Service  
`INOX` : (Azure like) Environnement protégé : Espace de travail sécurisé où on mettra les MVP  
-\> le datalake: composant Azure commun à tout total, non compliant INOX  
`Azure V2`: Le datalake  
`CoP` : Community of Project  
`CoP data`: Data management  
`DTMG`: Data Management  
`Data Officer`: Decline dtmg au sein de sa branche  
`Data Galaxy`: Outil metadata data

# Shortcuts & Manip

## Visual Studio Code

### main shortcut

Command Palette : `Cmd`+`Shift`+`P`

Terminal : `Ctrl`+`'`

New Terminal  `Ctrl`+`Shift`+`'`

Show Panel : `Cmd` + `J`

Show Sidebar : `Cmd` + `B`

### Plugins

Installer :  
ext install copy-markdown-as-html  
[https://marketplace.visualstudio.com/items?itemName=jerriepelser.copy-markdown-as-html](https://marketplace.visualstudio.com/items?itemName=jerriepelser.copy-markdown-as-html)  
Dans commande palette chercher markdown  
[https://marketplace.visualstudio.com/items?itemName=yzane.markdown-pdf](https://marketplace.visualstudio.com/items?itemName=yzane.markdown-pdf)  
Dans commande palette chercher export

### Shortcuts

selectionne le mot et tu fais `Cmd` \+ `D` alors il te selectionne tous les apparitions du mot dans la page, si tu veux
tous les modifier d'un seul coup ça tue

Colonne description:

- I : Real Param Name

Global View

| I   | Libellé                  | Windows            | Mac                | Modified |
|-----|--------------------------|--------------------|--------------------|----------|
| X   | Command Palette          |                    | `Cmd`+`Shift`+`P`  |          |
| X   | Open Explorer            |                    | `Cmd`+`Shift`+`E`  |          |
| X   | Show Sidebar             |                    | `Cmd`+`B`          |          |
| X   | Show Panel               |                    | `Cmd`+`J`          |          |
| X   | To preview markdown code | `Ctrl`+`Shift`+`V` |                    |          |
| X   | Terminal                 |                    | `Ctrl`+`'`         |          |
| X   | New Terminal             |                    | `Ctrl`+`Shift`+`'` |          |

Global

| I   | Libellé    | Windows | Mac       | Modified |
|-----|------------|---------|-----------|----------|
| X   | Rechercher |         | `Cmd`+`E` |          |

Code

| I          | Libellé                                      | Windows                  | Mac                | Modified           |
|------------|----------------------------------------------|--------------------------|--------------------|--------------------|
| X          | Reformat Code                                | `Ctrl`+`Alt`+`L`         | `Cmd`+`Opt`+`L`    |                    |
| X          | Optimize Imports                             |                          | `Shift`+`Opt`+`Up` |                    |
|            | Formater le doc                              |                          | `Shift`+`Opt`+`F`  |                    |
| \-\-\-\-\- | \-\-\-\-\-\-\-\-\-                           | \-\-\-\-\-\-\-\-\-       | \-\-\-\-\-\-\-\-\- | \-\-\-\-\-\-\-\-\- |
| X          | Copie la ligne au dessus/ous                 | `Shift`+`Alt`+`Haut/bas` |                    |                    |
| X          | Déplace la ligne au dessus/ous               | `Alt`+`Haut/bas`         |                    |                    |
| X          | Marque le code que vous voulez multiselect + | `Ctrl`+`D`               | `Cmd`+`D`          |                    |
| X          | Rajoute un curseur au dessus/ous             | `Ctrl`+`Alt`+`Haut/bas`  |                    |                    |

Extensions

| I   | Libellé                        | Windows | Mac               | Modified | Extension    |
|-----|--------------------------------|---------|-------------------|----------|--------------|
| X   | Shortcut markdown              |         | `Opt`+`Shift`+`M` |          |              |
| X   | raccourci pour markdown skills |         | `Opt`+`M`         |          | Markdown TOC |

## Pycharm

Cmd + N : Generer

### Plugins

### Shortcuts

#### Selected (already present in "Alls")

| I   | Libellé                         | Windows | Mac               | Modified | Extension    |
|-----|---------------------------------|---------|-------------------|----------|--------------|
| X   | change case (upper, lower) text |         | `Cmd`+`Shift`+`U` |          |              |

#### Alls

`double clic on page on the top` : zoom on it and close temporary alls other extensions

| I   | Libellé                        | Windows | Mac               | Modified | Extension    |
|-----|--------------------------------|---------|-------------------|----------|--------------|
| X   | Shortcut markdown              |         | `Opt`+`Shift`+`M` |          |              |
| X   | raccourci pour markdown skills |         | `Opt`+`M`         |          | Markdown TOC |

`Alt` + `clic`: rentrer dans une fonction  
`Ctrl` + `clic` : tu vois ou la fonction est utiliser  
Les flèches dans le toolbar  
(activer le tool : aller dans View > Appareance)

**En mode debug:**  
`F8` : aller une ligne plus loin  
`F9` : aller au point d'arret suivant

`Cmd` + `O` : = rechercher

| I   | Libellé             | Windows                             | Mac                            | Modified | Context |
|-----|---------------------|-------------------------------------|--------------------------------|----------|---------|
| X   | Run                 | `Shift`+`F10`                       |                                |          |         |
| X   | Debug               | `Shift`+`F9`                        |                                |          |         |
| X   | Evaluate Expression | `Alt`+`F8`                          |                                |          | Debug   |
| X   | Terminal            | `Alt`+`F12`                         |                                |          |         |
|     | Settings            | `Ctrl`+`Alt`+`S`                    |                                |          |         |
|     | Project             | `Alt`+`1`                           |                                |          |         |
|     | Favorites           | `Alt`+`2`                           |                                |          |         |
|     | Bookmark            | `F11` (tester avec `Ctrl` et `Alt`) |                                |          |         |
| X   | Optimize Imports    | `Ctrl`+`Alt`+`O`                    | `Ctrl`+`Opt`+`O`               |          |         |
| X   | Compare Files       |                                     | `Cmd`+`D`                      |          |         |
| X   | Run                 |                                     | `Ctrl`+`R` et `Ctrl`+`Opt`+`R` |          |         |

| I   | Libellé                             | Windows           | Mac               | Modified  |
|-----|-------------------------------------|-------------------|-------------------|-----------|
| X   | Execute Selection in Python Console | `Alt`+`Shift`+`E` | `Opt`+`Shift`+`E` |           |
| X   | Comment with Line Comment           |                   |                   | `Cmd`+`:` |

Code

| I   | Libellé                                                    | Windows                                          | Mac                              | Modified |
|-----|------------------------------------------------------------|--------------------------------------------------|----------------------------------|----------|
| X   | Backspace                                                  | `Shit` \+ `<-`                                   |                                  |          |
| X   | Move Caret to Code Bock End/Start (With selection)         | `Ctrl` \+ `]`/`[` (`Ctrl` \+ `Shift` \+ `]`/`[`) |                                  |          |
| X   | Complete Current Statement                                 | `Ctrl` \+ `Shift` \+ `Entrer`                    |                                  |          |
| X   | Delete Line                                                | `Ctrl` \+ `Y`                                    |                                  |          |
| X   | Delete to Word Start/End                                   | `Ctrl` \+ (`<-`/`Suppr`)                         |                                  |          |
| X   | Down with Selection                                        | `Shift` \+ `down`                                |                                  |          |
| X   | Duplicate line or selection                                | `Ctrl` \+ `D`                                    |                                  |          |
| X   | Add or remove Caret                                        | `Alt` \+ `Click`                                 |                                  |          |
| X   | Move Caret to Next Word                                    | `Ctrl` \+ `droite`                               |                                  |          |
|     | Add carets at selected locations using mouse               | `Shift` \+ `Alt` \+ `Click`                      |                                  |          |
|     | Add carets above or below the current caret using keyboard | `Ctrl` Twice + `up`/`down`                       |                                  |          |
| X   | Stretch to Left/Up…                                        | `Ctrl` \+ `Shift` \+ `Left`/`Up`…                |                                  |          |
| X   | Custom Code Folding                                        | `Ctrl` \+ `Alt` \+ `.`                           |                                  |          |
| X   | Reformat Code                                              | `Ctrl`+`Alt`+`L`                                 | `Cmd`+`Opt`+`L`                  |          |
| X   | Optimize Imports                                           | `Ctrl`+`Alt`+`O`                                 | `Ctrl`+`Opt`+`O`                 |          |
| X   | Code Expand Folding (All)                                  | `Ctrl` \+ `Dollar` \+ `=` (\+ `Shift`)           | `Cmd` \+ OU `Cmd` = (\+ `Shift`) | OUI      |
| X   | Code Collapse Folding (All)                                | `Ctrl` \+ `^` \+ (\+ `Shift`)                    | `Cmd` \- (\+ `Shift`)            | OUI      |

| I   | Libellé                                                  | Windows                          | Mac | Modified |
|-----|----------------------------------------------------------|----------------------------------|-----|----------|
| X   | Show Diff / Compare Files                                | `Ctrl`+`D`                       |     |          |
| X   | Next/Previous Difference (also for conflict but i added) | `F7` / (`Shift` \+ `F7`)         |     |          |
| X   | Move Line Up/Down                                        | `Alt` \+ `Shift` \+ `up`/`down`  |     |          |
| X   | Move Statement Up/Down                                   | `Ctrl` \+ `Shift` \+ `up`/`down` |     |          |
| X   | Next/Previous Method                                     | `Alt` \+ `up`/`down`             |     |          |
| X   | Next/Previous Occurence                                  | `Ctrl` \+ `Alt` \+ `up`/`down`   |     |          |
| X   | Run Step Over                                            | `F8`                             |     |          |
| X   | Resume Program                                           | `F9`                             |     |          |

## Sublime Text

| I   | Libellé                               | Windows                      | Mac                           | Modified |
|-----|---------------------------------------|------------------------------|-------------------------------|----------|
| X   | Indent                                | `Ctrl` \+ `]`                | `Cmd` \+ `]`                  |          |
| X   | Unindent                              | `Ctrl` \+ `[`                | `Cmd` \+ `[`                  |          |
| X   | Place le curseur sur plusieurs lignes | Right Mouse Button + `Shift` | `Alt` \+ Curseur cliqué       |          |
| X   | Place le curseur sur plusieurs lignes | `Ctrl` \+ `Alt` \+ `Flèche`  | `Ctrl` \+ `Shift` \+ `Flèche` |          |

# Autres

DSL au lieu des `self` normalement tu retournes des `clause`

how to define clauses  
how to write a custom DSL in python  
DSL in python

StringM

revision date not null  
workshopname not null

## Command Shell

`rm -i MyFile.rtf` But there’s a safety net: it’s the -i, or interactive, flag.  
`rm -i MyFile.rtf MyCV.rtf MyGreatAmericanNovel.rtf`  
`rmdir Archives`  
The rm command has a powerful option, -R (or -r), otherwise known as the recursive option.  
`rm -R Archives`  
`rm -iR Archives`