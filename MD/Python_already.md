Python

$$
Table Of Content


$$

*[Summary](#table-of-content)*

# Table Of Content

- [Table Of Content](#table-of-content)
- [Industrialisation](#industrialisation)
- [Configuration](#configuration)
- [Python](#python)
    - [Module](#module)
    - [File System](#file-system)
        - [File format](#file-format)
    - [Import / Export](#import--export)
        - [Json](#json)
        - [XML](#xml)
    - [Measure time execution](#measure-time-execution)
- [Objets](#objets)
    - [Infos](#infos)
    - [Les listes](#les-listes)
        - [Comprehensive lists](#comprehensive-lists)
    - [Les dictionnaires](#les-dictionnaires)
    - [Class](#class)
        - [Héritage](#h%C3%A9ritage)
        - [Atrributes and functions](#atrributes-and-functions)
    - [Hinting](#hinting)
- [Les Opérateurs de fonctions](#les-op%C3%A9rateurs-de-fonctions)
    - [Try, Catch](#try-catch)
    - [Itérations](#it%C3%A9rations)
    - [Boucles](#boucles)
        - [Python loop for inside lambda](#python-loop-for-inside-lambda)
    - [Iterate and apply on row dataframe](#iterate-and-apply-on-row-dataframe)
        - [Apply Dataframe](#apply-dataframe)
    - [Iterrow](#iterrow)
- [Manipulations](#manipulations)
    - [manipulations Diverses](#manipulations-diverses)
    - [Booleans](#booleans)
        - [Les expressions logiques](#les-expressions-logiques)
        - [On Booleans](#on-booleans)
        - [Booleans on Dataframe](#booleans-on-dataframe)
        - [Change a Value Conditionnal](#change-a-value-conditionnal)
    - [Les Chaines de caractère](#les-chaines-de-caract%C3%A8re)
        - [String Manipulation](#string-manipulation)
    - [Expression régulières](#expression-r%C3%A9guli%C3%A8res)
        - [écriture](#%C3%A9criture)
        - [Applications](#applications)
- [Fonctions et opérations](#fonctions-et-op%C3%A9rations)
    - [Manipulations](#manipulations-1)
        - [Count](#count)
        - [iterable : range](#iterable--range)
        - [Sort](#sort)
        - [Concatenation](#concatenation)
        - [Checks](#checks)
        - [Filter / Select](#filter--select)
        - [supprimer les doublons](#supprimer-les-doublons)
        - [Map, Apply](#map-apply)
        - [Reduce](#reduce)
        - [Manipulate list in list or list in column dataframe](#manipulate-list-in-list-or-list-in-column-dataframe)
- [Datetime](#datetime)
    - [Links](#links)
    - [Converting Strings Using datetime](#converting-strings-using-datetime)
        - [More Packages](#more-packages)
    - [Timezone](#timezone)
        - [Converting Timezone](#converting-timezone)
- [Object Mapipulation](#object-mapipulation)
    - [Dict](#dict)
        - [remove key](#remove-key)
- [Dataframe](#dataframe)
    - [infos](#infos-1)
        - [Numpy](#numpy)
        - [Create](#create)
        - [Index](#index)
        - [Drop](#drop)
        - [Columns](#columns)
            - [Add a column](#add-a-column)
            - [drop a column](#drop-a-column)
            - [Concatenation with Series](#concatenation-with-series)
            - [Calcul](#calcul)
- [Other Packages](#other-packages)
    - [Design Pattern](#design-pattern)
    - [PyValidator (Fluent Validation)](#pyvalidator-fluent-validation)
- [Spark](#spark)

$$
Let's Show Begin


$$

# Industrialisation

Sphinx : package de generation de documentation automatique a partir du code

[marshmallow](https://marshmallow.readthedocs.io/en/stable/) : Package pour gerer les chema, notamment pour l'export en JSON  
[https://stackoverflow.com/questions/27585192/how-to-convert-a-list-of-object-to-json-format-in-python](https://stackoverflow.com/questions/27585192/how-to-convert-a-list-of-object-to-json-format-in-python)

[https://json-schema.org/understanding-json-schema/reference/numeric.html#integer](https://json-schema.org/understanding-json-schema/reference/numeric.html#integer)

import glob  
import os

list\_of\_files = glob.glob('/path/to/folder/*') # * means all if need specific format then *.csv  
latest\_file = max(list\_of_files, key=os.path.getctime)  
print latest_file

# Configuration

`pip install requests`

# Python

[Cours python ENSAE](http://www.xavierdupre.fr/app/ensae_teaching_cs/helpsphinx/notebooks/td1a_cenonce_session_10.html)  
[w3Schools](https://www.w3schools.com/python/)

* * *

## Module

```python
import importlib

moduleName = input('Enter module name:')
importlib.import_module(moduleName)
```

## File System

```python
import sys
import os

scriptpath = "../Test/MyModule.py"

# Add the directory containing your module to the Python path (wants absolute paths)
sys.path.append(os.path.abspath(scriptpath))

# Do the import
import MyModule

os.chdir()
os.path.join(os.path.dirname(__file__)
```

`..\rep1\fic1.txt`: J'utilise ici des antislashs parce que l'exemple d'arborescence est un modèle Windows et que ce sont les séparateurs utilisés pour décrire une arborescence Windows. Mais, dans votre code je vous conseille quand même d'utiliser un slash(/)

`os.getcwd()`: CWD = « Current Working Directory »

placez-vous, à l'aide de `os.chdir`, dans un répertoire de test créé pour l'occasion.

`os.listdir(path)`

```python
from pathlib import Path

print(Path(str_path_var.parent.absolute())
```

### File format

```python
op1 = byte_file.read()
op2 = op1.decode("utf-8")
op2 = str(op1)
```


* * *

# Objets


## Itérations

```python
- # On dictionnary
filter(lambda item: self.worksheet[item[1]].fill.start_color.index == 'FF000000', choices.items())
dict(filter(lambda elem: elem[0] % 2 == 0, dictOfNames.items()))
{k: v for k, v in points.items() if v[0] < 5 and v[1] < 5}
{ key:value for (key,value) in dictOfNames.items() if key % 2 == 0}
for key in d:
    print(key, d[key])
for value in d.values():
    print(value)
for key, value in d.items():
    print(key, value)
```


## Iterate and apply on row dataframe

### Apply Dataframe

```python
df['col3'] = df.apply(lambda x: x['col1'] * x['col2'], axis=1)
df['col1'] = df['col1'].apply(complex_function) # avec 'complex_function' a 1 paramètre
df['col3'] = [dict_of_dicts[x][y] for x, y in zip(df['col1'], df['col2'])]
df.assign(a=lambda df: df.a / 2) # Returns a new object with all original columns in addition to new ones. Existing columns that are re-assigned will be overwritten.
df['col1'] = df['col1'].apply(complex_function)
```

## Iterrow

```python
- # Series: values
- # DataFrame: column labels

for index, row in df.iterrows():
    print(row['c1'], row['c2'])

for row in df.itertuples():
    print(row)
```

To iterate over the rows of a DataFrame, you can use the following methods:

- `iterrows()`: Iterate over the rows of a DataFrame as (index, Series) pairs. This converts the rows to Series objects, which can change the dtypes and has some performance implications.
    
- `itertuples()`: Iterate over the rows of a DataFrame as namedtuples of the values. This is a lot faster than *`iterrows()`*, and is in most cases preferable to use to iterate over the values of a DataFrame.
    

Iterating through pandas objects is generally slow. In many cases, iterating manually over the rows is not needed and can be avoided with one of the following approaches:

- Look for a vectorized solution: many operations can be performed using built-in methods or NumPy functions, (boolean) indexing, …
    
- When you have a function that cannot work on the full DataFrame/Series at once, it is better to use `apply()` instead of iterating over the values. See the docs on [function application](https://pandas.pydata.org/pandas-docs/stable/user_guide/basics.html#basics-apply).
    
- If you need to do iterative manipulations on the values but performance is important, consider writing the inner loop with cython or numba. See the [enhancing performance](https://pandas.pydata.org/pandas-docs/stable/user_guide/enhancingperf.html#enhancingperf) section for some examples of this approach.
    

Iteration in Pandas is an anti-pattern and is something you should only do when you have exhausted every other option. You should not use any function with "iter" in its name for more than a few thousand rows or you will have to get used to a lot of waiting.

Do you want to print a DataFrame? Use `DataFrame.to_string()`.

Faster than Looping: Vectorization, Cython

A good number of basic operations and computations are "vectorised" by pandas (either through NumPy, or through Cythonized functions). This includes arithmetic, comparisons, (most) reductions, reshaping (such as pivoting), joins, and groupby operations. Look through the documentation on Essential Basic Functionality to find a suitable vectorised method for your problem.

[How not iterate](https://stackoverflow.com/questions/16476924/how-to-iterate-over-rows-in-a-dataframe-in-pandas)

# Manipulations

## manipulations Diverses

```python
if x is not None
if x not in ['', None]
if (x != '')
```

## Booleans

### Les expressions logiques

```python
not True

booleans = [True, False, True, False, True]
negation_iterator = map(operator.not_, booleans)
print(list(negation_iterator))
```

### On Booleans

```python
[True, True, False]
~df_test


- # invert
not bool # for a value
[not elem for elem in mylist] # For a List
~boolean_serie_or_pdf # For a serie and a dataframe


- # For list
 all(iterable)
 any( all(iterable))
- # For Series
s.all()
s.any()
pdf..all(axis=None) # tous les elements
pdf.all(axis='columns').tolist() # on peut avoir index aussi et le retransformer en list
```

[getting the index of True values for a boolean series](https://stackoverflow.com/questions/52173161/getting-a-list-of-indices-where-pandas-boolean-series-is-true)

### Booleans on Dataframe

```python
a_series = pd.Series([True, False, True])
inverted = ~a_series
print(inverted)

df.all() # check by columns
df.all(axis='columns') # check by rows
df.all(axis=None) # check tout
```

### Change a Value Conditionnal

```python
df.[boolean_condition, column_name] = new_value # change a value conditionnal
```

**dataframe**

```python
- # Select
df[:3] = 0 # les 3 premieres lignes (seront egales à 0)

df.loc[boolean_condition, column_name] = new_value # change a value conditionnal
s[s>1] = 0

- # Using loc for Replace
mask = df.my_channel > 20000
column_name = 'my_channel'
df.loc[mask, column_name] = 0

- # Using Mask
df['Event'].mask(df['Event'] == 'Hip-Hop', 'Jazz', inplace=True)

- # Using numpy where
df['Event'] = np.where((df.Event == 'Painting'),'Art',df.Event)
```

## Les Chaines de caractère

### String Manipulation

```python
chaine1 = "une petite phrase"
chaine2 = chaine1.upper() # On met en majuscules chaine1

- # Prints the string by replacing only 3 occurrence of geeks by GeeksforGeeks
print(string.replace("geeks", "GeeksforGeeks", 3)) 

- # STRIP
- # The strip() method returns a copy of the string by removing both the leading and the trailing characters (based on the string argument passed).
string.strip([chars])
- # Apply strip to column of lists in DataFrame
df['AD'] = [[val.strip() for val in sublist] for sublist in df['AD'].values]
-- # You can do
df['AD'] = df['AD'].map(lambda l: list(map(lambda x: x.strip('o'), l)))
-- # or if you only need to remove whitespaces
df['AD'] = df['AD'].map(lambda l: list(map(str.strip, l)))
- # Example apply split then strip on a column dataframe:
dataframe[column] = [[val.strip() for val in sublist] for sublist in dataframe[column].str.split(';').values]
dataframe[column] = dataframe.apply(axis=1, func=lambda x: [val.strip() for val in x[column].split(';')])
dataframe[column] = dataframe[column].apply(lambda x: [val.strip() for val in x.split(';')])


- # The split() method splits a string into a list.
- # You can specify the separator, default separator is any whitespace.
"welcome to the jungle".split()
string.split(', ') # split par la string en list via la value donnée (default with ' ')
str.split(separator, maxsplit) # maxsplit : It is a number, which tells us to split the string into maximum of provided number of times
'_'.join(('welcome', 'to', 'stack', 'overflow')) # pour revert l'opération

- # Retirer le dernier element d'une string
name = 'Geekflare'
name[-1] # 'e'
name[-len(name)] # 'G'

buggy_name = 'GeekflareE'
buggy_name[:-1] # 'Geekflare'
buggy_name.rstrip(buggy_name[-1]) # 'Geekflare'
```

[voir paragraphe : Manipulate list in list or list in column dataframe](#manipulate-list-in-list-or-list-in-column-dataframe)

**Dataframe**

[For Series](https://pandas.pydata.org/pandas-docs/stable/user_guide/text.html#text-concatenate)

```python
df = DataFrame({'foo':['a','b','c'], 'bar':[1, 2, 3], 'new':['apple', 'banana', 'pear']})

df.astype(str).add('_').sum(axis=1).str[:-len('_')]

- # concatene la serie avec le text
pd.concat([serie, s_text], axis='columns').add(' ; ')
- # concatene les colonnes avec un ' ; ' entre chaque valeurs
msg_list = pdf_msg.add(' ; ')
        msg_list = msg_list.mask(msg_list == ' ; ', '').sum(
            axis=1).str[:-len(' ; ')].tolist()

df['combined'] = df['bar'].astype(str)+'_'+df['foo']+'_'+df['new']
df1 = df['1st Column Name'].map(str) + df['2nd Column Name'].map(str) # Also note that if your dataset contains a combination of integers and strings for example, and you are trying to use the above template, you’ll then get this error: ‘TypeError: ufunc ‘add’ did not contain a loop with signature matching types' ; You can bypass this error by mapping the values to strings 


big = pd.concat([df] * 10**4, ignore_index=True) # big.shape (30000, 3)

In [5]: %timeit big['bar'].astype(str)+'_'+big['foo']+'_'+big['new']
10 loops, best of 3: 44.2 ms per loop

In [6]: %timeit big.ix[:, :-1].astype(str).add('_').sum(axis=1).str.cat(big.new) # big.new c'est la dernière colonne que l'on rajoute quoi
10 loops, best of 3: 72.2 ms per loop

In [11]: %timeit big.astype(str).add('_').sum(axis=1).str[:-1]
10 loops, best of 3: 82.3 ms per loop
```

Concatenation des messages dans un dataframe

```python
msg_list = df_msg.add(' ; ')
msg_list = msg_list.mask(msg_list == ' ; ', '').sum(axis=1).str[:-len(' ; ')].tolist()
```

## Expression régulières

### écriture

Pour matérialiser la fin de la chaîne, vous utiliserez le signe `$`. Ainsi, l'expression q sera trouvée uniquement si votre chaîne se termine par la lettre q minuscule.  
`^`: debut de la chaine de caractère  
`$`: find e la chaine de caractère

`*`: N'importe quelle valeure une fois  
`.*`: N'importe quelle chaine de valeur de longueur comme vous voulez (de 0 à l'infini)

| Signe | Explication | Expression | Chaînes contenant l'expression |
| --- | --- | --- | --- |
| *   | 0, 1 ou plus | abc* | `'ab'`,`'abc'`,`'abcc'`,`'abcccccc'` |
| +   | 1 ou plus | abc+ | `'abc'`,`'abcc'`,`'abcccccc'` |
| ?   | 0 ou 1 | abc? | `'ab'`,`'abc'` |

`(.*)` : Recherchera l'expression  
`(.*?)` : Recherchera la plus petite expression

`E{4}`: signifie 4 fois la lettre E majuscule  
`E{2,4}`: signifie de 2 à 4 fois la lettre E majuscule  
`E{,5}`: signifie de 0 à 5 fois la lettre E majuscule  
`E{8,}`: signifie 8 fois minimum la lettre E majuscule  
`E?`: signifie de 0 à 1 fois la lettre E majuscule (peut être présente)

Pour symboliser les caractères spéciaux dans les expressions régulières, il est nécessaire d'échapper l'anti-slash en le faisant précéder d'un autre anti-slash. Cela veut dire que pour écrire le caractère spécial\\w, vous allez devoir écrire `\\w`

Voici une liste complète des métacaractères ; leur sens

```
. ^ $ * + ? { } [ ] \ | ( )
```

est décrit dans la suite de ce guide.

**Les classes de caractères**  
Vous pouvez préciser entre crochets plusieurs caractères ou classes de caractères. Par exemple, si vous écrivez`[abcd]`, cela signifie : l'une des lettres parmi `a`, `b`, `c` et `d`

`\d`: Correspond à n'importe quel caractère numérique ; équivalent à la classe `[0-9]`.  
`\D`: Correspond à n'importe caractère non numérique ; équivalent à la classe `[^0-9]`.  
`\s`: Correspond à n'importe quel caractère "blanc" ; équivalent à la classe `[ \t\n\r\f\v]`.  
`\S`: Correspond à n'importe caractère autre que "blanc" ; équivalent à la classe `[^ \t\n\r\f\v]`.  
`\w`: Correspond à n'importe caractère alphanumérique ; équivalent à la classe `[a-zA-Z0-9_]`.  
`\W`: Correspond à n'importe caractère non-alphanumérique ; équivalent à la classe `[^a-zA-Z0-9_]`.  
Ces séquences peuvent être incluses dans une classe de caractères. Par exemple, \[\\s,.\] est une classe de caractères qui correspond à tous les caractères "blancs" ou `','` ou `'.'`.

**Les groupes**  
`(cha){2,5}` : la séquence'cha'répétée entre deux et cinq fois. Les séquences'cha'doivent se suivre naturellement

### Applications

```python
chaine1 = "une petite phrase"
chaine2 = chaine1.upper() # On met en majuscules chaine1

import re
match(): Détermine si la RE fait correspond dès le début de la chaîne.
objects are always true, and None is returned if there is no match
search(): Analyse la chaîne à la recherche d’une position où la RE correspond.
findall() : Trouve toutes les sous-chaînes qui correspondent à la RE et les renvoie sous la forme d’une liste.
finditer(): Trouve toutes les sous-chaînes qui correspondent à la RE et les renvoie sous la forme d’un itérateur.

group(): Renvoie la chaîne de caractères correspondant à la RE
start(): Renvoie la position de début de la correspondance
end(): Renvoie la position de fin de la correspondance
span(): Renvoie un tuple contenant les positions (début, fin) de la correspondance

bool(re.match(expression, chaine))
``````python
import re
DATA = "Hey, you - what are you doing here!?"
print re.findall(r"[\w']+", DATA)
# Prints ['Hey', 'you', 'what', 'are', 'you', 'doing', 'here']

``````python
s = 'abcd1234dcba'

print(s.find('a'))  # 0
print(s.find('cd'))  # 2
print(s.find('1', 0, 5))  # 4
print(s.find('1', 0, 2))  # -1
``````python
s = 'abcd1234dcba'

print(s.rfind('a'))  # 11
print(s.rfind('a', 0, 20))  # 11
print(s.rfind('cd'))  # 2
print(s.rfind('1', 0, 5))  # 4
print(s.rfind('1', 0, 2))  # -1
```

**Dataframe**  
[Examples of applies](https://kanoki.org/2019/11/12/how-to-use-regex-in-pandas/)

```python
S.str.count('^[pP].*')>0]
S[S.str.match(r'(^P.*)')==True] # match () function is equivalent to python’s re.match() and returns a boolean value
S.replace('(-d)','',regex=True, inplace = True) # Replaces all the occurence of matched pattern in the string.
S.str.findall('^[Ff].*') # It calls re.findall() and find all occurence of matching patterns.
S.str.contains('^F.*') # It uses re.search() and returns a boolean value.
s.str.split(r"s", n=-1,expand=True) # This is equivalent to str.split() and accepts regex, if no regex passed then the default is \s (for whitespace).
S.str.rsplit() # it is equivalent to str.rsplit() and the only difference with split() function is that it splits the string from end.

# Extract strings with a specific regex
df= df['col_name'].str.extract[r'[Aa-Zz]']

# Replace strings within a regex
df['col_name'].str.replace('Replace this', 'With this')
```

**Both**

```python
# Equivalent
bool(re.match(r"^[A-Za-z0-9 -]*[ -]?[A-Za-z0-9]*$", value)) # for value
pdf[variable_name].astype('str').str.match(expression) # for pdf
```

# Fonctions et opérations

## Manipulations

### Count

- pandas.DataFrame
    - Display number of rows, columns, etc.: `df.info()`
    - Get the number of rows: `len(df)`
    - Get the number of columns: `len(df.columns)`
    - Get the number of rows and columns: `df.shape`
    - Get the number of elements: `df.size`
    - Notes when specifying index
- pandas.Series
    - Get the number of elements: `len(s)`, `s.size`

### iterable : range

```python
range()
   5, 6, 7, 8, 9


range(5, 10)
   5, 6, 7, 8, 9

range(0, 10, 3)
   0, 3, 6, 9

range(-10, -100, -30)
  -10, -40, -70

range(3) == range(0,3)

list(range(3))
```

### Sort

```python
# Object: With ut an object
## To sort the list in place...
ut.sort(key=lambda x: x.count, reverse=True)

## To return a new list, use the sorted() built-in function...
newlist = sorted(ut, key=lambda x: x.count, reverse=True)
```

### Concatenation

```python
pd.concat(serie_1, serie_2], axis='columns') # les regroupes en mettant chaque element avec chaque element
```

### Checks

```python
df['your column name'].isnull()
df.isnull().sum().sum()
```

Test de type

```python
No:  type(x) is pd.DataFrame
No:  type(x) == pd.DataFrame
Yes: isinstance(x, pd.DataFrame)
isinstance(x, pd.DataFrame) # dataframe
```

### Filter / Select

```python
filter(function, iterable)

# show a filter
print(list(filter_result))

# Typiquement, ce code peut être remplacé par une liste en intention dans le pur style Python :
majeurs = [a for a in ages if a > 18] # On peut en plus ajouter une transformation à 'a' facilement si on le désire

l = [1, 0, None, [], True]
print filter(bool, l)
[1, True]
```

liste

```python
[elem for elem in li if li.count(elem) == 1]
```

**dataframe**  
Dataframe column filter

```python
titanic[["Age", "Sex"]]
titanic[titanic["Age"] > 35]
titanic[(titanic["Pclass"] == 2) | (titanic["Pclass"] == 3)]
titanic[titanic["Age"].notna()]
```

Row Filter

```python
pandas.Series.isin(['foo', 'bar']) # Renvoie un booleen qui contient true pour les valeurs qui sont soit 'foo' soit 'bar'

- # By index
df[df.index.isin(my_list)]
```

### supprimer les doublons

```python
- # for List
nbrListe = list(set(nbrListe))
- # or
for i in nbrListe : 
    if i not in new_list: 
        new_list.append(i)
```

### Map, Apply

map() function returns a map object(which is an iterator) of the results after applying the given function to each item of a given iterable (list, tuple etc.)

> NOTE : The returned value from map() (map object) then can be passed to functions like list() (to create a list), set() (to create a set) .

```python
- # Return double of n 
def addition(n): 
    return n + n 
  
- # We double all numbers using map() 
numbers = (1, 2, 3, 4) 
result = map(addition, numbers) 
print(list(result)) 
map(fun, iter) # applique sur la fontion sur les elements de l'itération

map(unicode.upper, memes)
- # peut se traduire par:
s.upper() for s in memes

h, m, s = map(int, '8:19:22'.split(':'))

- # Add two lists using map and lambda   
numbers1 = [1, 2, 3] 
numbers2 = [4, 5, 6]
result = map(lambda x, y: x + y, numbers1, numbers2) 
print(list(result))
```

> \[!NOTE\]  
> For dictionnaries go to the dictionnaries section also [HERE](#les-dictionnaires)

### Reduce

`reduce()` est plus tordu. La fonction doit prendre deux paramètres en entrée, et retourner une valeur. Au premier appel, les deux premiers éléments de l’itérable sont passés en paramètres. Ensuite, le résultat de cet appel et l’élément suivant sont passés en paramètre, et ainsi de suite.

### Manipulate list in list or list in column dataframe

Example avec une colonne que l'on split puis pour chaque element de la liste créé on la strip

```python
df['AD'] = [[val.strip() for val in sublist] for sublist in df['AD'].values]
-- # You can do
df['AD'] = df['AD'].map(lambda l: list(map(lambda x: x.strip('o'), l)))
-- # or if you only need to remove whitespaces
df['AD'] = df['AD'].map(lambda l: list(map(str.strip, l)))
- # Example apply split then strip on a column dataframe:
dataframe[column] = [[val.strip() for val in sublist] for sublist in dataframe[column].str.split(';').values]
dataframe[column] = dataframe.apply(axis=1, func=lambda x: [val.strip() for val in x[column].split(';')])
dataframe[column] = dataframe[column].apply(lambda x: [val.strip() for val in x.split(';')])
```

* * *

* * *

# Object Mapipulation

## Dict

### remove key

```python
dictionary.pop(key[, default])
result = word_freq_dict.pop(key_to_be_deleted, None)

list(map(lambda x: x.pop("index"), self.obj_dict["data"]))
```

- If the given key exists in the dictionary then dict.pop() removes the element with the given key and return its value.
- If the given key doesn’t exist in the dictionary then it returns the given Default value.
- If the given key doesn’t exist in the dictionary and No Default value is passed to pop() then it will throw KeyError

Ne pas faire le default, sinon en cas de non existence de la key: error

# Dataframe

[Pandas Documentation](https://pandas.pydata.org/pandas-docs/stable/user_guide/basics.html#iteration)

## infos

```python
Df_panda.dtypes
Df_panda.info()
Df_panda.shape
Df_panda.describe()
```

Some of the important attributes of a NumPy object are:

```
Ndim: displays the dimension of the array
Shape: returns a tuple of integers indicating the size of the array
Size: returns the total number of elements in the NumPy array
Dtype: returns the type of elements in the array, i.e., int64, character
Itemsize: returns the size in bytes of each item
Reshape: Reshapes the NumPy array
```

NumPy array elements can be accessed using indexing. Below are some of the useful examples:

- `A[2:5]` will print items 2 to 4. Index in NumPy arrays starts from 0
- `A[2::2]` will print items 2 to end skipping 2 items
- `A[::-1]` will print the array in the reverse order
- `A[1:]` will print from row 1 to end

functions

- `head()`: returns the top 5 rows in the dataframe object
- `tail()`: returns the bottom 5 rows in the dataframe
- `info()`: prints the summary of the dataframe
- `describe()`: gives a nice overview of the main aggregated values over each column

### Numpy

```python
import numpy as np
Array: une matrice, que des elements de meme nature
a = np.array([1, 2, 3])
```

np.array

```python
a = np.array([1, 2, 3])
```

np.ones

```python
np.ones( (3,4), dtype=np.int16 )  
array([[ 1,  1,  1,  1],
       [ 1,  1,  1,  1],
       [ 1,  1,  1,  1]])
```

np.full

```python
np.full( (3,4), 0.11 )  
array([[ 0.11,  0.11,  0.11,  0.11],        
  [ 0.11,  0.11,  0.11,  0.11],        
  [ 0.11,  0.11,  0.11,  0.11]])
```

np.arange

```python
np.arange( 10, 30, 5 )
array([10, 15, 20, 25])
 
>>> np.arange( 0, 2, 0.3 )             
- # it accepts float arguments
array([ 0. ,  0.3,  0.6,  0.9,  1.2,  1.5,  1.8])
```

np.linspace

```python
np.linspace(0, 5/3, 6)
array([0. , 0.33333333 , 0.66666667 , 1. , 1.33333333  1.66666667])
```

np.random.rand(2,3)

```python
np.random.rand(2,3)
array([[ 0.55365951,  0.60150511,  0.36113117],
       [ 0.5388662 ,  0.06929014,  0.07908068]])
```

np.empty((2,3))

```python
np.empty((2,3))
array([[ 0.21288689,  0.20662218,  0.78018623],
       [ 0.35294004,  0.07347101,  0.54552084]])
```

### Create

```python
- # Import pandas library 
import pandas as pd

df = pd.DataFrame({'col1': np.random.randn(3),
                   'col2': np.random.randn(3)}, index=['a', 'b', 'c'])

pd.DataFrame() # Empty
pd.DataFrame(columns=['User_ID', 'UserName', 'Action']) # Empty

pd.DataFrame(columns=['User_ID', 'UserName', 'Action'], index=['a', 'b', 'c'])


data = [['tom', 10], ['nick', 15], ['juli', 14]] # initialize list of lists
df = pd.DataFrame(data, columns = ['Name', 'Age']) # Create the pandas DataFrame

data = [{'a': 1, 'b': 2, 'c':3}, {'a':10, 'b': 20, 'c': 30}] # Initialise data to lists.
df = pd.DataFrame(data) # Create DataFrame

data = {'Name':['Tom', 'nick', 'krish', 'jack'], 'Age':[20, 21, 19, 18]} # intialise data of lists.
df = pd.DataFrame(data) # Create DataFrame
df = pd.DataFrame(data, index =['rank1', 'rank2', 'rank3', 'rank4'])
```

Cas Particulier

```python
- # Intitialise lists data. 
data = [{'a': 1, 'b': 2}, {'a': 5, 'b': 10, 'c': 20}] 
   
- # With two column indices, values same  
- # as dictionary keys 
df1 = pd.DataFrame(data, index =['first', 'second'], columns =['a', 'b']) 
   
- # With two column indices with  
- # one index with other name 
df2 = pd.DataFrame(data, index =['first', 'second'], columns =['a', 'b1']) 
```

Creating DataFrame using `zip()` function

```python
Name = ['tom', 'krish', 'nick', 'juli']  # List1
Age = [25, 30, 26, 22]  # List2
list_of_tuples = list(zip(Name, Age)) # get the list of tuples from two lists and merge them by using zip()
df = pd.DataFrame(list_of_tuples, columns = ['Name', 'Age'])
```

Creating DataFrame from Dicts of series.

```python
- # Intialise data to Dicts of series. 
d = {'one' : pd.Series([10, 20, 30, 40], index =['a', 'b', 'c', 'd']),
     'two' : pd.Series([10, 20, 30, 40], index =['a', 'b', 'c', 'd'])} 
  
- # creates Dataframe. 
df = pd.DataFrame(d) 
```

Create a new DataFrame that contains “young users” only

```python
young = users.filter(users.age < 21)
```

Alternatively, using Pandas-like syntax

```python
young = users[users.age < 21]


create a data frame - dictionary is used here where keys get converted to column names and values to row values.
``````python
data = pd.DataFrame({'Country': ['Russia','Colombia','Chile','Equador','Nigeria'],
                    'Rank':[121,40,100,130,11]})
``````python
people_dict = {
    "weight": pd.Series([68, 83, 112],index=["alice", "bob", "charles"]),  
    "birthyear": pd.Series([1984, 1985, 1992], index=["bob", "alice", "charles"], name="year"),
    "children": pd.Series([0, 3], index=["charles", "bob"]),
    "hobby": pd.Series(["Biking", "Dancing"], index=["alice", "bob"]),}

people = pd.DataFrame(people_dict)
people[people["birthyear"] < 1990]
```

### Index

```python
df.groupby('name').sum().reset_index()

s.index.values

df.reset_index()
df.reset_index(drop=True)
```

### Drop

```python
df = pd.DataFrame(details, columns = ['Name', 'Age', 'University'], 
                  index = ['a', 'b', 'c', 'd'])

df.drop(index=['b', 'c'])
df.drop([df.index[1], df.index[2]])
df.drop(['c', 'd'], inplace = True)
```

### Columns

```python
for col in df:
    print(col)

list(df)
[c for c in df]
sampledf.columns.tolist()
df.columns
```

- `list(df)`
- `[c for c in df]`
- `sampledf.columns.tolist()`
- `df.columns`

#### Add a column

```python
dataframe.insert(loc=0, column="lessor", value=pd.Series)) # avec une série
df['Address'] = ta_list # avec une liste
df2 = df.assign(address = ['Delhi', 'Bangalore', 'Chennai', 'Patna'])  # avec une liste & assign
pdf.insert(loc=len(dataframe_valid.columns), column=col_names, value=validate_series.apply(pd.Series)[0])

df['three'] = df['one'] * df['two']
``````python
data.assign(new_variable = data['ounces']*10)
```

#### drop a column

```python
data.drop('animal2',axis='columns',inplace=True)
```

#### Concatenation with Series

[Doc](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.concat.html)

```python
pd.concat([Serie_1, Serie_2, Serie_3],  axis='columns')
```

#### Calcul

```python
- # Increment everybody’s age by 1
young.select(young.name, young.age + 1)

- # Count the number of young users by gender
young.groupBy(“gender”).count()

- # Join young users with another DataFrame called logs
young.join(logs, logs.userId == users.userId, “left_outer”)

young.registerTempTable(“young”)
context.sql(“SELECT count(*) FROM young”)

- # Series function from pandas are used to create arrays
data = pd.Series([1., -999., 2., -999., -1000., 3.])
- # replace -999 with NaN values
data.replace([-999,-1000],np.nan,inplace=True)
```

* * *

# Other Packages

## Design Pattern

[Pattern Strategy with class](https://refactoring.guru/design-patterns/strategy/python/example#example-0)  
[Pattern Strategy without class](https://www.giacomodebidda.com/strategy-pattern-in-python/)

## PyValidator (Fluent Validation)

[Link](https://pypi.org/project/PyValidator/)

```python
from pyvalidator import Validator, And, Use, Optional

validator = Validator([{'wagon_number': And(str, len),
                        'lessor': And(Use(int), lambda n: 18 <= n <= 99),
                        Optional('sex'): And(str, Use(str.lower),
                                             lambda s: s in ('male', 'female'))}])

validated = validator.validate(data)
``````python
def strategy(self, strategy: Strategy) -> None:
int str datetime
SparkSession
```

# Spark

```python
# Convert Spark DataFrame to Pandas
pandas_df = young.toPandas()
# Create a Spark DataFrame from Pandas
spark_df = context.createDataFrame(pandas_df)
```