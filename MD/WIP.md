$$
WIP

$$

- [Path](#path)
- [Draft](#draft)
- [Infos](#infos)
- [Global Knowledge](#global-knowledge)
    - [Programmation](#programmation)
        - [Versionning (Git Commit n' Cie)](#versionning-git-commit-n-cie)
    - [DataScience](#datascience)
    - [Path / Dir / Namefile](#path--dir--namefile)
    - [Formation MEP des modèles](#formation-mep-des-mod%C3%A8les)
- [QUESTIONS](#questions)
- [Technique](#technique)
    - [Autres](#autres)
    - [Git Strategy](#git-strategy)
    - [Architectures](#architectures)
        - [Design Pattern](#design-pattern)
    - [Back & Archi](#back--archi)
        - [Clean Architecture](#clean-architecture)
- [Language; Format](#language-format)
    - [MarkDown](#markdown)
        - [Issue to solve - Markdown TOC](#issue-to-solve---markdown-toc)
    - [R](#r)
    - [Pyspark](#pyspark)
    - [Python](#python)
        - [SetUp](#setup)
            - [Pipenv](#pipenv)
            - [PyTest](#pytest)
            - [Makefile](#makefile)
        - [Pycharm](#pycharm)
            - [Reset Pycharm](#reset-pycharm)
            - [Issues](#issues)
        - [Logging](#logging)
- [Technologies](#technologies)
    - [Laucher, Excecuter](#laucher-excecuter)
        - [Azure functions](#azure-functions)
        - [Azure Monitor](#azure-monitor)
            - [Logging](#logging-1)
        - [Logiciel ETL](#logiciel-etl)
    - [Scheduler](#scheduler)

# Path

cd /Users/kevin.guernigou@total.com/Documents/Repository/Azure_Functions  
/Users/kevin.guernigou@total.com/Documents/Repository/Azure\_Functions/tests/test\_data/

# Draft

```python
import logging
# from logging.config import fileConfig
import os

actual = os.getcwd()
os.chdir(actual[:len(actual) - len('/tests/PlannedMaintenance')])

from PlannedMaintenance.tmpl_plannedmaint import PlannedMaintenanceETLStrategy

path_test_files = '/tests/PlannedMaintenance/files_tests'
filename = '20200413 Revision Plan 2020 for WOLF.xlsx'
path_file = os.getcwd() + path_test_files + '/' + filename

strategy = PlannedMaintenanceETLStrategy()
result = strategy.execute(path_file)
# print(result)
```

# Infos

[Formation: Industrialisation avancée d’un projet de Data Science](https://www.octo.academy/fr/formation/435-industrialisation-avancee-d-un-projet-de-data-science)

# Global Knowledge

[Pandas dataframe Vs. List Objects](https://stackoverflow.com/questions/56446910/dataframes-vs-list-of-objects)

## Programmation

Declaratif vs fonctionnel  
Checkbox et radiobuttons  
Langage procedurale  
Features flipping

### Versionning (Git Commit n' Cie)

```
<type>(<portée>): <sujet>

<description>

<footer>
```

[Git Conventionnal Naming](https://www.conventionalcommits.org/en/v1.0.0/)  
[Karma : Git commit message](http://karma-runner.github.io/6.0/dev/git-commit-msg.html)  
[GitMoji](https://gitmoji.dev/)  
[Semantic Versionning](https://semver.org)  
[Sematic release: auto versionning tag, based on conventionnal commit naming](https://github.com/semantic-release/semantic-release)

[Azure Pipelines with Microsoft Teams](https://docs.microsoft.com/en-us/azure/devops/pipelines/integrations/microsoft-teams?view=azure-devops)

## DataScience

Optimisation bayesienne sur le tuning d'hyper parametre

## Path / Dir / Namefile

[https://stackoverflow.com/questions/2235173/what-is-the-naming-standard-for-path-components](https://stackoverflow.com/questions/2235173/what-is-the-naming-standard-for-path-components)

**With a file**  
C:\\users\\OddThinking\\Documents\\My Source\\Widget\\foo.src

| ab  | I   | Libellé | Example |
| --- | --- | --- | --- |
| fr  | filroot | file root | foo |
| fn  | filname | filename or base name | foo.src |
| fe  | fileext | file extension | src / .src |
| bd  | basedir | git calls it base directory | C:\\users\\OddThinking\\Documents\\My Source\ |
| dn  | dirname | dirname | C:\\users\\OddThinking\\Documents\\My Source\\Widget\ |
| rp  | relpath | relative path | Widget\\foo.src |
| ap  | abspath | absolut path | C:\\users\\OddThinking\\Documents\\My Source\\Widget\\foo.src |

**With a directory**  
C:\\users\\OddThinking\\Documents\\My Source\\Widget

| ab  | I   | Libellé | Example |
| --- | --- | --- | --- |
| dn  | dirname | Widget | foo |
| fn  | filname | filename or base name | C:\\users\\OddThinking\\Documents\\My Source\ |
| fe  | fileext | file extension | C:\\users\\OddThinking\\Documents\\My Source\\Widget |

## Formation MEP des modèles

[https://octo-technology.gitlab.io/octo-bda/cercle-formation/dsin2/tp4/index.html#6](https://octo-technology.gitlab.io/octo-bda/cercle-formation/dsin2/tp4/index.html#6)  
[http://54.229.230.54:443/user/charles](http://54.229.230.54:443/user/charles)  
[https://gitlab.com/Cake_2320/dsin2](https://gitlab.com/Cake_2320/dsin2)

Wheel packaging  
tests unitaires, d'integration, fonctionnels  
PEP8 avec flake 8  
Type hinting  
Pyramide de test

Gitlab CiI cool pour integrer sur kubernetes  
pipeline imperatif : (comme jenkins, on definit avec du code, des variables, des class...)  
pipeline descriptif : (langage de programation assez statique, genre du yaml3)  
pipeline WYS/WYG : what you see is what you get : clic bouton

**PEER et rituels**

- 2h de kata toutes les deux semaines (changer toutes les 15/20minutes de personne qui code)
- 1 peer par semaine (fait par US, le peer peut durer plusieurs jours)
- 1 review par semaine

**CICD**  
pip install -r requirements.txt (le r signifie prend un fichier de requirement)  
python -m unittest test/test_unit

Accelerate building

**Tests unitaire**  
En Spark les tests sont long, du coup il faut moqué (Mocé?)

BDD Behavior driven developpement : framework `Behave` pour les tests haut niveau (fonctionnel)

nom de fonction de test:  
Test + nom fonction + shoud DOSOMETHING when DOING SOMETHING

Faire que les noms aient un nom qui ait le plus de sens possible pour le metier

Faire kata BDD avec Alexandre?

Archi diagonale ou clean archi

`CodeCoverage` pour la converture du code testé  
`flake8` : pip8

truc a s'imposer : tout type hinter

[https://behave.readthedocs.io/en/latest/tutorial.html](https://behave.readthedocs.io/en/latest/tutorial.html)  
Dans le test:  
un Given  
un When  
un Then

**IaC Infrastructure as Code**  
idempotent  
Oui mais comparé au stockage?

**versionning**  
[dvc.org](http://dvc.org)  
versionning de dataset en ligne de commande

streamlit : petit package pour faire des petites applications web  
Flask pour faire un truc API

# QUESTIONS

Souci des imports entre VSCode et Pycharm  
Montre l'organisation des tests et des files? comment tu regroupes les tests sur les class?  
Comment tu organises ton code? avec les class et cie? Des fonctions a part?

c'est quoi la class `__str__` et `__eq__`

Tu utilise VSCode?

Et airflow a la TDF?

arborescence des file??

# Technique

## Quality & Tests
### Critères d'acceptance
[Doc Gherkin](https://www.artza-technologies.com/blog/langage-gherkin)
Écrire les critères d'acceptation avec Gherkin

## Autres

[https://pre-commit.com/](https://pre-commit.com/)  
[Azure data factory Vs Airflow](https://www.stitchdata.com/vs/azure-data-factory/airflow/)

## Git Strategy

<u>TrunckBase :</u> Conseillé de manière général  
<u>Gitflow :</u> bien quand tu as pleins de développers  
[https://nvie.com/posts/a-successful-git-branching-model/](https://nvie.com/posts/a-successful-git-branching-model/)

## Architectures

**[Factory Method](https://refactoring.guru/design-patterns/factory-method)**

[Technologies DevOps : Architecture](https://cloud.google.com/solutions/devops/devops-tech-architecture)  
[CQRS, l’architecture aux deux visages - part 1](https://blog.octo.com/cqrs-larchitecture-aux-deux-visages-partie-1/)  
[CQRS, l’architecture aux deux visages - part 2](https://blog.octo.com/cqrs-larchitecture-aux-deux-visages-partie2/)

### Design Pattern

[Repository Pattern](https://codewithshadman.com/repository-pattern-csharp/#generic-repository-pattern-csharp)  
[Git Strategy pattern](https://gist.github.com/mibrammall/6f5e23a1b7954b0b5eb6)  
[RESTful API Design](https://blog.octo.com/wp-content/uploads/2014/10/RESTful-API-design-OCTO-Quick-Reference-Card-2.2.pdf)

[DataBase Normalisation Wiki](https://en.wikipedia.org/wiki/Database_normalization)  
[Design Pattern](https://refactoring.guru/design-patterns/strategy/python/example#example-0--main-py)

[Feature Flipping](https://blog.octo.com/feature-flipping/)  
aussi appelé FeatureToggles  
[https://featureflags.io/](https://featureflags.io/)  
[https://martinfowler.com/articles/feature-toggles.html](https://martinfowler.com/articles/feature-toggles.html)

## Back & Archi

[ORM : Object Relationnal Mapping](https://fr.wikipedia.org/wiki/Mapping_objet-relationnel)  
Permet de traduire en SQL du code.

### Clean Architecture

[SOLID](https://medium.com/@mr.anmolsehgal/solid-principles-de1029ef8a8f)  
[https://medium.com/@hassan.alizadeh529/the-clean-architecture-concept-5fcbcb1a2a78](https://medium.com/@hassan.alizadeh529/the-clean-architecture-concept-5fcbcb1a2a78)  
[https://medium.com/@mr.anmolsehgal/clean-architecture-fef10b093ad0](https://medium.com/@mr.anmolsehgal/clean-architecture-fef10b093ad0)

Liens d'Alice sur l'architecture Back ( 24/09/2020 )  
[https://blog.octo.com/architecture-hexagonale-trois-principes-et-un-exemple-dimplementation/](https://blog.octo.com/architecture-hexagonale-trois-principes-et-un-exemple-dimplementation/)  
[https://www.thedigitalcatonline.com/blog/2016/11/14/clean-architectures-in-python-a-step-by-step-example/](https://www.thedigitalcatonline.com/blog/2016/11/14/clean-architectures-in-python-a-step-by-step-example/)  
[https://jordifierro.com/django-clean-architecture](https://jordifierro.com/django-clean-architecture)

### LogicApps

#### DevOps et mise en prod

[Automatiser le déploiement](https://docs.microsoft.com/fr-fr/azure/logic-apps/logic-apps-azure-resource-manager-templates-overview)

[Format de modèle](https://docs.microsoft.com/fr-fr/azure/azure-resource-manager/templates/template-syntax#parameters)  
[Créer et déployer votre premier modèle ARM](https://docs.microsoft.com/fr-fr/azure/azure-resource-manager/templates/template-tutorial-create-first-template?tabs=azure-powershell)

[Créer un fichier de paramètres Resource Manager](https://docs.microsoft.com/fr-fr/azure/azure-resource-manager/templates/parameter-files)

[Guide de référence du schéma du langage de définition de workflow dans Azure Logic Apps](https://docs.microsoft.com/fr-fr/azure/logic-apps/logic-apps-workflow-definition-language)

# Language; Format

[Format Parquet](https://www.cetic.be/Apache-Parquet-pour-le-stockage-de-donnees-volumineuses)  
[Compare python and Scala](http://datasciencevademecum.com/2016/01/28/6-points-to-compare-python-and-scala-for-data-science-using-apache-spark/)

## MarkDown

[Markdown Openclassroom](https://openclassrooms.com/fr/courses/1304236-redigez-en-markdown)  
[Markdown](https://www.ionos.fr/digitalguide/sites-internet/developpement-web/markdown/)

### Issue to solve - Markdown TOC

[https://github.com/AlanWalk/markdown-toc/issues/91](https://github.com/AlanWalk/markdown-toc/issues/91)  
When you open "Preferences / Settings" in Visual Studio Code and search foe "EOL", you will find an entry in "Text Editor"-->"Files".  
This is set to "Auto" by default (at least on Mac) and will be just copied by the extension when you create a TOC.  
Just set this to "\\r" or "\\r\\n" and you are good to go.

## R

[Operations sur les directory/files R](https://thinkr.fr/operations-sur-les-fichiers-et-les-dossiers-sous-r/)

## Pyspark

[Window Funtion](https://databricks.com/blog/2015/07/15/introducing-window-functions-in-spark-sql.html)

## Python

### SetUp

#### Pipenv

[JetBrain - Configure a Pipenv environment](https://www.jetbrains.com/help/pycharm/pipenv.html)  
[Pipenv & Virtual Environments](https://docs.python-guide.org/dev/virtualenvs/)  
[Conda or pipenv](https://stackoverflow.com/questions/51978816/what-is-the-difference-between-pycharm-virtual-environment-and-anaconda-environm)  
[Guide of Choosing Package Management Tool for Data Science Project](https://towardsdatascience.com/guide-of-choosing-package-management-tool-for-data-science-project-809a093efd46)

#### PyTest

[pytest - init et package - Good](https://docs.pytest.org/en/stable/goodpractices.html)  
[Call pytest](https://docs.pytest.org/en/stable/usage.html)

#### Makefile

make has an great option to validate the commands you want to run without executing them. For that just use --dry-run.  
[phony-usage-in-makefile](https://stackoverflow.com/questions/20352239/phony-usage-in-makefile)

[missing separatior issue](https://stackoverflow.com/questions/16931770/makefile4-missing-separator-stop)  
make has a very stupid relationship with tabs. All actions of every rule are identified by tabs. And, no, four spaces don't make a tab. Only a tab makes a tab.

To check, I use the command `cat -e -t -v makefile_name.`

It shows the presence of **tabs** with `^I` and **line endings** with `$`. Both are vital to ensure that dependencies end properly and tabs mark the action for the rules so that they are easily identifiable to the make utility.

### Pycharm

#### Reset Pycharm

~/Library/Preferences/jetbrains.jetprofile.asset.plist  
~/Library/Preferences/jetbrains.pycharm.861530e5.plist

~/Library/Caches/JetBrains/  
~/Library/Application Support/

#### Issues

[In IntelliJ, How Do I Fix “These Files Do Not Belong to the Project”?](https://stackoverflow.com/questions/28099716/in-intellij-how-do-i-fix-these-files-do-not-belong-to-the-project)  
This happened for me when I ignored the .idea/ files. Just do

```
rm -rf .idea/
```

And then File -> Invalidate Caches/Restart -> Invalidate And Restart.

### Logging

<u>**[FOR AZURE FUNCTIONS](#logging-1)**</u>

<u>**https://pypi.org/project/opencensus-ext-azure/**</u>

[Best Tuto Ever](http://sametmax.com/ecrire-des-logs-en-python/)  
[Doc Python](https://docs.python.org/3/howto/logging.html)  
[Doc Python FR](https://docs.python.org/fr/3/howto/logging.html)

[Best Practices :](https://www.loggly.com/use-cases/6-python-logging-best-practices-you-should-be-aware-of/)

1.  **Python Default Logging Module**
2.  **Logging Levels**
3.  **Basic Logging Configuration**```python
    import logging
    logging.basicConfig(filename='myfirstlog.log', 
    level=logging.DEBUG, 
    format='%(asctime)s | %(name)s | %(levelname)s | %(message)s')
    ```
4.  **Include a Timestamp for Each Log Entry**
5.  **Adopt the ISO-8601 Format for Timestamps**```python
     import logging
     logging.basicConfig(format='%(asctime)s %(message)s')
     logging.info('Example of logging with ISO-8601 timestamp')
     1977-04-22T06:00:00Z
    ```
6.  **Use the RotatingFileHandler Class**  
    A general logging best practice—in any language—is to use log rotation. This is a mechanism designed to automatically archive, compress, or delete old log files to prevent full disks.  
    Fortunately, you don’t have to implement this by hand in Python. Instead, use the RotatingFileHandler class instead of the regular FileHandler one.

[Tuto & Explanation](https://www.toptal.com/python/in-depth-python-logging)

#### Log techniques et fonctionnelles

[Slides](https://fr.slideshare.net/sander_devos/functional-and-non-functional-application-logging-8525575)  
Log Functional : Fonctionnelle

- For Client
- Audit
- Governance
- Security

Logs Non-Functional : Technique

- For developpers
- Faults, Errors, Exceptions
- Execution context
- Performance
- Component usage

[les 10 commandements](https://blog.engineering.publicissapient.fr/2008/07/11/les-10-commandements-des-logs-applicatives/)

#### Log File Config

[Example](https://docs.python-guide.org/writing/logging/#example-configuration-via-an-ini-file)  
[Doc Python](https://docs.python.org/3/howto/logging.html#configuring-logging)  
[Doc Python Logging Config](https://docs.python.org/3/library/logging.config.html)  
[Issue with multiple logger](https://stackoverflow.com/questions/8562954/python-logging-to-different-destination-using-a-configuration-file)

#### Interpreter

[LOGGLY : Python Logger](https://www.loggly.com/solution/python-logger/)

#### Precision

<u>Propagate:</u> Decides whether a log should be propagated to the logger’s parent. By default, its value is True.  
In Script  
`logger.propagate = False`  
In file init  
`propagate=0`

### Global

Relire les classes : [openclassroom](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/232721-apprehendez-les-classes)  
Refaire le trucs avec les args la c'est chouette : [link](https://www.pierre-giraud.com/python-apprendre-programmer-cours/parametre-argument-fonction/)

### Import Issue

```python
sys.path.append("/path/to/parent")
sys.path.append("..")

```

[https://stackoverflow.com/questions/51648724/relative-import-in-pycharm-2018-does-not-work-like-relative-import-in-python-3-6/51820200](https://stackoverflow.com/questions/51648724/relative-import-in-pycharm-2018-does-not-work-like-relative-import-in-python-3-6/51820200)  
[https://intellij-support.jetbrains.com/hc/en-us/community/posts/360004264019-Pycharm-doesn-t-detect-suggest-modules-classes-or-functions-to-import-when-typing](https://intellij-support.jetbrains.com/hc/en-us/community/posts/360004264019-Pycharm-doesn-t-detect-suggest-modules-classes-or-functions-to-import-when-typing)  
[https://stackoverflow.com/questions/32395926/python-relative-import-with-more-than-two-dots](https://stackoverflow.com/questions/32395926/python-relative-import-with-more-than-two-dots)  
[https://stackoverflow.com/questions/60828419/unresolved-import-python-vscode](https://stackoverflow.com/questions/60828419/unresolved-import-python-vscode)

# Technologies

## Laucher, Excecuter

### Azure functions

[https://docs.microsoft.com/en-us/azure/developer/python/tutorial-vs-code-serverless-python-01#:~:text=In this article%2C you use,or publish a web app](https://docs.microsoft.com/en-us/azure/developer/python/tutorial-vs-code-serverless-python-01#:~:text=In%20this%20article%2C%20you%20use,or%20publish%20a%20web%20app)  
[https://docs.microsoft.com/en-us/azure/developer/python/tutorial-vs-code-serverless-python-02](https://docs.microsoft.com/en-us/azure/developer/python/tutorial-vs-code-serverless-python-02)  
[Examine the Python code files in Visual Studio Code](https://docs.microsoft.com/en-us/azure/developer/python/tutorial-vs-code-serverless-python-03)  
[Invoke an Azure Function Using Power Automate](https://clavinfernandes.wordpress.com/2019/12/09/invoke-an-azure-function-using-microsoft-flow/)  
[Azure Functions in practice](https://www.troyhunt.com/azure-functions-in-practice/)  
[https://tryfunctions.com/ng-min/try?trial=true](https://tryfunctions.com/ng-min/try?trial=true)

### Azure Monitor

[Azure Monitor](https://azure.microsoft.com/fr-fr/services/monitor/)  
[Azure Monitor : Log Queries](https://docs.microsoft.com/fr-fr/azure/azure-monitor/log-query/get-started-queries)  
[Azure Monitor : Log Queries use functions](https://docs.microsoft.com/en-us/azure/azure-monitor/log-query/functions)

#### Logging

[Doc AzureFunc FR : Python logging](https://docs.microsoft.com/fr-fr/azure/azure-functions/functions-reference-python#logging)  
[Doc AzureFunc EN : Python logging](https://docs.microsoft.com/en-us/azure/azure-functions/functions-reference-python#logging)  
[Q&A](https://docs.microsoft.com/en-us/answers/questions/46110/application-logging-for-azure-functions-in-python.html)  
[Doc Microsoft Monitor Azure Functions](https://docs.microsoft.com/en-us/azure/azure-functions/functions-monitoring)  
[Monitorage d’Azure Functions avec Azure Monitor Logs](https://docs.microsoft.com/fr-fr/azure/azure-functions/functions-monitor-log-analytics?tabs=python)

### Logiciel ETL

[https://www.cartelis.com/blog/comparatif-logiciels-etl/](https://www.cartelis.com/blog/comparatif-logiciels-etl/)

## Scheduler

[Azure Data Factory](https://docs.microsoft.com/en-us/azure/data-factory/introduction)  
[Azure Data Factory vs. Apache Airflow vs. Stitch](https://www.stitchdata.com/vs/azure-data-factory/airflow/)