set global direction

```
left to right direction
```

top bottom left right

change skin component

```
skinparam componentStyle uml1
```

### Legend

```
 legend
 |= |= Type |
 |<back:#FF0000>   </back>| Type A class |
 |<back:#00FF00>   </back>| Type B class |
 |<back:blue>   </back>| Type C class |
 endlegend
 ```