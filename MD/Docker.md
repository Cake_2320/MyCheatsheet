<!-- TOC -->

- [Azure Functions & Docker](#azure-functions--docker)
  - [Post du Docker](#post-du-docker)
    - [1. On Build le docker](#1-on-build-le-docker)
    - [2. On la push sur le container registery](#2-on-la-push-sur-le-container-registery)
    - [3. On le Run en local](#3-on-le-run-en-local)
  - [Tester le code de l'azure function](#tester-le-code-de-lazure-function)
    - [En local en debuggant](#en-local-en-debuggant)
    - [Sur docker en local](#sur-docker-en-local)
    - [Sur la fonction finale](#sur-la-fonction-finale)

<!-- /TOC -->

# Azure Functions & Docker
## Post du Docker
### 1. On Build le docker
```docker
docker build -t containerregistrywolftestwe.azurecr.io/az-fn-python-extract-pdf:v1 .
```
`t`: c'est pour donner le nom du tag
`containerregistrywolftestwe.azurecr.io`: le path du container registery
`az-fn-python-extract-pdf`: le nom du container
`v1`: le nom du tag
`.`: le path du localfile : mais la on est dedans donc `.`

### 2. On la push sur le container registery
```docker
docker push containerregistrywolftestwe.azurecr.io/az-fn-python-extract-pdf:v1
```

Authentification : car c'est une registery privée
```docker
az acr login --name ContainerRegistryWolfTestWe
```
`az`: azure
`acr`: azure container registery
`ContainerRegistryWolfTestWe`: nom du conteneur. Pas de CamelCase ici, il faut tout en lowercase pour le nom du conteneur.


- Après le push il faut relancer l'azure fonction (plutot stop puis start que restart)
- Dans l'Azure function verrifier que le code est bien celui que l'on a pusher

### 3. On le Run en local
```docker
docker run -p 8080:80 -it containerregistrywolftestwe.azurecr.io/az-fn-python-extract-pdf:v1
```


## Tester le code de l'azure function
### En local en debuggant

[How to do](https://docs.microsoft.com/fr-fr/azure/azure-functions/functions-create-function-linux-custom-image?tabs=bash%2Cportal&pivots=programming-language-python#create-and-test-the-local-functions-project)

1. Dans VSCode on a une partie qui est le run (Dans debug faire l'attach) : Attach to python function
2. Une fois lancé on recupère l'adresse local (URL local host) de la fonction que l'on désire tester
3. On la rajoute dans PostMan, avec ajout des paramètre et on run
   Exemple: `/api/HttpExample?name=Functions`, avec `Functions` pour le paramètre de `name`
   `http://localhost:7071/api/NewWDR?ru=Europorte`

le fichier `lanch.json` contient les paramètres pour le run de ce truc la (avec le nom de l'execution que l'on va attach puis run)
`function.json` contient les paramètres de la fonction et la manière dont elle va être lancé

### Sur docker en local
1. Lauch le docker (dans le dossier obligatoirement ?)
```docker
docker run -p 8080:80 -it containerregistrywolftestwe.azurecr.io/az-fn-python-extract-pdf:v1
```
`8080:80` : Par default dans le docker le port est `80`, du coup on expose `8080` pour l'exterieur
`-it` : pour interactif
À l'interieur c'est le port `80` qui est lancé, mais a l'extérieur c'est `8080`
2. Les étapes sont les mêmes que se que l'on a fait précédemment avec comme forme d'url pour PostMan plutot cela:
   Exemple: `http://localhost:8080/api/NewWDR?ru=Europorte`

> [!WARNING] Si unauthorized :
> - go dans `function.json`, checker le `AuthLevel`, et mettre `anonymous` mais QUE EN LOCAL !!! (sinon c'est function ou admin : niveau de sécurité supérieur)
> - puis Rebuild le docker

### Sur la fonction finale
1. Il faut avoir la fonction pushé
2. récupérer l'url sur `portal.azure.com`
3. Aller sur l'azure fonction, dans l'`Overview` ou le `Code + Test`
4. On config/récupère l'url, les options sont:
   -  pour `AuthLevel` = `function`: on a alors le default (function key)
   -  pour `AuthLevel` = `admin`: (host key) 
5. On récupère ici l'url que l'on peut mettre dans PostMan