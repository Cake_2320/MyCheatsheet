# command

## two conditions

To verify this:

````html

<div class="Caption">
    Model saved
</div>
````

Write this -

```
//div[contains(@class, 'Caption') and text()='Model saved']
```

And to verify this:-

````html

<div id="alertLabel" class="gwt-HTML sfnStandardLeftMargin sfnStandardRightMargin sfnStandardTopMargin">
    Save to server successful
</div>
````

Write this -

```
//div[@id='alertLabel' and text()='Save to server successful']
```

## call an element in an other one

```python
xpath_first_object = // ts[contains( @


class , 'column-dateEnd')]
xpath_second_object_into_first_one = xpath_first_object + xpath_second_object
```

## use Keyboard

````python
from selenium.webdriver.common.keys import Keys

self.driver.find_element_by_xpath(xpath).send_keys(Keys.SHIFT, Keys.ARROW_UP)
self.driver.find_element_by_xpath(xpath).send_keys(Keys.DELETE)
````

## Get text

````python
self.driver.find_element(By.XPATH, xpath_elem).text
self.driver.find_element_by_xpath(xpath_elem).text
````

## Explanation

For :

````XML

<Home>
    <Addr>
        <Street>ABC</Street>
        <Number>5</Number>
        <Comment>BLAH BLAH BLAH
            <br/>
            <br/>ABC
        </Comment>
    </Addr>
</Home>
````


The `<Comment>` tag contains two text nodes and two `<br>` nodes as children.

Your xpath expression was

```
//*[contains(text(),'ABC')]
```

To break this down,

1. `*` is a selector that matches any element (i.e. tag) -- it returns a node-set.
2. The `[]` are a conditional that operates on each individual node in that node set. It matches if any of the
   individual nodes it operates on match the conditions inside the brackets.
3. `text()` is a selector that matches all of the text nodes that are children of the context node -- it returns a node
   set.
4. `contains` is a function that operates on a string. If it is passed a node set, the node set
   is [converted into a string by returning the string-value of the node in the node-set that is first in document order](http://www.w3.org/TR/xpath/#section-String-Functions)
   . Hence, it can match only the first text node in your `<Comment>` element -- namely `BLAH BLAH BLAH`. Since that doesn't match, you don't
   get a `<Comment>` in your results.


You need to change this to
```
//*[text()[contains(.,'ABC')]]
```

1. `*` is a selector that matches any element (i.e. tag) -- it returns a node-set.
2. The outer `[]` are a conditional that operates on each individual node in that node set -- here it operates on each
   element in the document.
3. `text()` is a selector that matches all of the text nodes that are children of the context node -- it returns a node
   set.
4. The inner `[]` are a conditional that operates on each node in that node set -- here each individual text node. Each
   individual text node is the starting point for any path in the brackets, and can also be referred to explicitly
   as `.` within the brackets. It matches if any of the individual nodes it operates on match the conditions inside the
   brackets.
5. `contains` is a function that operates on a string. Here it is passed an individual text node (.). Since it is passed
   the second text node in the `<Comment>` tag individually, it will see the `'ABC'` string and be able to match it.